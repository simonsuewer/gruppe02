/*
 * 
 */
package application;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import view.GameViewManager;

// TODO: Auto-generated Javadoc
/**
 * The Class KeyInput.
 */
public class KeyInput extends de.hsh.projekt.common.KeyInput {

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyTyped(KeyEvent e) {
	    GameViewManager.keyPressed(e);
	    System.out.println("\n\n\n\n\n "+e.getKeyCode());
	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyPressed(KeyEvent e) {
	    GameViewManager.keyPressed(e);

	}

	/* (non-Javadoc)
	 * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		System.out.println("r" + e.getKeyCode());
		GameViewManager.keyReleased(e);

	}

}

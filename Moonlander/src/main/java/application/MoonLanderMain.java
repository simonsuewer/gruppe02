package application;

import javafx.application.Application;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.stage.Stage;
import view.ViewManager;

public class MoonLanderMain extends Application {
    public static MoonLanderMain instance = new MoonLanderMain();
    Stage ps;
    public static JFXPanel panel;

    @Override
    public void start(Stage primaryStage) {
	ps = primaryStage;
	try {
	    ViewManager manager = new ViewManager();
	    primaryStage = manager.getMainStage();

	    primaryStage.show();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public JFXPanel jFX() {
	panel = new JFXPanel();
	ViewManager manager = new ViewManager();
	Scene scene = manager.getMainScene();
	panel.setScene(scene);
	return panel;
    }

    public static void main(String[] args) {
	launch(args);
    }
}

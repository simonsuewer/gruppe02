package de.hsh.max.starter;

import javax.swing.JMenuBar;
import javax.swing.JPanel;

import application.MoonLanderMain;
import de.hsh.projekt.common.GameEventInitiater;
import de.hsh.projekt.common.MouseInput;
import javafx.embed.swing.JFXPanel;

public class MoonlanderStarter extends de.hsh.projekt.common.Game {

    @Override
    public int start() {

        return 0;
    }

    @Override
    public int backToMain() {
        // gameloop stoppen!!!
        GameEventInitiater.exitGame();
        return 0;
    }

    @Override
    public de.hsh.projekt.common.KeyInput getKeyInput() {
        return new application.KeyInput();
    }

    @Override
    public MouseInput getMouseInput() {
        return new MouseInput();
    }

    @Override
    public String getName() {
        return "Moonlander";
    }

    @Override
    public String getDescription() {
        return "<html>des </html>";
    }

    @Override
    public JPanel getPanel() {
        return null;
    }

    @Override
    public JFXPanel getJFXPanel() {
	MoonLanderMain window = MoonLanderMain.instance;

        return window.jFX();

    }

    @Override
    public JMenuBar getJMenuBar() {
        return null;
    }
}

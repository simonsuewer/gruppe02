package model;

import java.text.DecimalFormat;
import java.util.Date;

import javafx.geometry.Pos;
import javafx.scene.SubScene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 * @author Max
 * Erstellt den EndScreen nach Ende eines Levels
 */
public class EndScreen extends SubScene {

    Label lSummary = new Label();
    InfoLabel lHead = new InfoLabel("");
    
    VBox box = new VBox();
    
    private long startTime;
    private long curTime;
    private String dur;
    private int level;
    
    Button btnMenu;
    
    /**
     * @param fuel verbleibender Treibstoff
     * @param velocity Geschwindigkeit bei Landung
     * @param time Startzeitpunkt des aktuellen Levels
     * @param curLevel aktuelles Level(für Easteregg)
     */
    public EndScreen(int fuel, double velocity, long time, int curLevel) 
    {
        super(new AnchorPane(), 400, 400);
        prefWidth(400);
        prefHeight(400);
        
        startTime = time;
        level = curLevel;
        BackgroundFill image = new BackgroundFill(Color.BLACK, null, null);
        AnchorPane root2 = (AnchorPane) this.getRoot();
        
        root2.setBackground(new Background(image));
        
        root2.getChildren().add(initEndScreen(fuel, velocity, time));
    }
    
    public VBox initEndScreen(int fuel, double velocity, long time)
    {
        box.setSpacing(80);
        box.setAlignment(Pos.CENTER);
        
        lSummary.setFont(new Font("Verdana", 18));
        lSummary.setTextFill(Color.WHITE);
        
        DecimalFormat df = new DecimalFormat("#0.00"); 
        
        curTime = new Date().getTime()-startTime;
        dur = df.format(curTime/1000d).toString();
        if(velocity <= 0.5)
        {
            velocity*=20;
            if(level==1&&curTime/1000d<9)
            {
                lHead.setText("Congratulations, Neil!");
                lSummary.setText("That\'s one small step for a man,\n one giant leap for mankind.\n\n You landed after "+ dur +"seconds \nwith a velocity of "+df.format(velocity).toString() + "m/s \nand "+ fuel +"fuel left.");
            }
            else 
            {
                lHead.setText("Congratulations!");
                lSummary.setText("You landed after "+ dur +"seconds \nwith a velocity of "+df.format(velocity).toString() + "m/s \nand "+ fuel +"fuel left.");
            }
        }
        else
        {
            velocity*=20;
            lHead.setText("You crashed!");
            lSummary.setText("You crashed after "+ dur +"seconds \nwith a velocity of "+df.format(velocity).toString() + "m/s \nand "+ fuel +"fuel left.");
        }
        
        btnMenu = new Button ("Back to main menu");
        
        btnMenu.setMinWidth(150);
        btnMenu.setMinHeight(50);
//        btnMenu.setOnAction(new EventHandler<ActionEvent>() 
//        {
//
//            @Override
//            public void handle(ActionEvent event) 
//            {
//                backToMenu();
//            }
//        });
        box.getChildren().addAll(lHead, lSummary, btnMenu);
        System.out.println(box.getChildren());
        return box;
    }
    
    public AnchorPane getPane()
    {
        return (AnchorPane) this.getRoot();
    }
    public Button getButton()
    {
        return btnMenu;
    }

}

package model;

import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class InfoLabel extends Label 
{
    public InfoLabel(String text)
    {
        setPrefWidth(380);
        setPrefHeight(49);
        setText(text);
        setWrapText(true);
        setFont(Font.font("Verdana",23));
        setTextFill(Color.WHITE);
        setAlignment(Pos.CENTER);
    }
    
}

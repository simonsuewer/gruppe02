package model;

import javafx.animation.TranslateTransition;
import javafx.scene.SubScene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class MoonlanderSubScene extends SubScene {

    private boolean isHidden;
    
    public MoonlanderSubScene() 
    {
        super(new AnchorPane(), 600, 400);
        prefWidth(600);
        prefHeight(400);

        BackgroundFill image = new BackgroundFill(Color.BLACK, null, null);
        AnchorPane root2 = (AnchorPane) this.getRoot();
        
        root2.setBackground(new Background(image));
        
        isHidden = true;
        
        setLayoutX(1024);
        setLayoutY(200);        
    }
    
    public void moveSubScene()
    {
        TranslateTransition transition = new TranslateTransition();
        transition.setDuration(Duration.seconds(0.3));
        transition.setNode(this);
        
        if(isHidden) 
        {
            transition.setToX(-635);
            isHidden = false;
        }
        else
        {
            transition.setToX(0);
            isHidden = true;
        }
        
        transition.play();
    }
    
    public AnchorPane getPane()
    {
        return (AnchorPane) this.getRoot();
    }

}

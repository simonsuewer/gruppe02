package model;

public enum PLANET 
{
    MOON("view/resources/planetchooser/moon.jpg"),
    VENUS("view/resources/planetchooser/venus.jpeg"),
    MARS("view/resources/planetchooser/mars.jpg");
    
    private String urlPlanet;
    
    private PLANET(String urlPlanet) 
    {
        this.urlPlanet = urlPlanet;
    }
    
    public String getURL() 
    {
        return this.urlPlanet;
    }
}

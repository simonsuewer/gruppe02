package model;

import javafx.geometry.Pos;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class PlanetPicker extends VBox 
{
    private ImageView planetImage;
    
    private PLANET planet;
    
    public PlanetPicker(PLANET planet)
    {
        planetImage = new ImageView(planet.getURL());
        this.planet = planet;
        this.setAlignment(Pos.CENTER);
        this.setSpacing(20);
        this.getChildren().add(planetImage);
    }
    
    public PLANET getPlanet() 
    {
        return planet;
    }
}

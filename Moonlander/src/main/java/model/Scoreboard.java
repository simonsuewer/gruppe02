package model;

import java.text.DecimalFormat;
import java.util.Date;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.SubScene;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Scoreboard extends SubScene {

    Label lDistance = new Label();
    Label lFuel = new Label();
    Label lVelocity = new Label();
    Label lGravity = new Label();
    Label lTime = new Label();
    
    VBox box = new VBox();
    
    private long startTime;
    private long curTime;
    private String dur;
    
    public Scoreboard(double distance, int fuel, double velocity, double gravity, long time) 
    {
        super(new AnchorPane(), 250, 150);
        prefWidth(250);
        prefHeight(100);
        
        BackgroundFill image = new BackgroundFill(Color.BLACK, null, null);
        AnchorPane root2 = (AnchorPane) this.getRoot();
        
        root2.setBackground(new Background(image));
        
        startTime = time;
        
        root2.getChildren().add(initScoreboard(distance, fuel, velocity, gravity));
    }
    
    public VBox initScoreboard(double distance, int fuel, double velocity, double gravity)
    {
        box.setSpacing(8);
        box.setPadding(new Insets(0, 0, 0, 10));
        lDistance.setFont(new Font("Verdana", 18));
        lDistance.setTextFill(Color.WHITE);
        lFuel.setFont(new Font("Verdana", 18));
        lFuel.setTextFill(Color.WHITE);
        lVelocity.setFont(new Font("Verdana", 18));
        lVelocity.setTextFill(Color.WHITE);
        lGravity.setFont(new Font("Verdana", 18));
        lGravity.setTextFill(Color.WHITE);
        lTime.setFont(new Font("Verdana", 18));
        lTime.setTextFill(Color.WHITE);
        
        distance*=20;
        velocity*=20;
        gravity*=1200;
        
        DecimalFormat df = new DecimalFormat("#0.00"); 
        lDistance.setText("Distance: " + df.format(distance).toString() + "m");
        lFuel.setText("Fuel: " + fuel + "units");
        lVelocity.setText("Velocity: " + df.format(velocity).toString() + "m/s");
        lGravity.setText("Gravity: " + df.format(gravity).toString() + "m/s²");
        lTime.setText("Duration: 0s");
        box.getChildren().addAll(lDistance, lFuel, lVelocity, lGravity, lTime);
        
        return box;
    }
    
    public void updateScoreboard(double distance, int fuel, double velocity)
    {
        if(distance<0)
        {
            distance=0;
        }
        else
        {
            distance*=20;
        }
        velocity*=20;
        DecimalFormat df = new DecimalFormat("#0.00"); 
        
        curTime = new Date().getTime()-startTime;
        dur = df.format(curTime/1000d).toString();
        lDistance.setText("Distance: " + df.format(distance).toString() + "m");
        lFuel.setText("Fuel: " + fuel + "units");
        lVelocity.setText("Velocity: " + df.format(velocity).toString() + "m/s");
        lTime.setText("Duration: " + dur + "s");
        System.out.println(dur);
        box.getChildren().removeAll(box.getChildren());
        box.getChildren().addAll(lDistance, lFuel, lVelocity, lGravity, lTime);
    }
    
    public AnchorPane getPane()
    {
        return (AnchorPane) this.getRoot();
    }

}

package view;

import java.awt.event.KeyEvent;
import java.util.Date;

import application.MoonLanderMain;
import javafx.animation.AnimationTimer;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.stage.Stage;
import model.EndScreen;
import model.Scoreboard;

/**
 * @author Max
 * Grundgerüst für das Spiellevel
 */
public class GameViewManager 
{
    private AnchorPane gamePane;
    private Scene gameScene;
    private static JFXPanel gameStage;
    private Scoreboard score;
    private EndScreen endScreen;
    
    private static final int GAME_WIDTH = 1024;
    private static final int GAME_HEIGHT = 768;
    
    private JFXPanel menuStage;
    private int level;
    private ImageView ship;
    private ImageView fire;
    private ImageView ground;
    
    private static boolean isAccelerating;
    private double velocity = 0;
    private double acceleration = 0;
    private double gravity = 0;
    private double distance = 554d;
    private int fuel = 600;
    private long startTime;
//    private long endTime;
    
//    private Media media;
//    private MediaPlayer player;
    
//    Media media = new Media(Paths.get("src/view/resources/sounds/bgm.wav").toUri().toString()); //replace /Movies/test.mp3 with your file
//    MediaPlayer player = new MediaPlayer(media); 
//    player.play();
    
    private AnimationTimer gameTimer;
    
    public GameViewManager()
    {
        initializeStage();
       // createListeners();
    }
    
    /**
     * Erstellt die TastenEvents für die Pfeiltaste nach oben und ESC
     */
    
    public static void keyPressed(java.awt.event.KeyEvent e) {
	
	System.out.println("\n\n\n\n\n "+e.getKeyCode());
	 if(e.getKeyCode() == KeyEvent.VK_UP)
         {
             isAccelerating = true;
         }
         else if(e.getKeyCode() == KeyEvent.VK_ESCAPE)
         {
             backToMenu();
         }
    }
    
    public static void keyReleased(java.awt.event.KeyEvent event) {
	 if(event.getKeyCode() == KeyEvent.VK_UP)
         {
             isAccelerating = false;
         }
   }
    /*
    private void createListeners() 
    {
        gameScene.setOnKeyPressed(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) 
            {
                if(event.getCode() == KeyCode.UP)
                {
                    isAccelerating = true;
                }
                else if(event.getCode() == KeyCode.ESCAPE)
                {
                    backToMenu();
                }
            }
        });
        gameScene.setOnKeyReleased(new EventHandler<KeyEvent>() {

            @Override
            public void handle(KeyEvent event) 
            {
                if(event.getCode() == KeyCode.UP)
                {
                    isAccelerating = false;
                }
            }
        });
    }*/

    /**
     * Erstellt das neue Fenster für das aktuelle Level
     */
    private void initializeStage()
    {
        gamePane = new AnchorPane();
        gameScene = new Scene(gamePane, GAME_WIDTH, GAME_HEIGHT);
        gameStage = MoonLanderMain.panel;
        gameStage.setScene(gameScene);
        
    }
    
    /**
     * Initialisiert ein neues Spiel und konfiguriert die Gravitations- und Beschleunigungswerte.
     * @param menuStage speichert die Stage vom Hauptmenü
     * @param level speichert die Levelauswahl
     */
    public void createNewGame(Stage menuStage, int level)
    {
        this.menuStage = MoonLanderMain.panel;
        this.level = level;
        if(level == 1)
        {
            gravity = 0.00135d; 
            acceleration = 0.0027d;
            createPlanet(level);
            //Entspricht Realität Maßstab 1:20, 2x gravity
        }
        else if(level == 2)
        {
            gravity = 0.002d;
            acceleration = 0.003d;
            createPlanet(level);
            //1.5x gravity
        }
        else if(level == 3)
        {
            gravity = 0.0008d;
            acceleration = 0.001d;
            createPlanet(level);
            //1.25x gravity<
        }
        this.menuStage.hide();
        startTime = new Date().getTime();
        createShip();
        createScoreboard();
        createGameLoop();
        gameStage.show();
    }
    
    private void createPlanet(int level) 
    {
        switch(level) 
        {
            case 1:
                ground = new ImageView("view/resources/GroundMoon.png");
                break;
                
            case 2:    
                ground = new ImageView("view/resources/GroundVenus.png");
                break;
                
            case 3:    
                ground = new ImageView("view/resources/GroundMars.png");
                break;
        }    
        ground.setLayoutY(GAME_HEIGHT-200);
        gamePane.getChildren().add(ground);

        Image backgroundImage = new Image("view/resources/spacebg.png",1024,768,false,true);
        BackgroundImage background = new BackgroundImage(backgroundImage, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, null);
        gamePane.setBackground(new Background(background));
    }

    /**
     * Erstellt die Antriebsgrafik als versteckte Grafik und die Spielfigur
     */
    private void createShip()
    {
        ship = new ImageView("view/resources/rocket.png");
        ship.setLayoutX(GAME_WIDTH/2-128/2);
        ship.setLayoutY(0);
        gamePane.getChildren().add(ship);
        fire = new ImageView("view/resources/Flame.png");
        fire.setLayoutX(ship.getLayoutX());
        fire.setLayoutY(ship.getLayoutY()+99);
        gamePane.getChildren().add(fire);
        gamePane.getChildren().get(2).setVisible(false);
    }
    
    /**
     * Initialisiert und positioniert das Scoreboard 
     */
    private void createScoreboard()
    {
        score = new Scoreboard(distance, fuel, velocity, gravity, startTime);
        score.setLayoutX(10);
        score.setLayoutY(10);
        gamePane.getChildren().add(score);
    }
    
    /**
     * Wird bis zum Ende des Spiels optimal 60x/sekunde aufgerufen und enthält alle Befehle für den Spielfluss
     * Behandelt ebenfalls das Spielende und ruft den EndScreen auf
     */
    private void createGameLoop() 
    {
        gameTimer = new AnimationTimer()
        {
            @Override
            public void handle(long arg0) 
            {
                if(isAccelerating && fuel > 0) 
                {
                    fuel--;
                    velocity += gravity - acceleration;
                    gamePane.getChildren().get(2).setVisible(true);
                }
                else
                {
                    velocity += gravity;
                    gamePane.getChildren().get(2).setVisible(false);
                }
                
                if(distance>0)
                {
                    moveShip();
                    score.updateScoreboard(distance, fuel, velocity);
                }
                else if(velocity>0.5)
                {
                    gamePane.getChildren().get(2).setVisible(false);
                    gameTimer.stop();
                    endScreen = new EndScreen(fuel, velocity, startTime, level);
                    endScreen.setLayoutX((1024-400)/2);
                    endScreen.setLayoutY((784-400)/2);
                    endScreen.getButton().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() 
                         {

                            @Override
                            public void handle(MouseEvent event) {
                                backToMenu();
                                
                            }
                    });
//                    endScreen.getPane().getChildren().get(2).addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() 
//                    {
//                        @Override
//                        public void handle(MouseEvent arg0) 
//                        {
//                            backToMenu();
//                        }
//                        
//                    });
                    gamePane.getChildren().add(endScreen);
                }
                else
                {
                    gamePane.getChildren().get(2).setVisible(false);
                    gameTimer.stop();
                    endScreen = new EndScreen(fuel, velocity, startTime, level);
                    endScreen.setLayoutX((1024-400)/2);
                    endScreen.setLayoutY((784-400)/2);
                    endScreen.getButton().addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() 
                    {

                       @Override
                       public void handle(MouseEvent event) {
                           backToMenu();
                           
                       }
               });
//                    endScreen.getPane().getChildren().get(2).addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() 
//                    {
//                        @Override
//                        public void handle(MouseEvent arg0) 
//                        {
//                            backToMenu();
//                        }
//                        
//                    });
                    gamePane.getChildren().add(endScreen);
                }
                
            }
        };
        gameTimer.start();
    }
    
    /**
     * bewegt die Spielfigur und die Animationsgrafik in jedem GameLoop durchlauf 
     */
    private void moveShip()
    {
        ship.setLayoutY(ship.getLayoutY()+velocity);
        fire.setLayoutY(fire.getLayoutY()+velocity);
        distance -= velocity;
    }
    
    /**
     * Schließt das Level und ruft das Menü auf
     */
    private static void backToMenu()
    {
        gameStage.removeAll();
        //menuStage.show();
    }
}

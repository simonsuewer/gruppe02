package view;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.InfoLabel;
import model.MoonlanderSubScene;
import model.PLANET;
import model.PlanetPicker;

public class ViewManager {

    private static final int HEIGHT = 768;
    private static final int WIDTH = 1024;
    private AnchorPane mainPane;
    private Scene mainScene;

    public Scene getMainScene() {
	return mainScene;
    }

    private Stage mainStage;

    private MoonlanderSubScene controlsSubScene;
    private MoonlanderSubScene planetChooserSubScene;

    private MoonlanderSubScene sceneToHide;

    List<PlanetPicker> planetList;

    public ViewManager() {
	mainPane = new AnchorPane();
	mainScene = new Scene(mainPane, WIDTH, HEIGHT);
	// mainStage = new Stage();
	// mainStage.setScene(mainScene);

	createSubScenes();
	createButtons();
	createBackground();
	createLogo();

	Media media = new Media(getClass().getClassLoader().getResource("sound/bgm.wav").toExternalForm());
	MediaPlayer player = new MediaPlayer(media);
	player.play();
    }

    private void showSubScene(MoonlanderSubScene subScene) {
	if (sceneToHide != null) {
	    sceneToHide.moveSubScene();
	}

	subScene.moveSubScene();
	sceneToHide = subScene;
    }

    private void createSubScenes() {
	createControlsSubScene();
	createPlanetChooserSubScene();
    }

    private void createControlsSubScene() {
	controlsSubScene = new MoonlanderSubScene();
	mainPane.getChildren().add(controlsSubScene);

	VBox box = new VBox();
	HBox boxUp = new HBox();
	HBox boxEsc = new HBox();
	Label lCtrlUp = new Label();
	Label lCtrlEsc = new Label();
	InfoLabel controlLabel = new InfoLabel("CONTROLS");
	ImageView ctrlUp = new ImageView("view/resources/keys/up.png");
	ImageView ctrlEsc = new ImageView("view/resources/keys/esc.png");

	lCtrlUp.setFont(new Font("Verdana", 18));
	lCtrlUp.setTextFill(Color.WHITE);
	lCtrlEsc.setFont(new Font("Verdana", 18));
	lCtrlEsc.setTextFill(Color.WHITE);

	lCtrlUp.setText("Holding up burns your fuel to slow you down. \n" + "You only have a limited amount of fuel.");
	lCtrlEsc.setText("Pressing escape cancels the current game \n" + " and opens the main menu.");

	boxUp.setSpacing(100);
	boxUp.setPadding(new Insets(0, 0, 0, 10));

	boxEsc.setSpacing(100);
	boxEsc.setPadding(new Insets(0, 0, 0, 10));

	box.setSpacing(50);
	box.setAlignment(Pos.CENTER);

	boxUp.getChildren().addAll(ctrlUp, lCtrlUp);
	boxEsc.getChildren().addAll(ctrlEsc, lCtrlEsc);

	box.getChildren().addAll(controlLabel, boxUp, boxEsc);
	controlsSubScene.getPane().getChildren().add(box);
    }

    private void createPlanetChooserSubScene() {
	planetChooserSubScene = new MoonlanderSubScene();
	mainPane.getChildren().add(planetChooserSubScene);

	InfoLabel choosePlanetLabel = new InfoLabel("CHOOSE A PLANET");
	choosePlanetLabel.setLayoutX(110);
	choosePlanetLabel.setLayoutY(25);
	planetChooserSubScene.getPane().getChildren().add(choosePlanetLabel);
	planetChooserSubScene.getPane().getChildren().add(createPlanetsToChoose());
    }

    private HBox createPlanetsToChoose() {
	HBox box = new HBox();
	box.setSpacing(20);
	planetList = new ArrayList<>();
	for (PLANET planet : PLANET.values()) {
	    PlanetPicker planetToPick = new PlanetPicker(planet);
	    planetList.add(planetToPick);
	    box.getChildren().add(planetToPick);
	    planetToPick.setOnMouseClicked(new EventHandler<MouseEvent>() {
		@Override
		public void handle(MouseEvent event) {
		    // TODO
		    if (planet.toString() == "MOON") {
			GameViewManager gameManager = new GameViewManager();
			gameManager.createNewGame(mainStage, 1);
		    } else if (planet.toString() == "VENUS") {
			GameViewManager gameManager = new GameViewManager();
			gameManager.createNewGame(mainStage, 2);
		    } else {
			GameViewManager gameManager = new GameViewManager();
			gameManager.createNewGame(mainStage, 3);
		    }
		}
	    });
	}
	box.setLayoutX(300 - (66 * 3));
	box.setLayoutY(100);
	return box;
    }

    public Stage getMainStage() {
	return mainStage;
    }

    private void createButtons() {
	Button btnStart = new Button("Select Level");
	Button btnControls = new Button("Show Controls");
	Button btnEnd = new Button("End Game");

	btnStart.setLayoutX(200);
	btnStart.setLayoutY(250);
	btnStart.setMinWidth(150);
	btnStart.setMinHeight(50);
	btnStart.setOnAction(new EventHandler<ActionEvent>() {

	    @Override
	    public void handle(ActionEvent event) {
		showSubScene(planetChooserSubScene);
	    }
	});

	btnControls.setLayoutX(200);
	btnControls.setLayoutY(350);
	btnControls.setMinWidth(150);
	btnControls.setMinHeight(50);
	btnControls.setOnAction(new EventHandler<ActionEvent>() {

	    @Override
	    public void handle(ActionEvent event) {
		showSubScene(controlsSubScene);
	    }
	});

	btnEnd.setLayoutX(200);
	btnEnd.setLayoutY(450);
	btnEnd.setMinWidth(150);
	btnEnd.setMinHeight(50);
	btnEnd.setOnAction(new EventHandler<ActionEvent>() {

	    @Override
	    public void handle(ActionEvent event) {
		mainStage.close();
	    }
	});

	mainPane.getChildren().addAll(btnStart, btnControls, btnEnd);
    }

    private void createBackground() {
	Image backgroundImage = new Image("view/resources/spacebg.png", 1024, 768, false, true);
	BackgroundImage background = new BackgroundImage(backgroundImage, BackgroundRepeat.REPEAT,
		BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, null);
	mainPane.setBackground(new Background(background));
    }

    private void createLogo() {
	ImageView logo = new ImageView("view/resources/Moonlander.png");
	logo.setLayoutX(200);
	logo.setLayoutY(50);
	mainPane.getChildren().add(logo);
    }
}

package de.hsh.projekt.common;

import javax.swing.JMenuBar;
import javax.swing.JPanel;

import javafx.embed.swing.JFXPanel;

public abstract class Game  {
    public abstract int start();
    public abstract int backToMain();
    
    public abstract KeyInput getKeyInput();
    public abstract MouseInput getMouseInput();
    
    public abstract String getName();
    public abstract String getDescription();
    
    public abstract JPanel getPanel();
    public abstract JFXPanel getJFXPanel();
    
    public abstract JMenuBar getJMenuBar();
}

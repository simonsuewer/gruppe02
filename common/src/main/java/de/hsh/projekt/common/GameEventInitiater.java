package de.hsh.projekt.common;

public class GameEventInitiater {
    
    private static GameEventListener listener;
    
    public static void addListener(GameEventListener l) {
	listener = l;
    }
    
    public static void renewJPanel() {
	listener.renewJPanel();
    }
    
    public static void exitGame() {
	listener.exitGame();
    }
    
    
}

package de.hsh.projekt.common;

public interface GameEventListener {
    public void renewJPanel();
    public void exitGame();
}

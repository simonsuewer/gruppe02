package de.hsh.projekt.common;

public class StaticSettings {
    /** The screen width. */
    public static final int SCREEN_WIDTH = 1024 + 16;

    /** The screen height. */
    public static final int SCREEN_HEIGHT = 768 + 50 + 16;
}

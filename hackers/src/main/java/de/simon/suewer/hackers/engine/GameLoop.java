/*
 * 
 */
package de.simon.suewer.hackers.engine;

import de.simon.suewer.hackers.grapics.Renderer;
import de.simon.suewer.hackers.world.World;

// TODO: Auto-generated Javadoc
/**
 * The Class GameLoop.
 */
public class GameLoop {

	/** The running. */
	private static boolean running = false;

	/** The updates. */
	private static int updates = 0;
	
	/** The Constant MAX_UPDATES. */
	private static final int MAX_UPDATES = 1;

	/** The last update time. */
	private static long lastUpdateTime = 0;

	/** The traget FPS. */
	private static int tragetFPS = 1;
	
	/** The target time. */
	private static int targetTime = 1000000000 / tragetFPS; // 10 sec in nano / fps

	/**
	 * Start.
	 */
	public static void start() {
		Thread tread = new Thread() {
			public void run() {

				running = true;

				lastUpdateTime = System.nanoTime();

				while (running) {

					long currentTime = System.nanoTime();

					updates = 0;

					while (currentTime - lastUpdateTime >= targetTime) {

						// Poll input

						// System.out.println("update");
						// update game
						World.update();
						lastUpdateTime += targetTime;
						updates++;

						if (updates > MAX_UPDATES) {
							break;
						}

					}

					// render
					Renderer.render();

					/*
					 * fps++; if(System.nanoTime() >= lastFpsCheck + 1000000000) {
					 * System.out.println(fps); fps = 0; lastFpsCheck = System.nanoTime(); }
					 */

					long timeTaken = System.nanoTime() - currentTime;

					if (timeTaken < targetTime) {
						try {
							Thread.sleep((targetTime - timeTaken) / 1000000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}


			}
		};
		tread.setName("GameLoop");
		tread.start();
	}

	/**
	 * Stop.
	 */
	public static void stop() {
		running = false;
	}
	
	public static boolean isRunning() {
		return running;
	}
}

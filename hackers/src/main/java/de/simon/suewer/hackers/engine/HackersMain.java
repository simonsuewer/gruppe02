/*
 * 
 */
package de.simon.suewer.hackers.engine;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JPanel;

import com.jogamp.opengl.awt.GLJPanel;

import de.hsh.projekt.common.Game;
import de.hsh.projekt.common.GameEventInitiater;
import de.simon.suewer.hackers.grapics.Renderer;
import de.simon.suewer.hackers.input.KeyInput;
import de.simon.suewer.hackers.input.MouseInput;
import de.simon.suewer.hackers.menu.MainMenu;
import de.simon.suewer.hackers.menu.StartMenu;
import de.simon.suewer.hackers.menu.UpgradeMenu;
import de.simon.suewer.hackers.model.Player;
import de.simon.suewer.hackers.sound.BackgroundMusic;
import de.simon.suewer.hackers.sound.TTS;
import javafx.embed.swing.JFXPanel;

// TODO: Auto-generated Javadoc
/**
 * The Class HackersMain.
 */
public class HackersMain extends Game {

    /** The Constant frame. */
    public static final JFrame frame = new JFrame(); // instantiation

    /** The Constant menu. */
    public static final UpgradeMenu menu = new UpgradeMenu();

    /** The level. */
    private static double level = 1;

    public static boolean sound = true;
    public static boolean tts = true;

    private static final MouseInput mouseInput = new MouseInput();
    private static final KeyInput keyInput = new KeyInput();

    /**
     * The HackersMain method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
	// add menu bar to frame

	init();

	HackersMain.frame.setSize(new Dimension(Renderer.screenWidth, Renderer.screenHeight + 50));
	HackersMain.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	HackersMain.frame.setJMenuBar(MainMenu.getMenuBar());
	HackersMain.frame.getContentPane().add(StartMenu.geLaunchMenu(), BorderLayout.CENTER);
	HackersMain.frame.addKeyListener(keyInput);
	HackersMain.frame.addMouseListener(mouseInput);
	HackersMain.frame.getJMenuBar().setVisible(true);
	HackersMain.frame.setVisible(true);

    }

    // init
    public static void init() {
	Renderer.init();

	BackgroundMusic.init();
	TTS.init();
    }

    /**
     * Start game.
     */
    public static void startGame() {
	/*
	 * HackersMain.frame.getContentPane().removeAll();
	 * HackersMain.frame.getContentPane().add(startGameAndReturnPanel(),
	 * BorderLayout.CENTER); HackersMain.frame.revalidate();
	 * HackersMain.frame.repaint();
	 */
	startGameAndReturnPanel();
	GameEventInitiater.renewJPanel();

    }

    /**
     * Start game and return panel
     */
    public static GLJPanel startGameAndReturnPanel() {

	GameLoop.start();

	if (sound) {
	    BackgroundMusic.play();
	}
	return Renderer.window;

    }

    public static void backToLaunchMenu() {

	GameLoop.stop();
	Player.renew();
	BackgroundMusic.stop();
	// Renderer.init();

	HackersMain.frame.getContentPane().removeAll();
	HackersMain.frame.getContentPane().add(StartMenu.geLaunchMenu(), BorderLayout.CENTER);

	HackersMain.frame.revalidate();
	HackersMain.frame.repaint();

	GameEventInitiater.renewJPanel();

    }

    /**
     * Start game.
     *
     * @param lvl the lvl
     */
    public static void startGame(double lvl) {
	setLevel(lvl);
	startGame();

    }

    /**
     * Open update dialog.
     */
    public static void openUpdateDialog() {
	menu.setVisible(true);
    }

    /**
     * Exit game.
     */
    public static void exitGame() {

	// System.exit(0);
	GameLoop.stop();
	Player.renew();
	BackgroundMusic.stop();
	BackgroundMusic.killALData();
	GameEventInitiater.exitGame();
    }

    /**
     * Gets the level.
     *
     * @return the level
     */
    public static double getLevel() {
	return level;
    }

    /**
     * Sets the level.
     *
     * @param level the new level
     */
    public static void setLevel(double level) {
	HackersMain.level = level;
    }

    @Override
    public int start() {
	init();
	return 0;
    }

    @Override
    public int backToMain() {
	GameLoop.stop();
	Player.renew();
	BackgroundMusic.stop();
	GameEventInitiater.exitGame();
	return 0;
    }

    @Override
    public KeyInput getKeyInput() {
	return keyInput;
    }

    @Override
    public MouseInput getMouseInput() {
	return mouseInput;
    }

    @Override
    public String getName() {
	return "Hackers";
    }

    @Override
    public String getDescription() {
	return StartMenu.getManual();
    }

    @Override
    public JPanel getPanel() {
	if (GameLoop.isRunning()) {
	    return Renderer.window;
	} else {
	    return StartMenu.geLaunchMenu();
	}

    }

    @Override
    public JMenuBar getJMenuBar() {
	return MainMenu.getMenuBar();
    }

    @Override
    public JFXPanel getJFXPanel() {
	// TODO Auto-generated method stub
	return null;
    }

}

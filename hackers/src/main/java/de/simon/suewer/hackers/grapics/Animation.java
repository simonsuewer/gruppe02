/*
 * 
 */
package de.simon.suewer.hackers.grapics;

import de.simon.suewer.hackers.resource.ImageResource;

// TODO: Auto-generated Javadoc
/**
 * The Class Animation.
 */
public class Animation {

	/** The frames. */
	//the frames of the animation
	public ImageResource[] frames;
	
	/** The current frame. */
	//the current frame of the animation
	public int currentFrame = 0;
	
	/** The fps. */
	//the FPS
	public int fps = 8;
	
	/** The last frame time. */
	private long lastFrameTime = 0;
	
	/** The loop. */
	//should we loop?
	public boolean loop = true;
	
	/**
	 * Play.
	 */
	public void play() {
		long currentTime = System.nanoTime();
		
		if(currentTime > lastFrameTime + 1000000000 / fps) {
			currentFrame++;
			
			if(currentFrame >=frames.length) {
				if(loop) {
					currentFrame = 0;
				} else {
					currentFrame --;
				}
			}
		}
	}
	
	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public ImageResource getImage() {
		return frames[currentFrame];
	}
	
}

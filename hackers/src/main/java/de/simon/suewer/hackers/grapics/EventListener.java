/*
 * 
 */
package de.simon.suewer.hackers.grapics;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;

import de.simon.suewer.hackers.world.World;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving event events.
 * The class that is interested in processing a event
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's <code>addEventListener<code> method. When
 * the event event occurs, that object's appropriate
 * method is invoked.
 *
 * @see EventEvent
 */
public class EventListener implements GLEventListener {

	/** The gl. */
	public static GL2 gl = null;

	/* (non-Javadoc)
	 * @see com.jogamp.opengl.GLEventListener#init(com.jogamp.opengl.GLAutoDrawable)
	 */
	@Override
	// start
	public void init(GLAutoDrawable drawable) {
		System.out.println("hello Worldv ini");
		gl = drawable.getGL().getGL2();
		
		gl.glClearColor(0, 0, 0, 1);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		

	}

	/* (non-Javadoc)
	 * @see com.jogamp.opengl.GLEventListener#dispose(com.jogamp.opengl.GLAutoDrawable)
	 */
	@Override
	// ende
	public void dispose(GLAutoDrawable drawable) {

		
	}

	/* (non-Javadoc)
	 * @see com.jogamp.opengl.GLEventListener#display(com.jogamp.opengl.GLAutoDrawable)
	 */
	@Override
	// jede sekunde
	public void display(GLAutoDrawable drawable) {
		GL2 gl = drawable.getGL().getGL2();

		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);

		World.render();

	}

	/* (non-Javadoc)
	 * @see com.jogamp.opengl.GLEventListener#reshape(com.jogamp.opengl.GLAutoDrawable, int, int, int, int)
	 */
	@Override
	// gröse verändern
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		GL2 gl = drawable.getGL().getGL2();
	
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();

		float unitstall = Renderer.getWindowHeight() / (Renderer.getWindowWidth() / Renderer.unitsWide);

		// Fenstergröße!
		gl.glOrtho(-Renderer.unitsWide / 2, Renderer.unitsWide / 2, -unitstall / 2, unitstall / 2, -1, 1);
		gl.glMatrixMode(GL2.GL_MODELVIEW);

	}

}

/*
 * 
 */
package de.simon.suewer.hackers.grapics;

import java.util.concurrent.ThreadLocalRandom;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.texture.Texture;

import de.simon.suewer.hackers.model.Network;
import de.simon.suewer.hackers.resource.ImageResource;
import de.simon.suewer.hackers.world.GameObject;
import de.simon.suewer.hackers.world.World;

// TODO: Auto-generated Javadoc
/**
 * The Class Graphics.
 */
public class Graphics {

	/** The red. */
	// Color values
	private static float red = 1;

	/** The green. */
	private static float green = 1;

	/** The blue. */
	private static float blue = 1;

	/** The alpha. */
	private static float alpha = 1;

	/** The rotation. */
	// Rotation in degrees
	private static float rotation = 0;

	/**
	 * Draw image.
	 *
	 * @param image  the image
	 * @param x      the x
	 * @param y      the y
	 * @param width  the width
	 * @param height the height
	 */
	public static void drawImage(ImageResource image, float x, float y, float width, float height) {
		GL2 gl = EventListener.gl;

		Texture tex = image.getTexture();

		if (tex != null) {
			gl.glBindTexture(GL2.GL_TEXTURE_2D, tex.getTextureObject());
		}

	

		gl.glTranslatef(x, y, 0);
		gl.glRotatef(-rotation, 0, 0, 1);

		gl.glColor4f(red, green, blue, alpha);
		gl.glBegin(GL2.GL_QUADS);
		{
			gl.glTexCoord2f(0, 1);
			gl.glVertex2f(-width / 2, -height / 2);

			gl.glTexCoord2f(1, 1);
			gl.glVertex2f(width / 2, -height / 2);

			gl.glTexCoord2f(1, 0);
			gl.glVertex2f(width / 2, height / 2);

			gl.glTexCoord2f(0, 0);
			gl.glVertex2f(-width / 2, height / 2);
		}
		gl.glEnd();
		gl.glFlush();

		gl.glBindTexture(GL2.GL_TEXTURE_2D, 0);

		gl.glRotatef(rotation, 0, 0, 1);
		gl.glTranslatef(-x, -y, 0);
	}

	/**
	 * Gets the network position.
	 *
	 * @param image the image
	 * @param maxX  the max X
	 * @param maxY  the max Y
	 * @return the network position
	 */
	public static GameObject getNetworkPosition(ImageResource image, int maxX, int maxY) {

		if (maxX < 1) {
			maxX = World.WORLD_WIDTH;
		}
		if (maxY < 1) {
			maxY = World.WORLD_HIGHT;
		}

		int counter = 0;
		do {

			int x = ThreadLocalRandom.current().nextInt(maxX);
			int y = ThreadLocalRandom.current().nextInt(maxY);

			try {
				int clr = image.getImage().getRGB(x, y);
				int red = (clr & 0x00ff0000) >> 16;
				// int green = (clr & 0x0000ff00) >> 8;
				// int blue = clr & 0x000000ff;
				// System.out.println(red + " -- " + green + " -- " + blue);
				if (red > 1) {
					y = Renderer.screenHeight / 2 - y;
					y = y - 130;
					x = Renderer.screenWidth / 2 - x;
					x = x * (-1);
					return new GameObject(x, y);
				}

			} catch (ArrayIndexOutOfBoundsException e) {
				System.out.print("out of bounce");

			}
			counter++;
			if (counter == 10) {
				System.out.println("ERROR");
				return getNetworkPosition(image, -1, -1);
			}
		} while (true);

	}

	/**
	 * Gets the network position.
	 *
	 * @param image the image
	 * @param n     the n
	 * @return the network position
	 */
	public static GameObject getNetworkPosition(ImageResource image, Network n) {
		int x = (int) n.x * (-1);
		x = x - Renderer.screenWidth / 2;
		x = x * (-1);

		int y = (int) n.y + 130;
		y = y - Renderer.screenHeight / 2;
		y = y * (-1);

		return getNetworkPosition(image, (int) (x - n.width), (int) (y - n.height));
	}

	/**
	 * Test length.
	 *
	 * @param x  the x
	 * @param x2 the x 2
	 * @return the int
	 */
	public static int testLength(int x, int x2) {
		x = x - Renderer.screenWidth / 2;
		x = x * (-1);
		x2 = x2 - Renderer.screenWidth / 2;
		x2 = x2 * (-1);

		if (x2 > x) {
			return x2 - x;
		} else {
			return x - x2;
		}
	}

	/**
	 * Fill rect.
	 *
	 * @param x      the x
	 * @param y      the y
	 * @param width  the width
	 * @param height the height
	 */
	public static void fillRect(float x, float y, float width, float height) {
		GL2 gl = EventListener.gl;

		gl.glTranslatef(x, y, 0);
		gl.glRotatef(-rotation, 0, 0, 1);

		gl.glColor4f(red, green, blue, alpha);
		gl.glBegin(GL2.GL_QUADS);
		{
			gl.glVertex2f(-width / 2, -height / 2);
			gl.glVertex2f(width / 2, -height / 2);
			gl.glVertex2f(width / 2, height / 2);
			gl.glVertex2f(-width / 2, height / 2);
		}
		gl.glEnd();
		gl.glFlush();

		gl.glRotatef(rotation, 0, 0, 1);
		gl.glTranslatef(-x, -y, 0);
	}

	/**
	 * Prints the line.
	 *
	 * @param fx  the fx
	 * @param fy  the fy
	 * @param tox the tox
	 * @param toy the toy
	 */
	public static void printLine(float fx, float fy, float tox, float toy) {
		GL2 gl = EventListener.gl;
		gl.glBegin(GL2.GL_LINES);// static field
		gl.glVertex2f(fx, fy);
		gl.glVertex2f(tox, toy);
		gl.glEnd();
		gl.glFlush();

	}

	/**
	 * Sets the color.
	 *
	 * @param r the r
	 * @param g the g
	 * @param b the b
	 * @param a the a
	 */
	public static void setColor(float r, float g, float b, float a) {
		red = Math.max(0, Math.min(1, r));
		green = Math.max(0, Math.min(1, g));
		blue = Math.max(0, Math.min(1, b));
		alpha = Math.max(0, Math.min(1, a));
	}

	/**
	 * Sets the rotation.
	 *
	 * @param r the new rotation
	 */
	public static void setRotation(float r) {
		rotation = r;
	}
}

/*
 * 
 */
package de.simon.suewer.hackers.grapics;

import javax.swing.JPanel;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;

import de.simon.suewer.hackers.input.KeyInput;
import de.simon.suewer.hackers.input.MouseInput;

// TODO: Auto-generated Javadoc
/**
 * The Class Renderer.
 */
public class Renderer {
	
	/** The window. */
	public static GLJPanel window = null;

	/** The screen width. */
	public static int screenWidth = 1024;
	
	/** The screen height. */
	public static int screenHeight = 768;

	/** The units wide. */
	public static float unitsWide = 1024;

	/** The j panel. */
	public static JPanel jPanel = null;
	
	/** The profile. */
	private static GLProfile profile = null;

	/**
	 * Inits the.
	 */
	public static void init() {
	 
		GLProfile.initSingleton();
		profile = GLProfile.get(GLProfile.GL2);

		GLCapabilities caps = new GLCapabilities(profile);
		window = new GLJPanel(caps);
		// window = GLWindow.create(caps);
		window.setSize(screenWidth, screenHeight);
		// window.setResizable(false);

		// listener -->
		window.addGLEventListener(new EventListener());
		//window.addMouseListener(new MouseInput());
		//window.addKeyListener(new KeyInput());

		window.requestFocus(true);

	}

	/**
	 * Exit.
	 */
	public static void exit() {
		window.destroy();
	}

	/**
	 * Render.
	 */
	public static void render() {
		if (window == null) {
			return;
		}
		window.display();
	}

	/**
	 * Gets the window width.
	 *
	 * @return the window width
	 */
	public static int getWindowWidth() {
		return window.getWidth();
	}

	/**
	 * Gets the window height.
	 *
	 * @return the window height
	 */
	public static int getWindowHeight() {
		return window.getHeight();
	}

	/**
	 * Gets the profile.
	 *
	 * @return the profile
	 */
	public static GLProfile getProfile() {
		return profile;
	}

}

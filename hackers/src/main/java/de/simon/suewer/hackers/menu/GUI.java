/*
 * 
 */
package de.simon.suewer.hackers.menu;

import java.awt.Color;
import java.awt.Font;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import com.jogamp.opengl.util.awt.TextRenderer;

import de.simon.suewer.hackers.engine.HackersMain;
import de.simon.suewer.hackers.grapics.Graphics;
import de.simon.suewer.hackers.grapics.Renderer;
import de.simon.suewer.hackers.model.Network;
import de.simon.suewer.hackers.model.Player;
import de.simon.suewer.hackers.world.GameObject;
import de.simon.suewer.hackers.world.World;

// TODO: Auto-generated Javadoc
/**
 * The Class GUI.
 */
public class GUI extends GameObject {

	/** The font height. */
	private static int fontHeight = 20;
	
	/** The font between. */
	private static int fontBetween = 25;

	/** The update gui open. */
	public static boolean updateGuiOpen = false;

	/** The log message. */
	private static String logMessage = "";
	
	/** The df. */
	private static DateFormat df = new SimpleDateFormat("dd.MM.yyyy");

	{
		x = 0;
		y = 0;
		height = Renderer.screenHeight - 200;
		width = Renderer.screenWidth - 100;
	}

	/* (non-Javadoc)
	 * @see de.simon.suewer.hackers.world.GameObject#render()
	 */
	public void render() {
		renderUpdateGui();
		renderText();
		renderGameEndMessage();
	}

	/**
	 * Render update gui.
	 */
	private void renderUpdateGui() {
		if (updateGuiOpen) {
			Graphics.setColor(1, 1, 0, 1);
			Graphics.fillRect(x, y, width, height);
			Graphics.setColor(1, 1, 1, 1);
		}

	}

	/**
	 * Render text.
	 */
	private void renderText() {

		TextRenderer textRenderer = new TextRenderer(new Font("Verdana", Font.BOLD, fontHeight));
		textRenderer.beginRendering(Renderer.screenWidth, Renderer.screenHeight);
		textRenderer.setColor(Color.WHITE);
		textRenderer.setSmoothing(true);

		textRenderer.draw(Player.getName() + " - Lvl : " + HackersMain.getLevel(), 10, Renderer.screenHeight - fontBetween);
		textRenderer.draw("Money : " + Player.getMoney(), 10, Renderer.screenHeight - fontBetween * 2);

		textRenderer.draw("Virus Mode:" + World.gamePercent + "%", Renderer.screenWidth / 2,
				Renderer.screenHeight - fontBetween);
		textRenderer.draw("Mouseclick for Uprade", Renderer.screenWidth / 2, Renderer.screenHeight - fontBetween * 2);

		textRenderer.setColor(Color.WHITE);
		textRenderer.draw(df.format(World.getDate()) + " : " + logMessage, 10, fontBetween);
		textRenderer.endRendering();
	}

	/**
	 * Render game end message.
	 */
	public void renderGameEndMessage() {
		if (World.gamePercent == 100) {
			TextRenderer textRenderer = new TextRenderer(new Font("Verdana", Font.BOLD, fontHeight * 5));
			textRenderer.beginRendering(Renderer.screenWidth, Renderer.screenHeight);
			textRenderer.setColor(Color.GREEN);
			textRenderer.setSmoothing(true);

			textRenderer.draw("Win", Renderer.screenWidth / 2, Renderer.screenHeight / 2);
			textRenderer.endRendering();
		}

		if (Network.getPercentOccupied() == 0 && !Network.getStaticNetwork().isOccupied()) {
			TextRenderer textRenderer = new TextRenderer(new Font("Verdana", Font.BOLD, fontHeight * 5));
			textRenderer.beginRendering(Renderer.screenWidth, Renderer.screenHeight);
			textRenderer.setColor(Color.RED);
			textRenderer.setSmoothing(true);

			textRenderer.draw("Game Over", Renderer.screenWidth / 9, Renderer.screenHeight / 2);
			textRenderer.endRendering();
		}
	}

	/**
	 * Update.
	 *
	 * @param message the message
	 */
	public static void update(String message) {
		logMessage = message;
	}
}

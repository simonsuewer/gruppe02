/*
 * 
 */
package de.simon.suewer.hackers.menu;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import de.simon.suewer.hackers.menu.MainMenuActionListener.MainMenuStartActionListener;

// TODO: Auto-generated Javadoc
/**
 * The Class MainMenu.
 */
public class MainMenu {

	/** The menu bar. */
	// Where the GUI is created:
	private static JMenuBar menuBar;

	/** The menu. */
	private static JMenu soundMenu;

	/** The gamne menu. */
	private static JMenu gamneMenu;

	/** The menu item. */
	private static JMenuItem menuItem;

	static {
		createMenuBar();
	}

	/**
	 * Creates the menu bar.
	 */
	private static void createMenuBar() {
		// Create the menu bar.
		menuBar = new JMenuBar();

		// Build the sound menu.
		soundMenu = new JMenu("Musik");
		soundMenu.setMnemonic(KeyEvent.VK_F);
		soundMenu.getAccessibleContext().setAccessibleDescription("Sound menu");
		menuBar.add(soundMenu);

		// Build the game menu.
		gamneMenu = new JMenu("Spiel");
		gamneMenu.setMnemonic(KeyEvent.VK_F);
		gamneMenu.getAccessibleContext().setAccessibleDescription("Game menu");
		menuBar.add(gamneMenu);

		// JMenuItems show the menu items
		// menuItem = new JMenuItem("New", new ImageIcon("images/new.gif"));
		menuItem = new JMenuItem("Play");
		menuItem.setMnemonic(KeyEvent.VK_N);
		menuItem.addActionListener(new MainMenuActionListener().new MainMenuMusikPlayActionListener());
		soundMenu.add(menuItem);

		// add a separator
		soundMenu.addSeparator();

		menuItem = new JMenuItem("Pause");
		menuItem.addActionListener(new MainMenuActionListener().new MainMenuMusikPauseActionListener());
		menuItem.setMnemonic(KeyEvent.VK_P);
		soundMenu.add(menuItem);

		menuItem = new JMenuItem("Stop");
		menuItem.addActionListener(new MainMenuActionListener().new MainMenuMusikStopActionListener());
		menuItem.setMnemonic(KeyEvent.VK_E);
		soundMenu.add(menuItem);

		menuItem = new JMenuItem("Gott spielen");
		menuItem.addActionListener(new MainMenuActionListener().new MainMenuEasterActionListener());
		soundMenu.add(menuItem);
		
		menuItem = new JMenuItem("Zurück zum Hauptmenu");
		menuItem.addActionListener(new MainMenuActionListener().new MainMenuBackToMenuActionListener());
		gamneMenu.add(menuItem);
		
		menuItem = new JMenuItem("Pause");
		menuItem.addActionListener(new MainMenuActionListener().new MainMenuGamePauseActionListener());
		gamneMenu.add(menuItem);
		
		menuItem = new JMenuItem("Weiter");
		menuItem.addActionListener(new MainMenuActionListener().new MainMenuGameStartActionListener());
		gamneMenu.add(menuItem);
	}

	/**
	 * Gets the menu bar.
	 *
	 * @return the menu bar
	 */
	public static JMenuBar getMenuBar() {
	    if(menuBar == null) {
		createMenuBar();
	    }
		return menuBar;
	}

	/**
	 * Gets the gamne menu.
	 *
	 * @return the gamne menu
	 */
	public static JMenu getGamneMenu() {
		return gamneMenu;
	}

	/**
	 * Sets the gamne menu.
	 *
	 * @param gamneMenu the new gamne menu
	 */
	public static void setGamneMenu(JMenu gamneMenu) {
		MainMenu.gamneMenu = gamneMenu;
	}

}

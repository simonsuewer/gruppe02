/*
 * 
 */
package de.simon.suewer.hackers.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.simon.suewer.hackers.engine.GameLoop;
import de.simon.suewer.hackers.engine.HackersMain;
import de.simon.suewer.hackers.grapics.Renderer;
import de.simon.suewer.hackers.sound.BackgroundMusic;
import de.simon.suewer.hackers.world.World;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving mainMenuAction events. The class that is
 * interested in processing a mainMenuAction event implements this interface,
 * and the object created with that class is registered with a component using
 * the component's <code>addMainMenuActionListener<code> method. When the
 * mainMenuAction event occurs, that object's appropriate method is invoked.
 *
 * @see MainMenuActionEvent
 */
public class MainMenuActionListener {

    /**
     * The listener interface for receiving mainMenuStartAction events. The class
     * that is interested in processing a mainMenuStartAction event implements this
     * interface, and the object created with that class is registered with a
     * component using the component's <code>addMainMenuStartActionListener<code>
     * method. When the mainMenuStartAction event occurs, that object's appropriate
     * method is invoked.
     *
     * @see MainMenuStartActionEvent
     */
    public class MainMenuStartActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    StartMenu.openStartPopup();

	}

    }

    /**
     * The listener interface for receiving mainMenuSettingSoundAction events. The
     * class that is interested in processing a mainMenuSettingSoundAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuSettingSoundActionListener<code> method. When the
     * mainMenuSettingSoundAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuSettingSoundActionEvent
     */
    public class MainMenuSettingSoundActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    HackersMain.sound = !HackersMain.sound;
	    StartMenu.closeSetinngPopup();

	}

    }

    /**
     * The listener interface for receiving mainMenuSettingTTSAction events. The
     * class that is interested in processing a mainMenuSettingTTSAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuSettingTTSActionListener<code> method. When the
     * mainMenuSettingTTSAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuSettingTTSActionEvent
     */
    public class MainMenuSettingTTSActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    HackersMain.tts = !HackersMain.tts;
	    StartMenu.closeSetinngPopup();

	}

    }

    /**
     * The listener interface for receiving mainMenuStartEasyAction events. The
     * class that is interested in processing a mainMenuStartEasyAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuStartEasyActionListener<code> method. When the
     * mainMenuStartEasyAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuStartEasyActionEvent
     */
    public class MainMenuStartEasyActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    HackersMain.startGame(1);
	    StartMenu.closeStartPopup();
	}

    }

    /**
     * The listener interface for receiving mainMenuStartMediumAction events. The
     * class that is interested in processing a mainMenuStartMediumAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuStartMediumActionListener<code> method. When the
     * mainMenuStartMediumAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuStartMediumActionEvent
     */
    public class MainMenuStartMediumActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    HackersMain.startGame(2);

	    StartMenu.closeStartPopup();

	}

    }

    /**
     * The listener interface for receiving mainMenuStartHardAction events. The
     * class that is interested in processing a mainMenuStartHardAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuStartHardActionListener<code> method. When the
     * mainMenuStartHardAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuStartHardActionEvent
     */
    public class MainMenuStartHardActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    HackersMain.startGame(4);

	    StartMenu.closeStartPopup();
	}

    }

    /**
     * The listener interface for receiving mainMenuExitAction events. The class
     * that is interested in processing a mainMenuExitAction event implements this
     * interface, and the object created with that class is registered with a
     * component using the component's <code>addMainMenuExitActionListener<code>
     * method. When the mainMenuExitAction event occurs, that object's appropriate
     * method is invoked.
     *
     * @see MainMenuExitActionEvent
     */
    public class MainMenuExitActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    HackersMain.exitGame();
	}

    }

    /**
     * The listener interface for receiving mainMenuSettingsAction events. The class
     * that is interested in processing a mainMenuSettingsAction event implements
     * this interface, and the object created with that class is registered with a
     * component using the component's <code>addMainMenuSettingsActionListener<code>
     * method. When the mainMenuSettingsAction event occurs, that object's
     * appropriate method is invoked.
     *
     * @see MainMenuSettingsActionEvent
     */
    public class MainMenuSettingsActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    StartMenu.openSettingPopup();
	}

    }

    /**
     * The listener interface for receiving mainMenuMusikPlayAction events. The
     * class that is interested in processing a mainMenuMusikPlayAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuMusikPlayActionListener<code> method. When the
     * mainMenuMusikPlayAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuMusikPlayActionEvent
     */
    public class MainMenuMusikPlayActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    BackgroundMusic.play();
	}

    }

    /**
     * The listener interface for receiving mainMenuMusikStopAction events. The
     * class that is interested in processing a mainMenuMusikStopAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuMusikStopActionListener<code> method. When the
     * mainMenuMusikStopAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuMusikStopActionEvent
     */
    public class MainMenuMusikStopActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    BackgroundMusic.stop();
	}

    }

    /**
     * The listener interface for receiving mainMenuMusikPauseAction events. The
     * class that is interested in processing a mainMenuMusikPauseAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuMusikPauseActionListener<code> method. When the
     * mainMenuMusikPauseAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuMusikPauseActionEvent
     */
    public class MainMenuMusikPauseActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    BackgroundMusic.pause();
	}

    }

    /**
     * The listener interface for receiving mainMenuEasterAction events. The class
     * that is interested in processing a mainMenuEasterAction event implements this
     * interface, and the object created with that class is registered with a
     * component using the component's <code>addMainMenuEasterActionListener<code>
     * method. When the mainMenuEasterAction event occurs, that object's appropriate
     * method is invoked.
     *
     * @see MainMenuEasterActionEvent
     */
    public class MainMenuEasterActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    World.toggleEasterEgg();
	}

    }

    /**
     * The listener interface for receiving mainMenuBackToMenuAction events. The
     * class that is interested in processing a mainMenuBackToMenuAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuBackToMenuActionListener<code> method. When the
     * mainMenuBackToMenuAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuBackToMenuActionEvent
     */
    public class MainMenuBackToMenuActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    HackersMain.backToLaunchMenu();
	}

    }

    /**
     * The listener interface for receiving mainMenuGamePauseAction events. The
     * class that is interested in processing a mainMenuGamePauseAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuGamePauseActionListener<code> method. When the
     * mainMenuGamePauseAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuGamePauseActionEvent
     */
    public class MainMenuGamePauseActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    GameLoop.stop();
	    Renderer.render();
	}

    }

    /**
     * The listener interface for receiving mainMenuGameStartAction events. The
     * class that is interested in processing a mainMenuGameStartAction event
     * implements this interface, and the object created with that class is
     * registered with a component using the component's
     * <code>addMainMenuGameStartActionListener<code> method. When the
     * mainMenuGameStartAction event occurs, that object's appropriate method is
     * invoked.
     *
     * @see MainMenuGameStartActionEvent
     */
    public class MainMenuGameStartActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    GameLoop.start();
	}

    }
}

/*
 * 
 */
package de.simon.suewer.hackers.menu;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;

// TODO: Auto-generated Javadoc
/**
 * The Class StartMenu.
 */
public class StartMenu {

	/** The menu. */
	private static JPanel menu;

	/** The popup menu. */
	private static JPopupMenu popupMenu = new JPopupMenu();

	/** The popup settings menu. */
	private static JPopupMenu popupSettingsMenu = new JPopupMenu();

	/** The btn start. */
	private static JButton btnStart = new JButton("Start");

	/** The btn settings. */
	private static JButton btnSettings = new JButton("Einstellungen");
	/**
	 * Create the application.
	 */
	static {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * 
	 * @wbp.parser.entryPoint
	 */
	private static void initialize() {
		menu = new JPanel();
		menu.setBackground(Color.GRAY);

		btnStart.setBounds(200, 87, 200, 100);
		btnStart.setIcon(new ImageIcon(StartMenu.class.getClassLoader().getResource("img/baseline_power_settings_new_black_18dp.png")));
		btnStart.setForeground(Color.BLACK);
		btnStart.setBackground(Color.GREEN);
		btnStart.addActionListener(new MainMenuActionListener().new MainMenuStartActionListener());

		JPanel panel = new JPanel();
		popupMenu.add(panel);
		panel.setLayout(new GridLayout(1, 0, 0, 0));

		JButton btnStart1 = new JButton("Easy");
		btnStart1.addActionListener(new MainMenuActionListener().new MainMenuStartEasyActionListener());
		panel.add(btnStart1);

		JButton btnStart2 = new JButton("Medium");
		btnStart2.addActionListener(new MainMenuActionListener().new MainMenuStartMediumActionListener());
		panel.add(btnStart2);

		JButton btnStart3 = new JButton("Hard");
		btnStart3.addActionListener(new MainMenuActionListener().new MainMenuStartHardActionListener());
		panel.add(btnStart3);
		menu.setLayout(null);
		menu.add(btnStart);

		JLabel manuelLabel = new JLabel(getManual(), SwingConstants.CENTER);
		manuelLabel.setBounds(613, 87, 344, 622);
		manuelLabel.setForeground(Color.WHITE);
		menu.add(manuelLabel);

		btnSettings.setBounds(200, 344, 200, 100);
		btnSettings.setBackground(Color.GREEN);
		btnSettings.addActionListener(new MainMenuActionListener().new MainMenuSettingsActionListener());
		btnSettings.setIcon(new ImageIcon(StartMenu.class.getClassLoader().getResource("img/baseline_build_black_18dp.png")));
		menu.add(btnSettings);

		JPanel panelSettings = new JPanel();
		popupSettingsMenu.add(panelSettings);
		panelSettings.setLayout(new GridLayout(1, 0, 0, 0));

		JToggleButton toggleSound = new JToggleButton("Sound", true);
		toggleSound.addActionListener(new MainMenuActionListener().new MainMenuSettingSoundActionListener());
		panelSettings.add(toggleSound);

		JToggleButton toggleTTS = new JToggleButton("TTS", true);
		toggleTTS.addActionListener(new MainMenuActionListener().new MainMenuSettingTTSActionListener());
		panelSettings.add(toggleTTS);

		JButton btnExit = new JButton("Beenden");
		btnExit.setIcon(new ImageIcon(StartMenu.class.getClassLoader().getResource("img/baseline_highlight_off_black_18dp.png")));
		btnExit.setBounds(200, 596, 200, 100);
		btnExit.addActionListener(new MainMenuActionListener().new MainMenuExitActionListener());
		menu.add(btnExit);
	}

	/**
	 * Ge launch menu.
	 *
	 * @return the j panel
	 */
	public static JPanel geLaunchMenu() {
		return menu;
	}

	/**
	 * Open start popup.
	 */
	public static void openStartPopup() {
		popupMenu.show(btnStart, 0, 0);
	}

	/**
	 * Open Setting popup.
	 */
	public static void openSettingPopup() {
		popupSettingsMenu.show(btnSettings, 0, 0);
	}

	/**
	 * Close Setting popup.
	 */
	public static void closeSetinngPopup() {
		popupSettingsMenu.setVisible(false);
	}

	/**
	 * Close start popup.
	 */
	public static void closeStartPopup() {
		popupMenu.setVisible(false);
	}

	/**
	 * Gets the manual.
	 *
	 * @return the manual
	 */
	public static String getManual() {
		return "<html>Hackers. ist ein Strategiesimulationsspiel, <br> bei dem der Spieler indirekt ein Virus kontrolliert "
				+ " Der Spieler kann zwischen verschiedenen Schwirigkeitsgraden und Virenupgrades wählen."
				+ " Es besteht jedoch Zeitdruck, das Spiel zu beenden, bevor der Mensch, der Gegner, ein Schutz gegen den Virus entwickelt hat. </html>";
	}
}

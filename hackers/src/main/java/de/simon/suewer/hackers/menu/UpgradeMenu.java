/*
 * 
 */
package de.simon.suewer.hackers.menu;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import de.simon.suewer.hackers.engine.HackersMain;
import de.simon.suewer.hackers.model.Player;
import de.simon.suewer.hackers.model.Virus;

// TODO: Auto-generated Javadoc
/**
 * The Class UpgradeMenu.
 */
public class UpgradeMenu extends JDialog {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The content panel. */
    private final JPanel contentPanel = new JPanel();

    /** The btn virus. */
    private JButton btnVirus = new JButton("Virus");

    /** The btn buyrebuy. */
    private JButton btnBuyrebuy = new JButton("-");

    /** The button list. */
    private Map<JButton, JButton> upgradebuttonMap = new HashMap<JButton, JButton>();

    /** The selected virus. */
    private static Virus selectedVirus = new Virus();

    /** The virus info. */
    private JLabel virusInfo;

    /** The virusinfo label. */
    private JLabel virusinfoLabel;

    /**
     * Create the dialog.
     */
    public UpgradeMenu() {
	setBounds(100, 100, 750, 473);
	getContentPane().setLayout(new BorderLayout());
	contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
	getContentPane().add(contentPanel, BorderLayout.CENTER);
	contentPanel.setLayout(null);
	setUndecorated(true);

	initButton();

	btnVirus.setBounds(18, 181, 76, 29);
	btnVirus.setBackground(Color.GREEN);
	contentPanel.add(btnVirus);

	JPanel panel = new JPanel();
	panel.setBounds(400, 15, 344, 378);
	panel.setBackground(Color.DARK_GRAY);
	contentPanel.add(panel);
	panel.setLayout(null);

	virusInfo = new JLabel(selectedVirus.getInfo(), SwingConstants.CENTER);
	virusInfo.setForeground(Color.WHITE);
	virusInfo.setBounds(6, 6, 332, 16);

	panel.add(virusInfo);

	virusinfoLabel = new JLabel(selectedVirus.getName(), SwingConstants.CENTER);
	virusinfoLabel.setForeground(Color.WHITE);
	virusinfoLabel.setBounds(6, 61, 332, 247);
	panel.add(virusinfoLabel);

	btnBuyrebuy.setBounds(6, 335, 149, 29);
	btnBuyrebuy.addActionListener(new UpgradeMenuActionListener().new BuyActionListener());
	panel.add(btnBuyrebuy);

	{
	    JPanel buttonPane = new JPanel();
	    buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
	    getContentPane().add(buttonPane, BorderLayout.SOUTH);
	    {
		/*
		 * JButton okButton = new JButton("OK"); okButton.setActionCommand("OK");
		 * buttonPane.add(okButton); getRootPane().setDefaultButton(okButton);
		 */
	    }
	    {
		JButton cancelButton = new JButton("Cancel");
		cancelButton.setActionCommand("Cancel");
		buttonPane.add(cancelButton);
		cancelButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
			dispose();
		    }
		});

	    }
	}

	setColorOfButton();
    }
    
    
    private void initButton() {
	upgradebuttonMap = new HashMap<JButton, JButton>();
	for (int i = 1; i <= 6; i++) {
	    String name = "";
	    switch (i) {
	    case 1:
		name = "Bootsektor";
		break;

	    case 2:
		name = "Computerwurm";
		break;

	    case 3:
		name = "Makrovirus";
		break;

	    case 4:
		name = "Programmvirus";
		break;

	    case 5:
		name = "Scriptvirus";
		break;

	    case 6:
		name = "Trojaner";
		break;
	    }

	    int y = 15 + 76 * (i - 1);
	    JButton virus = new JButton(name);
	    virus.setBounds(121, y, 130, 29);
	    contentPanel.add(virus);
	    virus.addActionListener(new UpgradeMenuActionListener().new VirusUpgradeSelectActionListener(i));

	    Virus v = new Virus();
	    v.setName(name);
	    JButton upgrade = new JButton("+1 (" + (Player.getCountVirus(v)+1) + " aktiv)");
	    upgrade.setBounds(280, y, 100, 29);
	    contentPanel.add(upgrade);
	    upgrade.addActionListener(new UpgradeMenuActionListener().new VirusUpgradeActionListener(i));
	    upgradebuttonMap.put(virus, upgrade);
	    upgrade.setVisible(false);
	}
    }
    
    /*
     * (non-Javadoc)
     * 
     * @see java.awt.Window#paint(java.awt.Graphics)
     */
    @Override
    public void paint(final Graphics g) {

	super.paint(g);

	Graphics2D g2 = (Graphics2D) g;

	Point p = SwingUtilities.convertPoint(btnVirus, 0, 0, this);

	for (JButton b : upgradebuttonMap.keySet()) {
	    Point p1 = SwingUtilities.convertPoint(b, 0, 0, this);
	    g2.drawLine(p.x + 30, p.y + 15, p1.x + 5, p1.y + 15);
	    
	    for (Virus v : Player.getViren()) {
		if (b.getText().equals(v.getName())) {
		    Point p2 = SwingUtilities.convertPoint(upgradebuttonMap.get(b), 0, 0, this);
		    g2.drawLine(p1.x + 30 , p1.y + 15, p2.x + 5, p2.y + 15);
		}
	    }
	}

    }

    /**
     * Sets the virus.
     *
     * @param e the new virus
     */
    public void setVirus(Virus e) {
	selectedVirus = e;
	virusInfo.setText("<html>" + e.getName() + "</html>");
	virusinfoLabel.setText("<html>" + e.getInfo() + "</html>");
	btnBuyrebuy.setText("Preis: " + e.getPrice() * HackersMain.getLevel());

	btnBuyrebuy.setEnabled(true);

	if (e.getPrice() * HackersMain.getLevel() > Player.getMoney()) {
	    btnBuyrebuy.setEnabled(false);
	}
	for (Virus virus : Player.getViren()) {
	    System.out.println(Player.getViren().size() + "  virus gekauft" + virus.getName());
	    if (virus.getName().equals(e.getName())) {
		// btnBuyrebuy.setEnabled(false);
	    }
	}
	revalidate();
    }

    /**
     * Gets the virus.
     *
     * @return the virus
     */
    public static Virus getVirus() {
	return selectedVirus;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.awt.Component#revalidate()
     */
    @Override
    public void revalidate() {
	//super.repaint();
	setColorOfButton();
	
	super.revalidate();
    }

    /**
     * Sets the color of button.
     */
    private void setColorOfButton() {
	btnVirus.setBackground(Color.GREEN);
	btnVirus.setOpaque(true);
	btnVirus.setBorderPainted(false);
	for (JButton b : upgradebuttonMap.keySet()) {

	    for (Virus v : Player.getViren()) {
		System.out.println("check " + b.getText() + "  " + v.getName());
		if (b.getText().equals(v.getName())) {
		    System.out.println("check " + b.getText() + "  " + v.getName() + " --_> true");
		    b.setBackground(Color.GREEN);
		    b.setOpaque(true);
		    b.setBorderPainted(false);
		    b.setEnabled(false);
		    upgradebuttonMap.get(b).setVisible(true);
		    upgradebuttonMap.get(b).setText( "+1 (" + (Player.getCountVirus(v)) + " aktiv)");
		   
		}
	    }

	}

    }

}

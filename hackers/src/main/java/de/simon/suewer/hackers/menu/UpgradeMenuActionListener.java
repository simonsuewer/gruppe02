/*
 * 
 */
package de.simon.suewer.hackers.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.simon.suewer.hackers.engine.HackersMain;
import de.simon.suewer.hackers.model.Player;
import de.simon.suewer.hackers.model.Virus;
import de.simon.suewer.hackers.staticmodel.Bootsektor;
import de.simon.suewer.hackers.staticmodel.Computerwurm;
import de.simon.suewer.hackers.staticmodel.Makrovirus;
import de.simon.suewer.hackers.staticmodel.Programmviren;
import de.simon.suewer.hackers.staticmodel.Scriptviren;
import de.simon.suewer.hackers.staticmodel.Trojaner;

// TODO: Auto-generated Javadoc
/**
 * The listener interface for receiving upgradeMenuAction events. The class that
 * is interested in processing a upgradeMenuAction event implements this
 * interface, and the object created with that class is registered with a
 * component using the component's <code>addUpgradeMenuActionListener<code>
 * method. When the upgradeMenuAction event occurs, that object's appropriate
 * method is invoked.
 *
 * @see UpgradeMenuActionEvent
 */
public class UpgradeMenuActionListener {

    /**
     * The listener interface for receiving bootsektorAction events. The class that
     * is interested in processing a bootsektorAction event implements this
     * interface, and the object created with that class is registered with a
     * component using the component's <code>addBootsektorActionListener<code>
     * method. When the bootsektorAction event occurs, that object's appropriate
     * method is invoked.
     *
     * @see BootsektorActionEvent
     */
    public class VirusUpgradeSelectActionListener implements ActionListener {

	int virusnr = 0;

	public VirusUpgradeSelectActionListener(int nr) {
	    virusnr = nr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    switch (virusnr) {
	    case 1:
		HackersMain.menu.setVirus(new Bootsektor());
		break;

	    case 2:
		HackersMain.menu.setVirus(new Computerwurm());
		break;

	    case 3:
		HackersMain.menu.setVirus(new Makrovirus());
		break;

	    case 4:
		HackersMain.menu.setVirus(new Programmviren());
		break;

	    case 5:
		HackersMain.menu.setVirus(new Scriptviren());
		break;

	    case 6:
		HackersMain.menu.setVirus(new Trojaner());
		break;
	    }

	}

    }

    /**
     * The listener interface for receiving buyAction events. The class that is
     * interested in processing a buyAction event implements this interface, and the
     * object created with that class is registered with a component using the
     * component's <code>addBuyActionListener<code> method. When the buyAction event
     * occurs, that object's appropriate method is invoked.
     *
     * @see BuyActionEvent
     */
    public class BuyActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    Player.setViren(UpgradeMenu.getVirus());
	    HackersMain.menu.setVirus(new Virus());
	    HackersMain.menu.revalidate();
	}

    }

    /**
     * The listener interface for receiving buyAction events. The class that is
     * interested in processing a buyAction event implements this interface, and the
     * object created with that class is registered with a component using the
     * component's <code>addBuyActionListener<code> method. When the buyAction event
     * occurs, that object's appropriate method is invoked.
     *
     * @see BuyActionEvent
     */
    public class UpgradeActionListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    Player.setViren(UpgradeMenu.getVirus());
	    HackersMain.menu.revalidate();
	}

    }

    /**
     * The listener interface for receiving bootsektorAction events. The class that
     * is interested in processing a bootsektorAction event implements this
     * interface, and the object created with that class is registered with a
     * component using the component's <code>addBootsektorActionListener<code>
     * method. When the bootsektorAction event occurs, that object's appropriate
     * method is invoked.
     *
     * @see BootsektorActionEvent
     */
    public class VirusUpgradeActionListener implements ActionListener {

	int virusnr = 0;

	public VirusUpgradeActionListener(int nr) {
	    virusnr = nr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    Virus v = null;
	    switch (virusnr) {
	    case 1:
		v = new Bootsektor();
		break;

	    case 2:
		v = new Computerwurm();
		break;

	    case 3:
		v = new Makrovirus();
		break;

	    case 4:
		v = new Programmviren();
		break;

	    case 5:
		v = new Scriptviren();
		break;

	    case 6:
		v = new Trojaner();
		break;
	    }
	    int c = Player.getCountVirus(v);
	    v.setPrice(v.getPrice() * (c *(c+1)));
	    v.addUpdate();
	    HackersMain.menu.setVirus(v);

	}

    }

}

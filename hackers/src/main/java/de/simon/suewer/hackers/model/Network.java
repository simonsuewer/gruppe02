/*
 * 
 */
package de.simon.suewer.hackers.model;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import de.simon.suewer.hackers.grapics.Graphics;
import de.simon.suewer.hackers.menu.GUI;
import de.simon.suewer.hackers.sound.TTS;
import de.simon.suewer.hackers.staticmodel.Settings;
import de.simon.suewer.hackers.world.GameObject;
import de.simon.suewer.hackers.world.World;

// TODO: Auto-generated Javadoc
/**
 * The Class Network.
 */
public class Network extends GameObject {

	/** The static id. */
	private static int staticId = 0;
	
	/** The protect. */
	private Protecter protect;
	
	/** The id. */
	private int id;
	
	/** The occupied. */
	private boolean occupied = false;
	
	/** The connect line. */
	private boolean connectLine = false;

	/** The updated virus. */
	private static int updatedVirus = 0;
	
	/** The updated network. */
	private static int updatedNetwork = 0;
	
	/** The upgrade. */
	private static ProtecterUpgrade upgrade = null;

	/** The percent occupied. */
	private static int percentOccupied;
	
	

	/**
	 * Gets the percent occupied.
	 *
	 * @return the percent occupied
	 */
	public static int getPercentOccupied() {
		return percentOccupied;
	}

	{
		staticId++;
		setId(staticId);
		setProtect(new Protecter());
		height = Settings.NETWORK_HEIGHT;
		width = Settings.NETWORK_WIDTH;

	}

	/**
	 * Instantiates a new network.
	 */
	public Network() {
		super(Graphics.getNetworkPosition(World.getBackground(), World.WORLD_WIDTH, World.WORLD_HIGHT));

	}

	/**
	 * Instantiates a new network.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public Network(float x, float y) {
		super(x, y);
	}

	/**
	 * Instantiates a new network.
	 *
	 * @param n the n
	 */
	public Network(Network n) {
		super(Graphics.getNetworkPosition(World.getBackground(), n));

	}

	/** The networks. */
	private List<Network> networks = null;

	/**
	 * Gets the network.
	 *
	 * @return the network
	 */
	public List<Network> getNetwork() {
		return networks;
	}

	/**
	 * Sets the network.
	 *
	 * @param network the new network
	 */
	public void setNetwork(List<Network> network) {
		this.networks = network;
	}

	/** The static network. */
	private static Network staticNetwork = null;

	/**
	 * Gets the static network.
	 *
	 * @return the static network
	 */
	public static Network getStaticNetwork() {
		if (staticNetwork == null) {
			createNetwork();
		}
		return staticNetwork;
	}

	/**
	 * Adds the network.
	 *
	 * @param n the n
	 */
	private void addNetwork(Network n) {
		if (networks == null) {
			networks = new ArrayList<>();
		}
		networks.add(n);

	}

	/**
	 * Creates the network.
	 */
	public static void createNetwork() {
		int under = Settings.NETWORK_CREATION_UNDER_NUMBER;
		int top = Settings.NETWORK_CREATION_TOP_NUMBER;
		Network root = new Network(1, 1);
		Network e1 = null;
		Network e2 = null;
		Network e3 = null;
		Network e4 = null;
		Network e5 = null;
		Network e6 = null;
		for (int topi = 1; topi < top; topi++) {
			e1 = new Network();
			for (int i = 1; i < under; i++) {
				e1.addNetwork(new Network());
			}
			e2 = new Network();
			for (int i = 1; i < 10; i++) {
				e1.addNetwork(new Network());
			}
			e3 = new Network();
			for (int i = 1; i < 10; i++) {
				e2.addNetwork(new Network());
			}

			e4 = new Network();
			for (int i = 1; i < under; i++) {
				e4.addNetwork(new Network());
			}
			e5 = new Network();
			for (int i = 1; i < 10; i++) {
				e5.addNetwork(new Network());
			}
			e6 = new Network();
			for (int i = 1; i < 10; i++) {
				e6.addNetwork(new Network());
			}

			e1.addNetwork(e2);
			e1.addNetwork(e3);
			e1.addNetwork(e4);
			e1.addNetwork(e5);
			e1.addNetwork(e6);
			root.addNetwork(e1);

		}

		int dice = 0;
		for (Network n : root.getNetwork()) {
			dice = ThreadLocalRandom.current().nextInt(1, 6);

			if (dice > Settings.NETWORK_DICE_PERCENT) {
				n.connectLine = true;
				n.getNetwork().get(ThreadLocalRandom.current().nextInt(1, n.getNetwork().size())).connectLine = true;
			}
		}

		staticNetwork = root;

	}

	/* (non-Javadoc)
	 * @see de.simon.suewer.hackers.world.GameObject#render()
	 */
	@Override
	public void render() {
		if (staticNetwork == null) {
			createNetwork();
		}
		renderForNetworks(staticNetwork);

	}

	/**
	 * Render for networks.
	 *
	 * @param n the n
	 */
	public void renderForNetworks(Network n) {
		if (occupied) {
			Graphics.setColor(1, 0, 0, 0);
			Graphics.fillRect(n.x, n.y, n.width, n.height);

		} else {
			Graphics.setColor(0, 1, 0, 0);
		}
		
		

		if (n.getNetwork() == null) {
			return;
		}
		n.getNetwork().forEach(net -> {
			if (net.occupied && net.connectLine && n.connectLine) {
				Graphics.printLine(n.x, n.y, net.x, net.y);
			}

			net.renderForNetworks(net);

		});
		Graphics.setColor(1, 1, 1, 1);
	}

	/* (non-Javadoc)
	 * @see de.simon.suewer.hackers.world.GameObject#update()
	 */
	@Override
	public void update() {
		Network.updater();
	}

	/**
	 * Updater.
	 */
	public static void updater() {
		// cler percent
		percentOccupied = 0;

		if (staticNetwork == null) {
			return;
		}
		updateForNetworks(staticNetwork);

		if (ThreadLocalRandom.current().nextInt(1, Settings.NETWORK_UPGRADE_PERCENT) == 1) {
			upgrade = ProtecterUpgrade.getUpdate();
			if(upgrade != null) {
				TTS.speak("Network updated with " + upgrade.name);
				GUI.update(upgrade.name);
			}
		} else {
			upgrade = null;
		}

		updatedVirus = 0;
		updatedNetwork = 0;
		World.gamePercent = (percentOccupied * 100) / staticId;
	}

	/**
	 * Update for networks.
	 *
	 * @param n the n
	 */
	public static void updateForNetworks(Network n) {

		if (n.occupied) {
			percentOccupied++;

			// Geld bekommen
			Player.earnMoney();
		}
		if (n.getNetwork() == null) {
			return;
		}

		for (Network child : n.getNetwork()) {

			// angriff && verteidigung
			if (n.occupied != child.occupied) {
				clacVirusAttacks(n, child);
				clacNetworkAttacks(n, child);
			}

			// update geräte
			if (upgrade != null) {

				child.getProtect().update(upgrade);
			}

			Network.updateForNetworks(child);
		}

	}

	/**
	 * Clac virus attacks.
	 *
	 * @param head the head
	 * @param child the child
	 */
	private static void clacVirusAttacks(Network head, Network child) {

		int dice = ThreadLocalRandom.current().nextInt(1, 6);

		// Angriff Player auf child
		if (Player.getPower() > child.getProtect().getProtect()) {
			if (dice >= 3 && Player.getSpeed() > updatedVirus) {

				child.occupied = true;
				updatedVirus++;
			}
		}

	}

	/**
	 * Clac network attacks.
	 *
	 * @param head the head
	 * @param child the child
	 */
	private static void clacNetworkAttacks(Network head, Network child) {

		int dice = ThreadLocalRandom.current().nextInt(1, 6);
		if (!child.occupied && head.occupied && dice >= 3) {
			// Gegenangriff
			// Angriff Player auf child von Head
			if (child.getProtect().getPower() > Player.getCamouflage()
					&& child.getProtect().getSpeed() > updatedNetwork) {

				// test ob alle kinde frei sind
				for (Network test : head.getNetwork()) {
					if (test.occupied) {
						// return;
					}
				}
				
				head.occupied = false;
				updatedNetwork++;
				return;

			}
		}

		if (child.occupied && !head.occupied) {
			// Gegenangriff
			// Angriff Player auf child von Head
			if (head.getProtect().getPower() > Player.getCamouflage()) {
				if (dice >= 3 && head.getProtect().getSpeed() > updatedNetwork) {
				
					child.occupied = false;
					updatedNetwork++;

				}
			}
		}

	}

	/**
	 * Gets the protect.
	 *
	 * @return the protect
	 */
	public Protecter getProtect() {
		return protect;
	}

	/**
	 * Sets the protect.
	 *
	 * @param protect the new protect
	 */
	public void setProtect(Protecter protect) {
		this.protect = protect;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Checks if is occupied.
	 *
	 * @return true, if is occupied
	 */
	public boolean isOccupied() {
		return occupied;
	}

	/**
	 * Sets the occupied.
	 *
	 * @param occupied the new occupied
	 */
	public void setOccupied(boolean occupied) {
		this.occupied = occupied;
	}
	
	/*public static void renew() {
		setProtect(new Protecter());
	}*/

}

/*
 * 
 */
package de.simon.suewer.hackers.model;

import de.simon.suewer.hackers.engine.GameLoop;
import de.simon.suewer.hackers.grapics.Graphics;
import de.simon.suewer.hackers.resource.ImageResource;
import de.simon.suewer.hackers.world.GameObject;

// TODO: Auto-generated Javadoc
/**
 * The Class PauseSymbol.
 */
public class PauseSymbol extends GameObject {

	/**
	 * Update.
	 */
	public void update() {
		// no update
	}

	/**
	 * Render.
	 */
	public void render() {
		if (!GameLoop.isRunning()) {
			Graphics.fillRect(-50, 0, 50, 150);
			Graphics.fillRect(50, 0, 50, 150);
		}
	}

}

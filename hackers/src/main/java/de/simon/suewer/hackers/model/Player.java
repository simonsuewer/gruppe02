/*
 * 
 */
package de.simon.suewer.hackers.model;

import java.util.ArrayList;
import java.util.List;

import de.simon.suewer.hackers.engine.HackersMain;
import de.simon.suewer.hackers.staticmodel.Settings;

// TODO: Auto-generated Javadoc
/**
 * The Class Player.
 */
public class Player {

	/** The money. */
	private static int money;

	/** The viren. */
	private static List<Virus> viren = new ArrayList<>();

	/** The name. */
	private static String name;

	static {
		money = 0;
		viren.add(new Virus());
		name = "test";
	}

	/**
	 * Gets the money.
	 *
	 * @return the money
	 */
	public static int getMoney() {
		return money;
	}

	/**
	 * Sets the money.
	 *
	 * @param money the new money
	 */
	public static void setMoney(int money) {
		Player.money = money;
	}

	/**
	 * Gets the viren.
	 *
	 * @return the viren
	 */
	public static List<Virus> getViren() {
		return viren;
	}

	/**
	 * Sets the viren.
	 *
	 * @param viren the new viren
	 */
	public static void setViren(List<Virus> viren) {
		Player.viren = viren;
	}

	/**
	 * Sets the viren.
	 *
	 * @param viren the new viren
	 */
	public static void setViren(Virus viren) {
		System.out.println("buy  " + viren.name);
		Player.viren.add(viren);
		money = (int) (money - viren.getPrice() * HackersMain.getLevel());
	}
	
	public static int getCountVirus(Virus b) {
	    int count = 0;
	    for (Virus v : Player.getViren()) {
		if (b.getName().equals(v.getName())) {
		    count ++;
		}
	    }
	    
	    return count;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public static String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public static void setName(String name) {
		Player.name = name;
	}

	/**
	 * Earn money.
	 */
	public static void earnMoney() {
		money += Settings.PLAYER_MONEY_PER_NETWORK;
	}

	/**
	 * Gets the power.
	 *
	 * @return the power
	 */
	public static int getPower() {
		int power = 0;
		for (Virus u : viren) {
			power += u.getPower();
		}
		return power;
	}

	/**
	 * Gets the speed.
	 *
	 * @return the speed
	 */
	public static int getSpeed() {
		int speed = 0;
		for (Virus u : viren) {
			speed += u.getSpeed();
		}
		return speed;
	}

	/**
	 * Gets the camouflage.
	 *
	 * @return the camouflage
	 */
	public static int getCamouflage() {
		int c = 0;
		for (Virus u : viren) {
			c += u.getCamouflage();
		}
		return c;
	}

	/**
	 * Renew.
	 */
	public static void renew() {
		money = 0;
		viren = new ArrayList<>();
		viren.add(new Virus());
		name = "test";
	}

}

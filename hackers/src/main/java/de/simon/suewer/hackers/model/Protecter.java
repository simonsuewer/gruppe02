/*
 * 
 */
package de.simon.suewer.hackers.model;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Protecter.
 */
public class Protecter {
	
	/** The base power. */
	protected static int basePower = 0;
	
	/** The base protect. */
	protected static int baseProtect = 0;
	
	/** The base speed. */
	protected static int baseSpeed = 0;
	
	/** The info. */
	protected static String info = "Start-Protecter";
	
	/** The name. */
	protected static String name = "Protecter";
	
	/** The upgrades. */
	protected List<ProtecterUpgrade> upgrades = new ArrayList<ProtecterUpgrade>();;

	/**
	 * Gets the power.
	 *
	 * @return the power
	 */
	public int getPower() {
		int power = basePower;
		for (ProtecterUpgrade u : upgrades) {
			power += u.power;
		}
		return power;
	}

	/**
	 * Gets the speed.
	 *
	 * @return the speed
	 */
	public int getSpeed() {
		int speed = baseSpeed;
		for (ProtecterUpgrade u : upgrades) {
			speed += u.speed;
		}
		return speed;
	}

	/**
	 * Gets the protect.
	 *
	 * @return the protect
	 */
	public int getProtect() {
		int c = baseProtect;
		for (ProtecterUpgrade u : upgrades) {
			c += u.protect;
		}
		return c;
	}

	/**
	 * Update.
	 *
	 * @param upgrade the upgrade
	 * @return the string
	 */
	public String update(ProtecterUpgrade upgrade) {
		upgrades.add(upgrade);
		return upgrade.name;

	}
}

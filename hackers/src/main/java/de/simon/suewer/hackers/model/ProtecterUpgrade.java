/*
 * 
 */
package de.simon.suewer.hackers.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import de.simon.suewer.hackers.engine.HackersMain;
import de.simon.suewer.hackers.staticmodel.Settings;
import de.simon.suewer.hackers.world.World;

// TODO: Auto-generated Javadoc
/**
 * The Class ProtecterUpgrade.
 */
public class ProtecterUpgrade {
	
	/** The power. */
	protected int power = 1;
	
	/** The protect. */
	protected int protect = 1;
	
	/** The speed. */
	protected int speed = 1;
	
	/** The name. */
	protected String name = "Protecter";
	
	/** The names. */
	private static List<String> names = new ArrayList<String>(
			Arrays.asList("Virenscanner Update", "Betribsystem Update", "Firewall Update", "Software Update"));

	/**
	 * Instantiates a new protecter upgrade.
	 */
	public ProtecterUpgrade() {

	}

	/**
	 * Gets the update.
	 *
	 * @return the update
	 */
	public static ProtecterUpgrade getUpdate() {

		// level
		double level = HackersMain.getLevel();
		// zeit
		Date now = World.getDate();
		Date start = World.getStartDate();
		long diff = now.getTime() - start.getTime();
		long daysBetween = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

		if (daysBetween == Settings.NETWORK_UPDATES_AFTER_DAYS) {
			return null;
		}

		long time = (daysBetween - Settings.NETWORK_UPDATES_AFTER_DAYS) / 10;

		double maxUpdate = Settings.PROTECTOR_BASE_UPGRADE_POWER * level * time;
		if (maxUpdate <= 0) {
			maxUpdate = Settings.PROTECTOR_MIN_UPGRADE_POWER;
		}

		// get name for de rendom update
		int index = ThreadLocalRandom.current().nextInt(names.size());
		ProtecterUpgrade update = new ProtecterUpgrade();
		update.name = names.get(index);
		update.protect = (int) ThreadLocalRandom.current().nextDouble(maxUpdate);
		update.power = (int) ThreadLocalRandom.current().nextDouble(maxUpdate);
		update.speed = (int) ThreadLocalRandom.current().nextDouble(maxUpdate);
		return update;

	}

}

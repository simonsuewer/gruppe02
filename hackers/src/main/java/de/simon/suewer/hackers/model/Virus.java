/*
 * 
 */
package de.simon.suewer.hackers.model;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class Virus.
 */
// Quelle "https://www.biteno.com/computerviren-arten-wirkungen-und-schutz/";
public class Virus {

    /** The base power. */
    protected int basePower = 1;

    /** The base camouflage. */
    protected int baseCamouflage = 1;

    /** The base speed. */
    protected int baseSpeed = 1;

    /** The price. */
    protected int price = 0;

    /** The info. */
    protected String info = "Start-Virus";

    /** The name. */
    protected String name = "Virus";

    /** The upgrades. */
    protected List<VirusUpgrade> upgrades = new ArrayList<VirusUpgrade>();

    /**
     * Instantiates a new virus.
     */
    public Virus() {
	System.out.println("ddf");
    }

    /**
     * Gets the base power.
     *
     * @return the base power
     */
    public int getBasePower() {
	return basePower;
    }

    /**
     * Sets the base power.
     *
     * @param basePower the new base power
     */
    public void setBasePower(int basePower) {
	this.basePower = basePower;
    }

    /**
     * Gets the base camouflage.
     *
     * @return the base camouflage
     */
    public int getBaseCamouflage() {
	return baseCamouflage;
    }

    /**
     * Sets the base camouflage.
     *
     * @param baseCamouflage the new base camouflage
     */
    public void setBaseCamouflage(int baseCamouflage) {
	this.baseCamouflage = baseCamouflage;
    }

    /**
     * Gets the base speed.
     *
     * @return the base speed
     */
    public int getBaseSpeed() {
	return baseSpeed;
    }

    /**
     * Sets the base speed.
     *
     * @param baseSpeed the new base speed
     */
    public void setBaseSpeed(int baseSpeed) {
	this.baseSpeed = baseSpeed;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public int getPrice() {
	return price;
    }

    /**
     * Sets the price.
     *
     * @param price the new price
     */
    public void setPrice(int price) {
	this.price = price;
    }

    /**
     * Gets the info.
     *
     * @return the info
     */
    public String getInfo() {
	return info;
    }

    /**
     * Sets the info.
     *
     * @param info the new info
     */
    public void setInfo(String info) {
	this.info = info;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
	return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * Gets the upgrades.
     *
     * @return the upgrades
     */
    public List<VirusUpgrade> getUpgrades() {
	return upgrades;
    }

    /**
     * Sets the upgrades.
     *
     * @param upgrades the new upgrades
     */
    public void setUpgrades(List<VirusUpgrade> upgrades) {
	this.upgrades = upgrades;
    }

    /**
     * Gets the power.
     *
     * @return the power
     */
    public int getPower() {
	int power = basePower;
	for (VirusUpgrade u : upgrades) {
	    power += u.getPower();
	}
	return power;
    }

    /**
     * Gets the speed.
     *
     * @return the speed
     */
    public int getSpeed() {
	int speed = baseSpeed;
	for (VirusUpgrade u : upgrades) {
	    speed += u.getSpeed();
	}
	return speed;
    }

    /**
     * Gets the camouflage.
     *
     * @return the camouflage
     */
    public int getCamouflage() {
	int c = baseCamouflage;
	for (VirusUpgrade u : upgrades) {
	    c += u.getCamouflage();
	}
	return c;
    }

    public void addUpdate() {
	upgrades.add(VirusUpgrade.getUpdate());
    }

}

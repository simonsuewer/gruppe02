/*
 * 
 */
package de.simon.suewer.hackers.model;

import java.util.concurrent.ThreadLocalRandom;

import de.simon.suewer.hackers.staticmodel.Settings;

// TODO: Auto-generated Javadoc
/**
 * The Class VirusUpgrade.
 */
public class VirusUpgrade {

    /** The power. */
    private int power;

    /** The camouflage. */
    private int camouflage;

    /** The speed. */
    private int speed;

    /** The price. */
    private int price;

    /** The info. */
    private String info;

    /** The name. */
    private String name;

    /**
     * Gets the power.
     *
     * @return the power
     */
    public int getPower() {
	return power;
    }

    /**
     * Sets the power.
     *
     * @param power the new power
     */
    public void setPower(int power) {
	this.power = power;
    }

    /**
     * Gets the camouflage.
     *
     * @return the camouflage
     */
    public int getCamouflage() {
	return camouflage;
    }

    /**
     * Sets the camouflage.
     *
     * @param camouflage the new camouflage
     */
    public void setCamouflage(int camouflage) {
	this.camouflage = camouflage;
    }

    /**
     * Gets the speed.
     *
     * @return the speed
     */
    public int getSpeed() {
	return speed;
    }

    /**
     * Sets the speed.
     *
     * @param speed the new speed
     */
    public void setSpeed(int speed) {
	this.speed = speed;
    }

    /**
     * Gets the info.
     *
     * @return the info
     */
    public String getInfo() {
	return info;
    }

    /**
     * Sets the info.
     *
     * @param info the new info
     */
    public void setInfo(String info) {
	this.info = info;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
	return name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * Gets the price.
     *
     * @return the price
     */
    public int getPrice() {
	return price;
    }

    /**
     * Sets the price.
     *
     * @param price the new price
     */
    public void setPrice(int price) {
	this.price = price;
    }

    /**
     * Gets the update.
     *
     * @return the update
     */
    public static VirusUpgrade getUpdate() {

	double maxUpdate = Settings.VIRUS_BASE_UPGRADE_POWER;
	if (maxUpdate <= 0) {
	    maxUpdate = Settings.VIRUS_MIN_UPGRADE_POWER;
	}

	// get name for de rendom update
	VirusUpgrade update = new VirusUpgrade();
	update.camouflage = (int) ThreadLocalRandom.current().nextDouble(maxUpdate);
	update.power = (int) ThreadLocalRandom.current().nextDouble(maxUpdate);
	update.speed = (int) ThreadLocalRandom.current().nextDouble(maxUpdate);
	return update;

    }

}

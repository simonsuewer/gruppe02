/*
 * 
 */
package de.simon.suewer.hackers.sound;

import java.nio.ByteBuffer;

import com.jogamp.openal.AL;
import com.jogamp.openal.ALException;
import com.jogamp.openal.ALFactory;
import com.jogamp.openal.util.ALut;

import de.simon.suewer.hackers.engine.HackersMain;

// TODO: Auto-generated Javadoc
/**
 * The Class BackgroundMusic.
 */
public class BackgroundMusic {

	/** The al. */
	static AL al = ALFactory.getAL();

	/** The buffer. */
	// Buffers hold sound data.
	static int[] buffer = new int[1];;

	/** The source. */
	// Sources are points emitting sound.
	static int[] source = new int[1];

	/** The source pos. */
	static float[] sourcePos = { 0.0f, 0.0f, 0.0f };

	/** The source vel. */
	// Velocity of the source sound.
	static float[] sourceVel = { 0.0f, 0.0f, 0.0f };

	/** The listener pos. */
	// Position of the listener.
	static float[] listenerPos = { 0.0f, 0.0f, 0.0f };

	/** The listener vel. */
	// Velocity of the listener.
	static float[] listenerVel = { 0.0f, 0.0f, 0.0f };

	/** The listener ori. */
	// Orientation of the listener. (first 3 elements are "at", second 3 are "up")
	static float[] listenerOri = { 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f };

	private static boolean running = true;

	/**
	 * Load AL data.
	 *
	 * @return the int
	 */
	private static int loadALData() {

		// variables to load into

		int[] format = new int[1];
		int[] size = new int[1];
		ByteBuffer[] data = new ByteBuffer[1];
		int[] freq = new int[1];
		int[] loop = new int[1];

		// Load wav data into a buffer.
		al.alGenBuffers(1, buffer, 0);
		if (al.alGetError() != AL.AL_NO_ERROR)
			return AL.AL_FALSE;

		ALut.alutLoadWAVFile("src/main/resources/backgroundmusic.wav", format, data, size, freq, loop);
		al.alBufferData(buffer[0], format[0], data[0], size[0], freq[0]);

		// Bind buffer with a source.
		al.alGenSources(1, source, 0);

		if (al.alGetError() != AL.AL_NO_ERROR)
			return AL.AL_FALSE;

		al.alSourcei(source[0], AL.AL_BUFFER, buffer[0]);
		al.alSourcef(source[0], AL.AL_PITCH, 1.0f);
		al.alSourcef(source[0], AL.AL_GAIN, 1.0f);
		al.alSourcefv(source[0], AL.AL_POSITION, sourcePos, 0);
		al.alSourcefv(source[0], AL.AL_VELOCITY, sourceVel, 0);
		al.alSourcei(source[0], AL.AL_LOOPING, loop[0]);

		// Do another error check and return.
		if (al.alGetError() == AL.AL_NO_ERROR)
			return AL.AL_TRUE;

		return AL.AL_FALSE;
	}

	/**
	 * Sets the listener values.
	 */
	static void setListenerValues() {
		al.alListenerfv(AL.AL_POSITION, listenerPos, 0);
		al.alListenerfv(AL.AL_VELOCITY, listenerVel, 0);
		al.alListenerfv(AL.AL_ORIENTATION, listenerOri, 0);
	}

	/**
	 * Kill AL data.
	 */
	public static void killALData() {
		running = false;
		al.alDeleteBuffers(1, buffer, 0);
		al.alDeleteSources(1, source, 0);
		try {
		    ALut.alutExit(); 
		}catch(ALException e) {
		    //Do nothing
		}
		
	}

	/**
	 * Inits the.
	 */
	public static void init() {
		// Initialize OpenAL and clear the error bit.
		System.out.println("INIT SOUND");
		loadALData();
		ALut.alutInit();
		al.alGetError();

		// Load the wav data.
		if (loadALData() == AL.AL_FALSE)
			System.exit(-1);

		setListenerValues();

		// Setup an exit procedure.

		Runtime runtime = Runtime.getRuntime();
		runtime.addShutdownHook(new Thread(new Runnable() {
			public void run() {
				killALData();
			}
		}));

		Thread tread = new Thread() {
			public void run() {

				// long startTime = System.nanoTime();

				long ticker = 0;
				while (running) {
					long currentTime = System.nanoTime();

					if (ticker > 440) {
						ticker = 0;
						sourcePos[0] += sourceVel[0];
						sourcePos[1] += sourceVel[1];
						sourcePos[2] += sourceVel[2];
						al.alSourcefv(source[0], AL.AL_POSITION, sourcePos, 0);
						play();
						System.out.println("newboot sound");
					}
					long timeTaken = System.nanoTime() - currentTime;

					if (timeTaken < 1000000000) {
						try {
							Thread.sleep((1000000000 - timeTaken) / 10000000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					ticker++;
				}

			}
		};

		tread.setName("BackgroundMusic");
		tread.start();

	}

	/**
	 * Play.
	 */
	public static void play() {
		if (HackersMain.sound) {
			al.alSourcePlay(source[0]);
		}

	}

	/**
	 * Pause.
	 */
	public static void pause() {
		al.alSourcePause(source[0]);
	}

	/**
	 * Stop.
	 */
	public static void stop() {
		al.alSourceStop(source[0]);
	}

	/**
	 * Volume.
	 */
	public static void volume() {

	}
}

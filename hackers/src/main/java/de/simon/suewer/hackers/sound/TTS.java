/*
 * 
 */
package de.simon.suewer.hackers.sound;

import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import de.simon.suewer.hackers.engine.HackersMain;

// TODO: Auto-generated Javadoc
/**
 * The Class TTS.
 */
public class TTS {

	/** The Constant VOICENAME. */
	private static final String VOICENAME = "kevin";

	/** The voice. */
	private static Voice voice;

	/** The voice manager. */
	private static VoiceManager voiceManager;

	/**
	 * Inits the.
	 */
	public static void init() {
		System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us.cmu_us_kal.KevinVoiceDirectory");
		voiceManager = VoiceManager.getInstance();
		/*
		 * Voice[] voices = voiceManager.getVoices(); for (int i = 0; i < voices.length;
		 * i++){ System.out.print( "voice name is: " + voices[i].getName() +"\n"); }
		 */
		voice = voiceManager.getVoice(VOICENAME);
		voice.allocate();

	}

	/**
	 * Speak.
	 *
	 * @param text the text
	 */
	public static void speak(String text) {
		if (HackersMain.tts) {
			BackgroundMusic.pause();
			voice.speak(text);
			BackgroundMusic.play();
		}

	}

}

/*
 * 
 */
package de.simon.suewer.hackers.staticmodel;

import de.simon.suewer.hackers.model.Virus;

/**
 * The Class Bootsektor.
 */
public class Bootsektor extends Virus {

	// zuweisen der Werte
	{
		basePower = 1;
		baseSpeed = 1;
		baseCamouflage = 100;
		price = 1000;
		name = "Bootsektor";
		info = "Der sogenannte Bootsektor-Virus ist der älteste unter allen Computerviren. Wie der Name vermuten lässt, nistet er sich im Bootsystem ein. Dabei handelt es sich um ein Programm, welches sich auf der Festplatte befindet und durch das der Betriebssystemstart erst möglich ist.\n"
				+ "\n"
				+ "Der Bootsektorvirus verändert die korrekten Programmdaten sowie Einstellungen so, dass ein Hochfahren des PCs nicht mehr erfolgen kann.\n"
				+ "\n"
				+ "Besondere Popularität besaß diese Virusart australian online casino sites in den 80er und 90er Jahren. Hier wurde er von den damals gängigen Disketten-Wechselträgern auf den PC übertragen. Heute findet die Infizierung überwiegend durch das Internet statt. Allerdings sind sie nicht mehr weit verbreitet.";
	}
}

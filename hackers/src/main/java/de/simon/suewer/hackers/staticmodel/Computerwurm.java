/*
 * 
 */
package de.simon.suewer.hackers.staticmodel;

import de.simon.suewer.hackers.model.Virus;

/**
 * The Class Computerwurm.
 */
public class Computerwurm extends Virus {

	// zuweisen der Werte
	{
		basePower = 1;
		baseSpeed = 10;
		baseCamouflage = 1;
		price = 1000;
		name = "Computerwurm";
		info = "Bei einem Computerwurm handelt es sich um eine spezielle Virus-Art, die sich eigenständig und in rasender Geschwindigkeit ausbreiten kann.\n"
				+ "Im Gegensatz zu anderen Viren-Arten benötigen Computerviren nicht das Agieren des PC-Nutzers, der beispielsweise den Virus erst aktiviert, wenn er ein virusverseuchtes Programm ausführt.\n"
				+ "\n"
				+ "Ein Computervirus wird überwiegend in E-Mails eingebaut. Die größte Verbreitung erfolgt über sogenannte Ketten-E-Mails, welche sich automatisch über das Internet als E-Mail in Millionen von E-Mail-Postfächer selbst verschicken können. Dort angekommen, breiten sie sich über das Postfach im PC aus.\n"
				+ "\n"
				+ "Dies geschah zum Beispiel im Jahre 2000. Hier gelangen Computerwürmer über E-Mail als Liebesbrief getarnt, weltweit in die Postfächer und verursachte Milliarden von US-Dollar an Schäden. Hauptsächlich dienten diese Computerwürmer zum Ausspähen von sensiblen Daten wie Passwörtern und zerstörten Bilddateien.";

	}
}

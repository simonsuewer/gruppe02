/*
 * 
 */
package de.simon.suewer.hackers.staticmodel;

import de.simon.suewer.hackers.model.Virus;

/**
 * The Class Makrovirus.
 */
public class Makrovirus extends Virus {

	// zuweisen der Werte
	{
		basePower = 10;
		baseSpeed = 1;
		baseCamouflage = 1;
		price = 100;
		name = "Makrovirus";
		info = "Der Makrovirus verhält sich Prinzip gleich wie ein Trojaner oder Programmvirus. Der Unterschied besteht darin, dass sich Makroviren in Word- und Excel-Dateien befinden. In Programmen sind sie nicht vorhanden.\n"
				+ "\n"
				+ "Ihre Wirkung beginnt automatisch in dem Moment, wenn eine mit dem Virus enthaltende Datei geöffnet wird. Meist verbreitet sich der Makrovirus über das Internet per Email-Versand oder den Download von Dateien.";
	}
}

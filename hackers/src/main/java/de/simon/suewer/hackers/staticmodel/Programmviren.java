/*
 * 
 */
package de.simon.suewer.hackers.staticmodel;

import de.simon.suewer.hackers.model.Virus;

/**
 * The Class Programmviren.
 */
public class Programmviren extends Virus {

	// zuweisen der Werte
	{
		basePower = 10;
		baseSpeed = 1;
		baseCamouflage = 10;
		price = 100;
		name = "Programmvirus";
		info = "Wie der Name vermuten lässt, infizieren Programmviren PCs über Programme aber auch Dateien, in denen sie integriert sind. Zu einer Aktivierung von Programmviren kommt es, wenn mit ihnen bestückte Programme oder Dateien geöffnet oder ausgeführt werden.\n"
				+ "\n"
				+ "Zu finden sind diese Computerviren vor allem in Programmen, die über das Internet heruntergeladen werden und aus meist unbekannten, unsicheren Quellen entstammen. Zahlreich sind sie in kostenlosen Spielen oder Musikdownloads vorhanden.\n"
				+ "\n" + "";
	}
}

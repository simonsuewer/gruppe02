/*
 * 
 */
package de.simon.suewer.hackers.staticmodel;

import de.simon.suewer.hackers.model.Virus;

/**
 * The Class Scriptviren.
 */
public class Scriptviren extends Virus {

	// zuweisen der Werte
	{
		basePower = 3;
		baseSpeed = 3;
		baseCamouflage = 3;
		price = 10;
		name = "Scriptvirus";
		info = "Bei dieser Überkategorie handelt es sich um Computerviren, welche sich zum Beispiel in Skripten von Internetseiten verstecken. Da die Scriptviren in gängigen Programmiersprachen geschrieben sind, versteht der Browser diese rein als Script, führt diese aus und macht es auf diese Weise möglich, dass sie auf den PC gelangen können.\n"
				+ "\n"
				+ "Scriptviren sind in jeder Programmiersprache wie Java- oder Virtual-Basic Scripts schreibbar. Für den allgemeinen PC- beziehungsweise Internetnutzer sind sie nicht erkennbar. Einmal auf dem PC eingenistet, sind diese Computerviren in der Lage, beispielsweise automatisch E-Mails zu versenden und Dateien zu löschen oder Umbenennungen dieser vorzunehmen.";
	}
}

package de.simon.suewer.hackers.staticmodel;

public class Settings {

	//NETWORK SETTINGS
	public static final float NETWORK_HEIGHT = 4;
	public static final float NETWORK_WIDTH = 4;
	public static final int NETWORK_CREATION_UNDER_NUMBER = 100;
	public static final int NETWORK_CREATION_TOP_NUMBER = 100;
	/** The Constant UPDATES_AFTER_DAYS. */
	public static final long NETWORK_UPDATES_AFTER_DAYS = 50;
	public static final int NETWORK_DICE_PERCENT = 3;
	public static final int NETWORK_UPGRADE_PERCENT = 20;
	
	//PLAYER
	public static final int PLAYER_MONEY_PER_NETWORK = 1;
	
	//PROTECTOR
	public static final int PROTECTOR_BASE_UPGRADE_POWER = 10;
	public static final int PROTECTOR_MIN_UPGRADE_POWER = 1;
	
	public static final int VIRUS_BASE_UPGRADE_POWER = 10;
	public static final int VIRUS_MIN_UPGRADE_POWER = 1;
}


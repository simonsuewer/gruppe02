/*
 * 
 */
package de.simon.suewer.hackers.staticmodel;

import de.simon.suewer.hackers.model.Virus;

/**
 * The Class Trojaner.
 */
public class Trojaner extends Virus {

	// zuweisen der Werte
	{
		basePower = 100;
		baseSpeed = 1;
		baseCamouflage = 1;
		price = 1000;
		name = "Trojaner";
		info = "Einer der seit vielen Jahren aktivste PC-Virus ist der sogenannte Trojaner. Er wird in der Regel mit der Absicht der Schädigung und des Ausspähens von persönlichen Daten auf dem PC verwendet.\n"
				+ "\n"
				+ "Sicher ist vor ihm kein PC-Nutzer oder Internet-User, weil er sich zahlreich in harmlos erscheinenden Programmen versteckt. Oftmals befindet er sich auch in Spam-Mails, die nicht selten als Rechnung oder Ähnliches getarnt sind. Aus diesen schleicht er sich in den PC ein, sobald sie oder sich darin befindliche Links geöffnet werden.";
	}
}

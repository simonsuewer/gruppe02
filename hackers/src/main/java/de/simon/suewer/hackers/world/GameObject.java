/*
 * 
 */
package de.simon.suewer.hackers.world;

import de.simon.suewer.hackers.grapics.Animation;
import de.simon.suewer.hackers.grapics.Graphics;

// TODO: Auto-generated Javadoc
/**
 * The Class GameObject.
 */
public class GameObject {

	/** The x. */
	// position
	public float x = 0;
	
	/** The y. */
	public float y = 0;

	/** The width. */
	// size
	public float width = 1;
	
	/** The height. */
	public float height = 1;

	/** The rotation. */
	// rotation in °
	public float rotation = 0;

	/** The animations. */
	// Animations
	public Animation[] animations;
	
	/** The current animation. */
	public int currentAnimation = 0;

	/**
	 * Instantiates a new game object.
	 *
	 * @param x the x
	 * @param y the y
	 */
	public GameObject(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Instantiates a new game object.
	 */
	public GameObject() {

	}
	
	/**
	 * Instantiates a new game object.
	 *
	 * @param g the g
	 */
	public GameObject(GameObject g) {
		this.x = g.x;
		this.y =g.y;
	}

	/**
	 * Update.
	 */
	public void update() {
		// implemet in subclass
	}

	/**
	 * Render.
	 */
	public void render() {
		// implemet in subclass
		animations[currentAnimation].play();
		Graphics.setRotation(rotation);
		Graphics.drawImage(animations[currentAnimation].getImage(), x, y, width, height);
		Graphics.setRotation(0);
	}
}

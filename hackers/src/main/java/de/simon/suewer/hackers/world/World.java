/*
 * 
 */
package de.simon.suewer.hackers.world;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import de.simon.suewer.hackers.grapics.Graphics;
import de.simon.suewer.hackers.menu.GUI;
import de.simon.suewer.hackers.model.Network;
import de.simon.suewer.hackers.model.PauseSymbol;
import de.simon.suewer.hackers.resource.ImageResource;

// TODO: Auto-generated Javadoc
/**
 * The Class World.
 */
public class World {

	/**
	 * Gets the start date.
	 *
	 * @return the start date
	 */
	public static Date getStartDate() {
		return startDate;
	}

	/** The game objects. */
	private static ArrayList<GameObject> gameObjects = new ArrayList<GameObject>();

	/** The gui. */
	private static GUI gui = new GUI();

	/** The background. */
	private static ImageResource background = new ImageResource("img/worl-map.png");

	/** The Constant WORLD_HIGHT. */
	public static final int WORLD_HIGHT = 504;

	/** The Constant WORLD_WIDTH. */
	public static final int WORLD_WIDTH = 1024;

	/** The easter egg. */
	private static boolean easterEgg = false;

	/**
	 * Gets the background.
	 *
	 * @return the background
	 */
	public static ImageResource getBackground() {
		return background;
	}

	/** The game percent. */
	public static int gamePercent = 0;

	/** The calender. */
	private static Calendar calender = Calendar.getInstance();

	/** The start date. */
	private static Date startDate = new Date();

	static {
		Network.createNetwork();
		World.addObject(Network.getStaticNetwork());
		World.addObject(gui);
		World.addObject(new PauseSymbol());
		Network.getStaticNetwork().setOccupied(true);
	}

	/**
	 * Update.
	 */
	public static void update() {
		// tag ++
		calender.add(Calendar.DATE, 1);

		// all updates update
		gameObjects.forEach(go -> {
			go.update();
		});

	}

	/**
	 * Render.
	 */
	public static void render() {
		if (easterEgg) {
			renderEasterEggColor();
		}
		Graphics.drawImage(background, 0, 0, WORLD_WIDTH, WORLD_HIGHT);
		if (easterEgg) {
			Graphics.setColor(1, 1, 1, 1);
		}

		// all updates render
		gameObjects.forEach(go -> {
			go.render();
		});
		
	}

	/**
	 * Render easter egg color.
	 */
	private static void renderEasterEggColor() {
		int red = ThreadLocalRandom.current().nextInt(0, 5);
		int blue = ThreadLocalRandom.current().nextInt(0, 5);
		int green = ThreadLocalRandom.current().nextInt(0, 5);
		int alpha = ThreadLocalRandom.current().nextInt(0, 5);
		Graphics.setColor(red, green, blue, alpha);
	}

	/**
	 * Adds the object.
	 *
	 * @param go the go
	 */
	public static void addObject(GameObject go) {
		gameObjects.add(go);
	}

	/**
	 * Gets the date.
	 *
	 * @return the date
	 */
	public static Date getDate() {
		return calender.getTime();
	}

	/**
	 * Toggle easter egg.
	 */
	public static void toggleEasterEgg() {
		easterEgg = !easterEgg;
	}

}

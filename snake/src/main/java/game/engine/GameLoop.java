package game.engine;

import game.graphics.GraphicRenderer;
import game.world.World;

public class GameLoop {
	private static boolean isRunning = false;
	private static int updates = 0;
	private static final int MAX_UPDATES = 1;
//	
	private static long lastUpdateTime = 0;
	
	private static int targetFPS = 25;
	private static int targetTime = 1000000000/ targetFPS;
	
	public static long framecount= 0;
	public static int cyclesDone=0;
	public static void start() {
		//SnakeMain.sound = true;
//		Thread thread = new Thread() {
//			public void run() {
//				
//				setRunning(true);
//				
//				lastUpdateTime = System.nanoTime();
//				
//				
//				
//				while(isRunning) {
//					long currentTime = System.nanoTime();
//					framecount++;
//					//updates = 0;
//					/*System.out.println("Target time :"+targetTime);
//					System.out.println("Current time :"+currentTime);
//					System.out.println("LastUpdateTime"+lastUpdateTime);*/
//					
//					
//					/*while(currentTime-lastUpdateTime>= targetTime) {
//						World.update();
//						lastUpdateTime+=targetTime;
//						updates++;
//						
//						if(updates > MAX_UPDATES) {
//							break;
//						}
//					}*/
//					World.update();
//					//Graphics.fillRect(15, 25, 5, 5);
//					
//					
//					GraphicRenderer.render();
//					/*
//					if(System.nanoTime()>= lastFpsCheck+10000000) {
//						System.out.println(fps);
//						fps=0;
//						lastFpsCheck= System.nanoTime();
//					}*/
//					long timeTaken = currentTime - System.nanoTime();
//					
//					if(targetTime > timeTaken) {
//						try {
//							Thread.sleep((targetTime - timeTaken)/1000000);
//						}catch(InterruptedException e){
//							e.printStackTrace();
//							
//						}
//					}
//				}
//			}
//		};
		Thread thread = new Thread() {
			public void run() {
				

				isRunning = true;

				lastUpdateTime = System.nanoTime();

				while (isRunning) {
					framecount++;
					long currentTime = System.nanoTime();
					if(framecount==targetFPS) {
						cyclesDone++;
						framecount = 0;
					}
					if(cyclesDone==2) {
						cyclesDone=0;
					}
					updates = 0;

					while (currentTime - lastUpdateTime >= targetTime) {

						// Poll input

						// System.out.println("update");
						// update game
						World.update();
						lastUpdateTime += targetTime;
						updates++;

						if (updates > MAX_UPDATES) {
							break;
						}

					}
					//BackgroundMusic.play();
					// render
					GraphicRenderer.render();

					/*
					 * fps++; if(System.nanoTime() >= lastFpsCheck + 1000000000) {
					 * System.out.println(fps); fps = 0; lastFpsCheck = System.nanoTime(); }
					 */

					long timeTaken = System.nanoTime() - currentTime;

					if (timeTaken < targetTime) {
						try {
							Thread.sleep((targetTime - timeTaken) / 1000000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}


			}
		};

		thread.setName("GameLoop");
		thread.start();
	}

	public static void setRunning(boolean isRunning) {
		GameLoop.isRunning = isRunning;
	}

	public static boolean isRunning() {
		return isRunning;
	}
	
}

package game.engine;

import javax.swing.JMenuBar;
import javax.swing.JPanel;

import de.hsh.projekt.common.Game;
import de.hsh.projekt.common.GameEventInitiater;
import de.hsh.projekt.common.KeyInput;
import de.hsh.projekt.common.MouseInput;
import game.graphics.GraphicRenderer;
import game.input.SnakeKeyInput;
import game.menu.MainMenuActionListener;
import game.menu.MenuTest;
import game.world.GameStatus;
import javafx.embed.swing.JFXPanel;

public class SnakeMain extends Game {
    //public static JFrame frame = new JFrame();
    public static JPanel jpanel = new JPanel();
    // public static final KeyInput keyinput = new KeyInput();
    public static boolean sound;
    public static boolean soundInit = false;
    public static void main(String[] args) {
	new MenuTest();
	MenuTest.init();
	sound = true;
	//BackgroundMusic.init();
	//BackgroundMusic.init();

    }

    
    @Override
    public int start() {
	new MenuTest();
	MenuTest.init();
	
	//BackgroundMusic.play();
	return 0;
    }

    public  int backToMain() {
	GameLoop.setRunning(false);
	MainMenuActionListener.mp.exit();
	GameStatus.reset();
	GameEventInitiater.exitGame();
	return 0;
    }
    
    public static void staticbackToMain() {
    	GameLoop.setRunning(false);
    	GameStatus.reset();
    	MainMenuActionListener.mp.exit();
    	
    	GameEventInitiater.exitGame();
    }

    @Override
    public KeyInput getKeyInput() {

	return new SnakeKeyInput();
    }

    @Override
    public MouseInput getMouseInput() {
	return new MouseInput();
    }

    @Override
    public String getName() {

	return "SnakeEater";
    }

    @Override
    public String getDescription() {

    	return "<html> In SnakeEater steuert der Spieler einen Virus, das Ziel ist es alle Daten(gr�ne Objekte) einzusammeln. "
				+ " Vorsicht ist geboten  beim einsammeln eines falschen Objekts oder beim zusammensto� mit sich selbst ist das Spiel vorbei."
				+ " Der Spieler kann zwischen verschiedenen Schwierigkeitsgraden  waehlen."
				+ " Gewonnen hat man wenn das gesamte Spielfeld besetzt ist.</html>";    }

    @Override
    public JPanel getPanel() {
	
	if (GameLoop.isRunning()) {
	    //GraphicRenderer.getGLJPanel().requestFocus(true);
	    //Main.frame.addKeyListener();
	  
	    return GraphicRenderer.getGLJPanel();
	} else {
	    return MenuTest.menuPanel;
	}
	
    }

    @Override
    public JMenuBar getJMenuBar() {
	// TODO Auto-generated method stub
	return null;
    }

    @Override
    public JFXPanel getJFXPanel() {
	// TODO Auto-generated method stub
	return null;
    }
}

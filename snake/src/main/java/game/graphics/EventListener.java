package game.graphics;



import java.awt.Font;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.util.awt.TextRenderer;

import game.world.World;



public class EventListener implements GLEventListener {
	public static GL2 gl = null;
	public static float xPos = 0;
	public static int unitsTall;
	public static TextRenderer tRenderer;
	@Override
	public void display(GLAutoDrawable drawable) {
		gl = drawable.getGL().getGL2();
		
		gl.glClearColor(1, 1, 1, 1);
		gl.glClear(GL2.GL_COLOR_BUFFER_BIT);
		World.render();
		//Graphics.drawSnake();
	}

	@Override
	public void dispose(GLAutoDrawable drawable) {
		
		
	}

	@Override
	public void init(GLAutoDrawable drawable) {
		gl = drawable.getGL().getGL2();
		gl.glClearColor(0, 0, 0, 1);
		gl.glEnable(GL2.GL_TEXTURE_2D);
		tRenderer = new TextRenderer(new Font("SansSerif", Font.BOLD, 10));
		
	}

	@Override
	public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
		gl = drawable.getGL().getGL2();
		
		gl.glMatrixMode(GL2.GL_PROJECTION);
		gl.glLoadIdentity();
		
		unitsTall = GraphicRenderer.screenHeight/(GraphicRenderer.screenWidth/GraphicRenderer.unitsWide);
		
		gl.glOrtho(0, GraphicRenderer.unitsWide, 0, unitsTall, -1, 1);
		gl.glMatrixMode(GL2.GL_MODELVIEW);
	}

}

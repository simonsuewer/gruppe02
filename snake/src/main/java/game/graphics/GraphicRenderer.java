package game.graphics;

import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLJPanel;

import game.input.SnakeKeyInput;

public class GraphicRenderer {
	public static EventListener el = new EventListener();
	
	public static int screenWidth = 1024;
	public static int screenHeight = 768;
	
	public static int unitsWide = 60;
	public static int unitsTall = 60;
	private static GLJPanel window = null;
	private static GLProfile profile = null;
	//private static GLWindow window = null;
	public static void init() {
		GLProfile.initSingleton();
		profile = GLProfile.get(GLProfile.GL2);
		GLCapabilities caps = new GLCapabilities(profile);
		
		window = new GLJPanel(caps);
		window.setSize(screenWidth,screenHeight);
		window.addGLEventListener(el);
		window.addKeyListener(new SnakeKeyInput());
		window.requestFocus(true);
		window.setVisible(true);
//***REMOVE WHEN GLJPanel is implemented		
//		window = GLWindow.create(caps);
//		window.setSize(screenWidth,screenHeight);
//		//window.setResizable(false);
//		window.addGLEventListener(el);
//		//window.addKeyListener(new KeyInput());
		
		
		//window.setVisible(true);
		
	}
	
	public static void render() {
		if(window == null) {
			return;
		}
		window.display();
	}
	
	public static void quitGame() {
		window.destroy();
	}
	
	public static GLJPanel getGLJPanel() {
		return window;
	}

	public static GLProfile getProfile() {
		
		return profile;
	}
	
	
	/*public static void main(String[] args) {
		System.out.println("pre init");
		init();
		
	}*/

}

package game.graphics;

import java.awt.Color;
import java.awt.Font;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.util.awt.TextRenderer;

import game.resource.ImageResource;
import game.world.GameObject;
import game.world.Player;
import game.world.Tail;

public class Graphics {
	//Color Values
	private static float red = 0 ;
	private static float green = 1  ;
	private static float blue = 0 ;
	private static float alpha = 1;
	
	//private static float rotation = 0 ;
	public static void fillRect( float x, float y, float width, float height) {
		GL2 gl = EventListener.gl;
		
		gl.glColor4f(red, green, blue,alpha);
		gl.glBegin(GL2.GL_QUADS);
			gl.glVertex2f(x - width / 2, y - height / 2);
			gl.glVertex2f(x + width / 2, y - height / 2);
			gl.glVertex2f(x + width / 2, y + height / 2);
			gl.glVertex2f(x - width / 2, y + height / 2);
		gl.glEnd(); 
	}
	
	
	
	public static void drawImage(ImageResource image, float x, float y, float width, float height) {
		GL2 gl = EventListener.gl;
		
		gl.glColor4f(red, green, blue,alpha);
		gl.glBegin(GL2.GL_QUADS);
			gl.glVertex2f(x - width / 2, y - height / 2);
			gl.glVertex2f(x + width / 2, y - height / 2);
			gl.glVertex2f(x + width / 2, y + height / 2);
			gl.glVertex2f(x - width / 2, y + height / 2);
		gl.glEnd();
		gl.glFlush();
	}
	
	public static void setRotation(float r) {
		//rotation = r;
	}
	
	public static void drawObject(GameObject g) {
		int x = g.getxPos();
		int y = g.getyPos();
		int width = g.getWidth() ;
		int height = g.getHeight() ;
		fillRect(x,y,width,height);		
		
	}
	
	public static void renderHighscore(int highscore) {
//		EventListener.tRenderer.beginRendering(GraphicRenderer.screenWidth, GraphicRenderer.screenHeight);
//	    // optionally set the color
//		EventListener.tRenderer.setColor(1.0f, 0.2f, 0.2f, 0.8f);
//		EventListener.tRenderer.draw("Highscore: "+highscore, GraphicRenderer.screenWidth,10 );
//	    // ... more draw commands, color changes, etc.
//		EventListener.tRenderer.endRendering();
		TextRenderer textRenderer = new TextRenderer(new Font("Verdana", Font.BOLD, 12));
		textRenderer.beginRendering(GraphicRenderer.screenWidth, GraphicRenderer.screenHeight);
		textRenderer.setColor(Color.BLACK);
		textRenderer.setSmoothing(true);

		
		textRenderer.draw("Highscore:"+highscore, GraphicRenderer.screenWidth/2-10, 30);
		textRenderer.endRendering();
	}
	
	public static void renderLosingScreen() {
		TextRenderer textRenderer = new TextRenderer(new Font("Verdana", Font.BOLD, 20));
		textRenderer.beginRendering(GraphicRenderer.screenWidth, GraphicRenderer.screenHeight);
		textRenderer.setColor(Color.BLACK);
		textRenderer.setSmoothing(true);

		
		textRenderer.draw("You Lose", GraphicRenderer.screenWidth/2-10, GraphicRenderer.screenWidth/2-10);
		textRenderer.draw("Hit Enter To Restart / ESC to quit", GraphicRenderer.screenWidth/2, GraphicRenderer.screenHeight/2+20);
		textRenderer.endRendering();

	}
	
	public static void renderWinScreen() {
		TextRenderer textRenderer = new TextRenderer(new Font("Verdana", Font.BOLD, 35));
		textRenderer.beginRendering(GraphicRenderer.screenWidth, GraphicRenderer.screenHeight);
		textRenderer.setColor(Color.BLACK);
		textRenderer.setSmoothing(true);

		
		textRenderer.draw("Congratulations", GraphicRenderer.screenWidth/2-15, GraphicRenderer.screenWidth/2-15);
		textRenderer.draw("YOU WON!", GraphicRenderer.screenWidth/2, GraphicRenderer.screenHeight/2+20);
		textRenderer.endRendering();

	}
	
	public static void drawSnake(Player p) {
		drawObject(p.head);
		for(Tail t : p.tail) {
			drawObject(t);
			}
	}
	
	public static void setColor(float r , float g, float b , float a) {
		red = Math.max( 0, Math.min(1, r));
		green = Math.max( 0, Math.min(1, g));
		blue = Math.max( 0 , Math.min(1, b)) ;
		alpha = Math.max( 0 , Math.min(1, a));
		
	
	}
}

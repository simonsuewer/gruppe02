package game.input;

import java.awt.event.KeyEvent;

import game.engine.SnakeMain;
import game.world.GameStatus;
import game.world.Player;
import game.world.World;

public class SnakeKeyInput extends de.hsh.projekt.common.KeyInput {

    @Override
    public void keyPressed(KeyEvent event) {
	// System.out.println(event.getKeyCode());
	
	switch (event.getKeyCode()) {
	
	// left
	case 37:
	    if (!Player.getDirection().equals("right"))
		World.p.setDirection("left");

	    break;
	// up
	case 38:
		//System.out.println("oben");
		if (!Player.getDirection().equals("down"))
		World.p.setDirection("up");

	    break;
	// right
	case 39:
	    if (!Player.getDirection().equals("left"))
		World.p.setDirection("right");

	    break;
	// down
	case 40:
	    //System.out.println("unten");
		if (!Player.getDirection().equals("up"))
		World.p.setDirection("down");

	    break;

	// exit on esc

	case 27:
	    if(World.losingScreen) {
		//Runtime.getRuntime().exit(1);
		SnakeMain.staticbackToMain();}
	    
		//System.out.println("yPos ="+World.p.head.getyPos());
		break;
	}

	if (World.losingScreen && event.getKeyCode() == 10) {
	    GameStatus.reset();
	    World.init();
	    World.isLost = false;
	    World.losingScreen = false;
	}
    }

    @Override
    public void keyReleased(KeyEvent event) {
	// TODO Auto-generated method stub
	// System.out.println(event.getKeyCode());

    }

    @Override
    public void keyTyped(KeyEvent event) {
	// System.out.println(event.getKeyCode());
    }

}

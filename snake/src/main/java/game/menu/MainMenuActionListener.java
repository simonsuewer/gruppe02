package game.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.hsh.projekt.common.GameEventInitiater;
import game.engine.GameLoop;
import game.engine.SnakeMain;
import game.graphics.GraphicRenderer;
import game.sound.MusicPlayer;
import game.world.GameStatus;
import game.world.World;

public class MainMenuActionListener implements ActionListener {

	public static MusicPlayer mp;

	@Override
	public void actionPerformed(ActionEvent event) {

		if (event.getActionCommand().equals("Start Game")) {

			// initialize render components and a world, start the GameLoop
			if(MenuTest.rbEasy.isSelected())
				GameStatus.setDifficulty("easy");
			else if (MenuTest.rbMedium.isSelected())
				GameStatus.setDifficulty("medium");
			else if(MenuTest.rbHard.isSelected())
				GameStatus.setDifficulty("hard");
			GraphicRenderer.init();
			//BackgroundMusic.init();
			// new World();
			World.init();
			
			GameLoop.start();
			MenuTest.menuPanel.removeAll();
			MenuTest.menuPanel.validate();
			mp = new MusicPlayer("backgroundmusic");
			mp.run();
			// MenuTest.pnMenuPanel.setSize(new Dimension(GraphicRenderer.screenWidth,
			// GraphicRenderer.screenHeight));
			// jpanel.add(GraphicRenderer.getGLJPanel());
			GameEventInitiater.renewJPanel();
			//MenuTest.frame.getContentPane().add(GraphicRenderer.getGLJPanel());
			//MenuTest.frame.addKeyListener(new SnakeKeyInput());
			//MenuTest.frame.setSize(new Dimension(GraphicRenderer.screenWidth, GraphicRenderer.screenHeight));
			//MenuTest.frame.requestFocus();
			//MenuTest.frame.setVisible(true);
			//MenuTest.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		}
		System.out.println(event.getActionCommand());
		if (event.getActionCommand().equals("Exit")) {
			SnakeMain.staticbackToMain();
			GameEventInitiater.renewJPanel();
			}

	}

}

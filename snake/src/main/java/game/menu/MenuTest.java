package game.menu;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class MenuTest {
	public static JPanel menuPanel;
	static ButtonGroup rbgMenuPanel;
	static JButton btnStart;
	static JButton btnExit;
	static JRadioButton rbEasy;
	static JRadioButton rbMedium;
	static JRadioButton rbHard;
	//static JFrame frame = new JFrame();

	public static void init() {
		menuPanel = new JPanel();
		menuPanel.setBorder(BorderFactory.createTitledBorder("MainMenu"));
		rbgMenuPanel = new ButtonGroup();
		GridBagLayout gbMenuPanel = new GridBagLayout();
		GridBagConstraints gbcMenuPanel = new GridBagConstraints();
		menuPanel.setLayout(gbMenuPanel);

		btnStart = new JButton("Start Game");
		btnStart.addActionListener(new MainMenuActionListener());
		//btnStart.setBounds(200, 87, 200, 100);
		gbcMenuPanel.gridx = 10;
		gbcMenuPanel.gridy = 7;
		gbcMenuPanel.gridwidth = 3;
		gbcMenuPanel.gridheight = 2;
		gbcMenuPanel.fill = GridBagConstraints.BOTH;
		gbcMenuPanel.weightx = 100;
		gbcMenuPanel.weighty = 0;
		gbcMenuPanel.anchor = GridBagConstraints.CENTER;
		gbcMenuPanel.insets = new Insets(50, 50, 50, 50);
		gbMenuPanel.setConstraints(btnStart, gbcMenuPanel);
		menuPanel.add(btnStart);

		btnExit = new JButton("Exit");
		btnExit.setBounds(1,5,1,9);
		gbcMenuPanel.gridx = 10;
		gbcMenuPanel.gridy = 16;
		gbcMenuPanel.gridwidth = 3;
		gbcMenuPanel.gridheight = 2;
		gbcMenuPanel.fill = GridBagConstraints.BOTH;
		gbcMenuPanel.weightx = 100;
		gbcMenuPanel.weighty = 0;
		gbcMenuPanel.anchor = GridBagConstraints.CENTER;
		gbcMenuPanel.insets = new Insets(50, 50, 50, 50);
		gbMenuPanel.setConstraints(btnExit, gbcMenuPanel);
		menuPanel.add(btnExit);

		rbEasy = new JRadioButton("Easy");
		rbgMenuPanel.add(rbEasy);
		gbcMenuPanel.gridx = 10;
		gbcMenuPanel.gridy = 25;
		gbcMenuPanel.gridwidth = 1;
		gbcMenuPanel.gridheight = 1;
		gbcMenuPanel.fill = GridBagConstraints.BOTH;
		gbcMenuPanel.weightx = 1;
		gbcMenuPanel.weighty = 0;
		gbcMenuPanel.anchor = GridBagConstraints.NORTH;
		gbMenuPanel.setConstraints(rbEasy, gbcMenuPanel);
		menuPanel.add(rbEasy);

		rbMedium = new JRadioButton("Medium");
		rbgMenuPanel.add(rbMedium);
		gbcMenuPanel.gridx = 11;
		gbcMenuPanel.gridy = 25;
		gbcMenuPanel.gridwidth = 1;
		gbcMenuPanel.gridheight = 1;
		gbcMenuPanel.fill = GridBagConstraints.BOTH;
		gbcMenuPanel.weightx = 1;
		gbcMenuPanel.weighty = 0;
		gbcMenuPanel.anchor = GridBagConstraints.NORTH;
		gbMenuPanel.setConstraints(rbMedium, gbcMenuPanel);
		menuPanel.add(rbMedium);

		rbHard = new JRadioButton("Hard");
		rbHard.setSelected(true);
		rbgMenuPanel.add(rbHard);
		gbcMenuPanel.gridx = 12;
		gbcMenuPanel.gridy = 25;
		gbcMenuPanel.gridwidth = 1;
		gbcMenuPanel.gridheight = 1;
		gbcMenuPanel.fill = GridBagConstraints.BOTH;
		gbcMenuPanel.weightx = 1;
		gbcMenuPanel.weighty = 0;
		gbcMenuPanel.anchor = GridBagConstraints.NORTH;
		gbMenuPanel.setConstraints(rbHard, gbcMenuPanel);
		menuPanel.add(rbHard);
		//frame.setSize(new Dimension(1024, 768));
		//frame.setLocationRelativeTo(null);
		//frame.setVisible(true);
		//frame.getContentPane().add(menuPanel);

	}

	public static void main(String[] args) {
		init();
	}

}

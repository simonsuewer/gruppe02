package game.resource;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import com.jogamp.opengl.util.texture.Texture;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;


import game.graphics.GraphicRenderer;
import game.menu.MenuTest;

// TODO: Auto-generated Javadoc
/**
 * The Class ImageResource.
 */
public class ImageResource {

	/** The texture. */
	// open gl texture object
	private Texture texture = null;

	/** The image. */
	// bufferdimage
	private BufferedImage image = null;

	/**
	 * Instantiates a new image resource.
	 *
	 * @param path the path
	 */
	public ImageResource(String path) {
		URL url = MenuTest.class.getClassLoader().getResource(path);

		try {
			image = ImageIO.read(url);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (image != null) {
			image.flush();
		}

	}

	/**
	 * Gets the texture.
	 *
	 * @return the texture
	 */
	public Texture getTexture() {

		if (image == null) {
			return null;
		}
		if (texture == null) {
			texture = AWTTextureIO.newTexture(GraphicRenderer.getProfile(), image, true);
		}

		return texture;
	}
	
	/**
	 * Gets the image.
	 *
	 * @return the image
	 */
	public BufferedImage getImage(){
		if (image == null) {
			return null;
		}
		
		return image;
	}
	
	public String getInfo()
	{
		return getTexture().toString();
	}
}

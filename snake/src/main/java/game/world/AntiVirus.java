package game.world;

import game.graphics.Graphics;

public class AntiVirus extends Consumable{
	public static int spawnFrequency = 5;
	public AntiVirus(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void update() {
		if(isEaten()) {
			World.isLost=true;
			//World.gameStatus.update();
			}
		
	}

	@Override
	public void render() {
		if(visible) {
		Graphics.setColor(1, 0, 0, 1);
		Graphics.drawObject(this);
		Graphics.setColor(1, 1, 1, 1);
		}
		
	}
	
	@Override
	public void setEaten(boolean isEaten) {
		//System.out.println("println");
		this.isEaten=isEaten;
		
	}

}

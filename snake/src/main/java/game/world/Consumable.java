package game.world;

import java.util.Random;

import game.graphics.EventListener;
import game.graphics.GraphicRenderer;
import game.graphics.Graphics;

public class Consumable extends GameObject {
	public boolean isEaten = false;
	
	public Consumable(int x, int y) {
			setxPos(x);
			setyPos(y);
			setWidth(1);
			setHeight(1);
		
	}

	@Override
	public void update() {
		if(isEaten()) {
			GameStatus.highscore++;

			Random r = new Random();
			this.setxPos(r.nextInt(GraphicRenderer.unitsWide));
			this.setyPos(r.nextInt(EventListener.unitsTall));
			setEaten(false);
		}
	}
		
	

	@Override
	public void render() {
		Graphics.setColor(0, 1, 0, 1);
		Graphics.drawObject(this);
		Graphics.setColor(1, 1, 1, 1);
		
	}

	public boolean isEaten() {
		return isEaten;
	}

	public void setEaten(boolean isEaten) {
		this.isEaten = isEaten;
	}

}

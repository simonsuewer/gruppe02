package game.world;

public class EasterEgg extends GameObject {
	boolean activated = false;

	@Override
	public void update() {
		if (Highscore.highscore % 50 == 0) {
			activated = true;
		}
		
		if (activated) {
			if (Player.red < 1) {
				Player.red += 0.1f;
				}else {
					Player.red=0;
				}
			if (Player.green == 0) {
				Player.green = 1;
			} else {
				Player.green -= 0.1f;
			}
			if (Player.blue < 1) {
				Player.blue += 0.05f;
			}else {
				Player.blue=0f;
			}
		}
	}

	@Override
	public void render() {
		
	}

}

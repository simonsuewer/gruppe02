package game.world;

public abstract class GameObject {
	
	private int xPos = 0 ;
	private int yPos = 0;
	
	protected boolean visible = true;
	
	private int height = 0;
	private int width = 0;
	
	//private float rotation = 0;
	public int getxPos() {
		return xPos;
	}
	public void setxPos(int xPos) {
		this.xPos = xPos;
	}
	public int getyPos() {
		return yPos;
	}
	public void setyPos(int yPos) {
		this.yPos = yPos;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	
	public abstract void update();
	
	public abstract void render();
		
	
	
	
}

package game.world;

import java.util.Random;

import game.engine.SnakeMain;
import game.graphics.GraphicRenderer;

public class GameStatus extends GameObject {
	public static int highscore = 1;
	private static String difficulty = "easy";
	public static void reset() {
		highscore = 1;
		Highscore.highscore=1;
		difficulty = "medium";
		World.isLost=false;
		SnakeMain.sound=false;
	}
	public static void setDifficulty(String diff) {
		
		
		difficulty = diff;
		
	}
	public static String getDifficulty() {
		return difficulty;
	}
	@Override
	public void update() {
		if (World.isLost) {
			System.out.println("update reset");
			GameStatus.reset();
			 World.losingScreen = true;
			 World.gameObjects.clear();
			 World.addObject(new LoseScreen());
			
			
			
		}
		if(World.p.tail.size()+1+World.consumables.size()>GraphicRenderer.unitsWide*GraphicRenderer.unitsTall) {
			GameStatus.reset();
			 World.gameObjects.clear();
			 World.addObject(new WinScreen());
		}
		if(highscore%AntiVirus.spawnFrequency==0) {
			Random r = new Random();
			boolean spawnable = false;
			int x = 0;
			int y = 0;
			while(!spawnable) {
				x = r.nextInt(GraphicRenderer.unitsWide);
				y = r.nextInt(GraphicRenderer.unitsTall);
				spawnable =true;
			for(GameObject go: World.gameObjects) {
				if(x==go.getxPos()&&y==go.getyPos())
					spawnable = false;
				
			}
			for(GameObject go:World.consumables) {
				if(x==go.getxPos()&&y==go.getyPos())
					spawnable = false;
			}
			
			}
			AntiVirus av = new AntiVirus(x,y);
			World.gameObjects.add(av);
			World.consumables.add(av);
			highscore++;
			
		}
		
		
		
	}
	@Override
	public void render() {
		// TODO Auto-generated method stub
		
	}
}

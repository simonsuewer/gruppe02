package game.world;

import game.graphics.Graphics;

public class Head extends GameObject {
		public Head(int xPos, int yPos) {
			setxPos(xPos);
			setyPos(yPos);
			setWidth(1);
			setHeight(1);
		}

		@Override
		public void update() {
			//
			
		}

		@Override
		public void render() {
			Graphics.fillRect(this.getxPos(), this.getyPos(), 5, 5);
		}
}

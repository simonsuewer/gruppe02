package game.world;

import game.graphics.Graphics;

public class Highscore extends GameObject {
	public static int highscore = 1;

	@Override
	public void update() {
		highscore = GameStatus.highscore;
		
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		Graphics.renderHighscore(highscore);
		
	}
}

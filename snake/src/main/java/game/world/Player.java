package game.world;

import java.util.ArrayList;

import game.engine.GameLoop;
import game.graphics.GraphicRenderer;
import game.graphics.Graphics;

public class Player extends GameObject {
    public Head head = null;
    public ArrayList<Tail> tail = new ArrayList<Tail>();
    private static String direction = "up";
    private int tempx1 = 0;
    private int tempx2 = 0;
    private int tempy1 = 0;
    private int tempy2 = 0;
    private int deltaX = 1;
    public int speed = 4;
    public boolean toggleVisibility = false;
    public static float red = 0;
    public static float blue = 0;
    public static float green = 0;

    public Player(int x, int y) {
	head = new Head(x, y);

    }

    public void update() {
	for (Tail t : tail) {
	    World.obstacle.add(new Obstacle(t.getxPos(), t.getyPos()));
	}
	if (GameLoop.framecount % speed == 0) {
	    // System.out.println("update");

	    move(direction);

	    // GameLoop.framecount = 1;
	}
	checkCollision();
    }

    public void render() {
	if(toggleVisibility&&GameLoop.cyclesDone<1) {
		this.visible=false;
	}
    if (this.visible) {
	    Graphics.setColor(red,blue,green,1);
	    Graphics.drawSnake(this);
	    Graphics.setColor(1, 1, 1, 1);
	}
    this.visible=true;
    }

    public void setDirection(String d) {
	direction = d;
    }

    public static String getDirection() {
	return direction;
    }

    public void move(String direction) {
	tempx1 = head.getxPos();
	tempy1 = head.getyPos();
	tempx2 = 0;
	tempy2 = 0;
	// head.setxPos(head.getxPos()+speed);
	switch (direction) {
	case "right":
	    if (head.getxPos() + deltaX > GraphicRenderer.unitsWide) {
		World.isLost=true;
	    } else {
		head.setxPos(head.getxPos() + deltaX);
	    }
	    break;
	case "left":
	    if (head.getxPos() - deltaX < 0) {
	    	World.isLost=true;
	    } else {
		head.setxPos(head.getxPos() - deltaX);
	    }
	    break;
	case "up":
	    if (head.getyPos() + deltaX > GraphicRenderer.unitsTall+1) {
	    	World.isLost=true;
	    } else {
		head.setyPos(head.getyPos() + deltaX);
	    }
	    break;
	case "down":
	    System.out.println(head.getyPos());
		if (head.getyPos()-deltaX<0) {
	    	World.isLost=true;
	    } else {
		head.setyPos(head.getyPos() - deltaX);
		break;
	    }

	}

	for (int i = 0; i < tail.size(); i++) {
	    tempx2 = tail.get(i).getxPos();
	    tempy2 = tail.get(i).getyPos();

	    tail.get(i).setxPos(tempx1);
	    tail.get(i).setyPos(tempy1);

	    tempx1 = tempx2;
	    tempy1 = tempy2;
	}

    }

    public void checkCollision() {

	// Consumable collision
	if (head.getxPos() == World.c.getxPos() && head.getyPos() == World.c.getyPos()) {

	    int x = 0;
	    int y = 0;
	    int width = 1;
	    if (tail.size() > 0) {
		switch (Player.getDirection()) {
		case "right":
		    x = tail.get(tail.size() - 1).getxPos() - width;
		    y = tail.get(tail.size() - 1).getyPos();
		    break;

		case "left":
		    x = tail.get(tail.size() - 1).getxPos() + width;
		    y = tail.get(tail.size() - 1).getyPos();
		    break;

		case "up":
		    x = tail.get(tail.size() - 1).getxPos();
		    y = tail.get(tail.size() - 1).getyPos() - width;
		    break;

		case "down":
		    x = tail.get(tail.size() - 1).getxPos();
		    y = tail.get(tail.size() - 1).getyPos() + width;
		    break;

		}
	    } else {
	    	switch (Player.getDirection()) {
			case "right":
			    x = head.getxPos() - width;
			    y = head.getyPos();
			    break;

			case "left":
			    x = head.getxPos() + width;
			    y = head.getyPos();
			    break;

			case "up":
			    x = head.getxPos();
			    y = head.getyPos() - width;
			    break;

			case "down":
			    x = head.getxPos();
			    y = head.getyPos() + width;
			    break;

			}
	    }

	    tail.add(new Tail(x, y));
	    
	    World.c.setEaten(true);
	}
	// Virus collision
	for (Consumable c : World.consumables) {
	    if (head.getxPos() == c.getxPos() && head.getyPos() == c.getyPos()) {

//			tail.add(new Tail(x, y));

		c.setEaten(true);
	    }
	}

	// Obstacle collison

	for (Obstacle o : World.obstacle) {
	    if (head.getxPos() == o.x && head.getyPos() == o.y) {
		World.isLost = true;
	    }
	}
    }

    public void ttoString() {
	System.out.println("Head Position" + head.getxPos() + " " + head.getyPos());
	for (Tail t : tail) {
	    System.out.println("Tail" + t.getxPos() + " " + t.getyPos());
	}

    }

    public void setSpeed(int speed) {
	this.speed = speed;
    }

    public void toggleVisibilty(boolean b) {
	this.toggleVisibility = b;

    }
}

package game.world;

import game.graphics.Graphics;

public class Tail extends GameObject {

	public Tail(int xPos, int yPos) {
		this.setxPos(xPos);
		this.setyPos(yPos);
		this.setWidth(1);
		this.setHeight(1);
	}

	@Override
	public void update() {
		
		
	}

	@Override
	public void render() {
		
		Graphics.fillRect(this.getxPos(), this.getyPos(), this.getWidth(), this.getHeight());
		
	}

}

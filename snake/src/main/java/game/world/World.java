package game.world;

import java.util.ArrayList;

import game.graphics.GraphicRenderer;

public class World {
	public static Player p = null;
	public static ArrayList<GameObject> gameObjects = null;
	public static ArrayList<Obstacle> obstacle = null;
	public static Consumable c = null;
	//public static ArrayList<AntiVirus> antiVirus = null;
	public static Highscore highscore = null;
	// public static boolean firstrun = true;
	public static boolean isLost = false;
	public static ArrayList<Consumable> consumables;
	public static GameStatus gameStatus = new GameStatus();
	public static boolean losingScreen = false;
	public static void init() {
		p = new Player((int) GraphicRenderer.unitsWide / 2, (int) GraphicRenderer.unitsWide / 2);
		// antiVirus = new ArrayList<AntiVirus>();
		// antiVirus.add(new AntiVirus(40,50));
		highscore = new Highscore();
		if (GameStatus.getDifficulty().equals("easy")) {
			p.speed = 4;
			AntiVirus.spawnFrequency = 10;

		}
		if (GameStatus.getDifficulty().equals("medium")) {
			p.speed = 3;
			AntiVirus.spawnFrequency = 7;
		}
		if (GameStatus.getDifficulty().equals("hard")) {
			p.speed = 2;
			p.toggleVisibilty(true);
			AntiVirus.spawnFrequency = 4;
		}
		gameObjects = new ArrayList<GameObject>();
		obstacle = new ArrayList<Obstacle>();
		c = new Consumable(30, 20);
		consumables = new ArrayList<Consumable>();
		//consumables.add(c);
		addObject(p);
		addObject(c);
		addObject(new EasterEgg());
		addObject(highscore);
		//addObject(gameStatus);
		// addObject(new AntiVirus(20,10));
	}

	public static void reset() {
		gameObjects = null;
		consumables.clear();
		p = null;
		c = null;
		//antiVirus = null;
	}

	public static void update() {
		
		obstacle.clear();
		for (GameObject go : gameObjects) {
			go.update();
		}
		gameStatus.update();
	}
		

	public static void render() {

		for (GameObject go : gameObjects) {
			go.render();
		}

	}

	public static void addObject(GameObject go) {
		gameObjects.add(go);

	}
}

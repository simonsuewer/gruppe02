package de.projekt.input;

import java.awt.event.KeyEvent;

import de.projekt.start.Main;

public class KeyListener implements java.awt.event.KeyListener {

    @Override
    public void keyTyped(KeyEvent e) {

	if (Main.startedGame != null) {
	    Main.startedGame.getKeyInput().keyTyped(e);
	}

    }

    @Override
    public void keyPressed(KeyEvent e) {

	if (Main.startedGame != null) {
	    Main.startedGame.getKeyInput().keyPressed(e);

	}

    }

    @Override
    public void keyReleased(KeyEvent e) {

	if (Main.startedGame != null) {
	    Main.startedGame.getKeyInput().keyReleased(e);
	}

    }

}

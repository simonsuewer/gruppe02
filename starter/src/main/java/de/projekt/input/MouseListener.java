package de.projekt.input;

import java.awt.event.MouseEvent;

import de.projekt.start.Main;

public class MouseListener implements java.awt.event.MouseListener{

    @Override
    public void mouseClicked(MouseEvent e) {
	if(Main.startedGame!= null) {
	    Main.startedGame.getMouseInput().mouseClicked(e);
	}
	
    }

    @Override
    public void mousePressed(MouseEvent e) {
	if(Main.startedGame!= null) {
	    Main.startedGame.getMouseInput().mousePressed(e);
	}
	
    }

    @Override
    public void mouseReleased(MouseEvent e) {
	if(Main.startedGame!= null) {
	    Main.startedGame.getMouseInput().mouseReleased(e);
	}
	
    }

    @Override
    public void mouseEntered(MouseEvent e) {
	if(Main.startedGame!= null) {
	    Main.startedGame.getMouseInput().mouseEntered(e);
	}
	
    }

    @Override
    public void mouseExited(MouseEvent e) {
	if(Main.startedGame!= null) {
	    Main.startedGame.getMouseInput().mouseExited(e);
	}
	
    }

}

package de.projekt.listener;

import java.awt.BorderLayout;

import javax.swing.JPanel;

import de.hsh.projekt.common.GameEventListener;
import de.projekt.start.Main;
import de.projekt.start.StartMenu;

public class GameEventResponder implements GameEventListener {

    @Override
    public void renewJPanel() {

	System.out.println("renewJPanel EVENT fire");
	Main.frame.getContentPane().removeAll();
	//Main.frame.getJMenuBar().removeAll();
	
	JPanel panel = Main.startedGame.getPanel();
	if(panel != null) {
	    Main.frame.getContentPane().add(panel, BorderLayout.CENTER);
	} else{
	    Main.frame.getContentPane().add(Main.startedGame.getJFXPanel(), BorderLayout.CENTER); 
	}
	Main.frame.setJMenuBar(Main.startedGame.getJMenuBar());
	Main.frame.revalidate();
	Main.frame.repaint();

    }

    @Override
    public void exitGame() {
	System.out.println("exitGame EVENT fire");
	Main.frame.getContentPane().removeAll();
	Main.frame.getContentPane().add(StartMenu.getLaunchMenu(), BorderLayout.CENTER);
	//Main.frame.getJMenuBar().removeAll();
	Main.frame.revalidate();
	Main.frame.repaint();
	
    }

}

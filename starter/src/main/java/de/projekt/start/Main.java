/*
 * 
 */
package de.projekt.start;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import de.hsh.max.starter.MoonlanderStarter;
import de.hsh.patrick.starter.VirattaXStarter;
import de.hsh.projekt.common.Game;
import de.hsh.projekt.common.GameEventInitiater;
import de.hsh.projekt.common.StaticSettings;
import de.projekt.input.KeyListener;
import de.projekt.input.MouseListener;
import de.projekt.listener.GameEventResponder;
//import de.simon.suewer.hackers.engine.HackersMain;
import de.simon.suewer.hackers.engine.HackersMain;
import dev.maxolle.starter.MalwareShooterMain;
import engine.VirusDefender;
import game.engine.SnakeMain;

// TODO: Auto-generated Javadoc
/**
 * The Class Main.
 */
public class Main {
    /** The Constant frame. */
    public static final JFrame frame = new JFrame(); // instantiation
    public static final List<Game> games = new ArrayList<Game>();
    public static Game startedGame = null;

    static {
	GameEventInitiater.addListener(new GameEventResponder());
	// games.add(new Main);
	games.add(new HackersMain());
	games.add(new VirusDefender());
	games.add(new VirattaXStarter());
	games.add(new MalwareShooterMain());
	
	games.add(new SnakeMain());
	games.add(new MoonlanderStarter());
    }

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
	Main.frame.setSize(new Dimension(StaticSettings.SCREEN_WIDTH, StaticSettings.SCREEN_HEIGHT));
	Main.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	Main.frame.getContentPane().add(StartMenu.getLaunchMenu(), BorderLayout.CENTER);
	Main.frame.addKeyListener(new KeyListener());

	Main.frame.addMouseListener(new MouseListener());
	Main.frame.setFocusable(true);
	Main.frame.setVisible(true);

    }

    public static void backToLaunchMenu() {

	Main.frame.getContentPane().removeAll();
	Main.frame.getContentPane().add(StartMenu.getLaunchMenu(), BorderLayout.CENTER);

	Main.frame.revalidate();
	Main.frame.repaint();

    }

    public static void startGame(Game game) {
	startedGame = game;
	startedGame.start();
	GameEventResponder responder = new GameEventResponder();
	responder.renewJPanel();
    }

}

/*
 * 
 */
package de.projekt.start;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import de.hsh.projekt.common.Game;

// TODO: Auto-generated Javadoc
/**
 * The Class StartMenu.
 */
public class StartMenu {

    /** The menu. */
    private static JPanel menu;

    private static Game selectedGame;

    private static JLabel info;
    private static JLabel infoLabel;
    /**
     * Create the application.
     */
    static {
	initialize();
    }

    /**
     * Initialize the contents of the frame.
     * 
     * @wbp.parser.entryPoint
     */
    private static void initialize() {
	menu = new JPanel();
	menu.setBackground(Color.GRAY);
	menu.setLayout(null);

	int x = 15;
	for (Game game : Main.games) {
	    JButton btnGame = new JButton(game.getName());

	    btnGame.setBounds(100, x, 200, 85);
	    x += 104;
	    // btnStart.setIcon(
	    // new
	    // ImageIcon(StartMenu.class.getResource("/res/baseline_power_settings_new_black_18dp.png")));
	    btnGame.setForeground(Color.BLACK);
	    btnGame.setBackground(Color.GREEN);
	    btnGame.addActionListener(new ActionListener() {

		@Override
		public void actionPerformed(ActionEvent e) {
		    // Main.startGame(game);
		    selectedGame = game;
		    info.setText(selectedGame.getName());
		    infoLabel.setText(selectedGame.getDescription());
		    menu.revalidate();
		    menu.repaint();
		}
	    });

	    menu.add(btnGame);

	}

	JPanel panel = new JPanel();
	panel.setBounds(400, 15, 600, 600);
	panel.setBackground(Color.DARK_GRAY);
	menu.add(panel);
	panel.setLayout(null);

	info = new JLabel("", SwingConstants.CENTER);
	info.setForeground(Color.WHITE);
	info.setBounds(6, 6, 500, 16);

	panel.add(info);

	infoLabel = new JLabel("", SwingConstants.CENTER);
	infoLabel.setForeground(Color.WHITE);
	infoLabel.setBounds(50, 61, 500, 247);
	panel.add(infoLabel);

	JButton btnStart = new JButton("Start");
	btnStart.setBounds(790, 700, 200, 80);

	btnStart.setForeground(Color.BLACK);
	btnStart.setBackground(Color.YELLOW);
	btnStart.setOpaque(true);
	btnStart.setBorderPainted(false);
	btnStart.setFont(new Font("Arial", Font.BOLD, 30));
	btnStart.addActionListener(new ActionListener() {

	    @Override
	    public void actionPerformed(ActionEvent e) {
		if (selectedGame != null) {
		    Main.startGame(selectedGame);
		}
	    }
	});

	menu.add(btnStart);

    }

    /**
     * Ge launch menu.
     *
     * @return the j panel
     */
    public static JPanel getLaunchMenu() {
	return menu;
    }

}

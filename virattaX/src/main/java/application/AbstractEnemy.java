package application;

import java.io.File;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Rectangle;

/**
 * 
 * @author Patrick Schaper
 *
 */
public abstract class AbstractEnemy {
    public int width;
    public int height;
    public int points;
    public double xCord;
    public double yCord;
    public String backgroundImage;
    public String path = getClass().getClassLoader().getResource("images/").toExternalForm().toString();
    public String localUrl;
    public File file;
    public Image image;
    public Rectangle rectangle;
    public Boolean life = true;
    public Boolean clicked = false;

    /**
     * 
     * @param xCord start value
     * @param yCord start value
     * @param width
     * @param height
     * @param backgroundImage imagename with postfix
     * @param points
     */
    AbstractEnemy(int xCord, int yCord, int width, int height, String backgroundImage, int points) {
        this.image = new Image(path + backgroundImage);
        this.setxCord(xCord);
        this.setyCord(yCord);
        this.setWidth(width);
        this.setHeight(height);
        this.setBackgroundImage(backgroundImage);
        this.setPoints(points);

        this.initDraw();

    }

    /**
     * initial draw
     */
    void initDraw() {
        this.rectangle = new Rectangle(this.getxCord(), this.getyCord(), this.getWidth(), this.getHeight());
        this.rectangle.setFill(new ImagePattern(image, 0, 0, 1, 1, true));
        this.rectangle.getStyleClass().add("enemy");
        this.rectangle.setOnMousePressed(event -> {
            setLife(false);
            setClicked(true);
        });
    }

    /**
     * 
     * @return this rectangle object
     */
    Rectangle getRectangle() {
        this.moveRec();
        return this.rectangle;
    }

    /**
     * move rectangle to new position
     */
    void moveRec() {
        this.rectangle.setY(this.getyCord());
        this.rectangle.setX(this.getxCord());
    }

    /**
     * 
     * @param deltaTime
     */
    abstract void move(double deltaTime);

    /**
     * @return the life
     */
    public Boolean getLife() {
        return life;
    }

    /**
     * @param life
     *            the life to set
     */
    public void setLife(Boolean life) {
        this.life = life;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public int getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(int height) {
        this.height = height;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points
     *            the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @return the xCord
     */
    public double getxCord() {
        return xCord;
    }

    /**
     * @param xCord
     *            the xCord to set
     */
    public void setxCord(double xCord) {
        this.xCord = xCord;
    }

    /**
     * @return the yCord
     */
    public double getyCord() {
        return yCord;
    }

    /**
     * @param yCord
     *            the yCord to set
     */
    public void setyCord(double yCord) {
        this.yCord = yCord;
    }

    /**
     * @return the backgroundImage
     */
    public String getBackgroundImage() {
        return backgroundImage;
    }

    /**
     * @param backgroundImage
     *            the backgroundImage to set
     */
    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    /**
     * @return the clicked
     */
    public Boolean getClicked() {
        return clicked;
    }

    /**
     * @param clicked
     *            the clicked to set
     */
    public void setClicked(Boolean clicked) {
        this.clicked = clicked;
    }
}

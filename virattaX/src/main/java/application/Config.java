package application;

/**
 * 
 * @author Patrick Schaper
 * class for global values
 *
 */
public class Config {
    public static final int windowWidth = 1024;
    public static final int windowHeight = 768;
    public static final int gameWidth = windowWidth;
    public static final int gameHeight = (windowHeight / 8) * 7;
    public static final int footerHeight = windowHeight - gameHeight;
}

package application;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class EnemyBasic extends AbstractEnemy {
    
    EnemyBasic(int xCord, int yCord) {
        super(xCord, yCord, 50, 64, "EnemyBasic.png", 5);
        // TODO Auto-generated constructor stub
    }
    
    @Override
    void move(double deltaTime) {
        if (deltaTime < 1) {
            super.setyCord(super.getyCord() + (60 * deltaTime));
        }
    }

}

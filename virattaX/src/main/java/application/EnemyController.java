package application;

import java.util.ArrayList;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class EnemyController {

    private ArrayList<AbstractEnemy> liveEnemies;
    private ArrayList<AbstractEnemy> toDeleteEnemies;
    private EnemyFactory enemyFactory;

    EnemyController() {
        this.setLiveEnemies(new ArrayList<>());
        this.setToDeleteEnemies(new ArrayList<>());
        this.setEnemyFactory(EnemyFactory.getInstance());
        this.getEnemyFactory().setLiveEnemies(this.liveEnemies);
    }

    /**
     * 
     * @return
     */
    public ArrayList<AbstractEnemy> getLiveEnemies() {
        return liveEnemies;
    }

    /**
     * 
     * @param liveEnemies
     */
    public void setLiveEnemies(ArrayList<AbstractEnemy> liveEnemies) {
        this.liveEnemies = liveEnemies;
    }

    /**
     * 
     * @return
     */
    public ArrayList<AbstractEnemy> getToDeleteEnemies() {
        return toDeleteEnemies;
    }

    /**
     * 
     * @param toDeleteEnemies
     */
    public void setToDeleteEnemies(ArrayList<AbstractEnemy> toDeleteEnemies) {
        this.toDeleteEnemies = toDeleteEnemies;
    }

    /**
     * 
     * @return enemyFactory
     */
    public EnemyFactory getEnemyFactory() {
        return enemyFactory;
    }

    /**
     * 
     * @param enemyFactory
     */
    public void setEnemyFactory(EnemyFactory enemyFactory) {
        this.enemyFactory = enemyFactory;
    }

}

package application;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class EnemyFactory {
    private static EnemyFactory instance;
    ArrayList<AbstractEnemy> liveEnemies;

    /**
     * create singleton
     * @return
     */
    public static EnemyFactory getInstance() {
        if (EnemyFactory.instance == null) {
            EnemyFactory.instance = new EnemyFactory();
        }
        return EnemyFactory.instance;
    }

    /**
     * 
     * @param numberForWhichEnemy 1 or 2
     * @param numberOfEnemies how much create
     */
    public void createEnemies(int numberForWhichEnemy, int numberOfEnemies) {
        this.create(numberForWhichEnemy, numberOfEnemies);
    }

    /**
     * 
     * @param numberForWhichEnemy 1 or 2
     * @param numberOfEnemies how much create
     */
    private void create(int numberForWhichEnemy, int numberOfEnemies) {
        AbstractEnemy enemy = null;
        Random random = new Random();
        int maxY = (Config.gameHeight / 3) * 2;
        int minY = 0;

        for (int i = 0; i < numberOfEnemies; i++) {
            int xCord = getXCordForCreate();

            int yCord = minY + random.nextInt(maxY);

            switch (numberForWhichEnemy) {
            case 1:
                enemy = (EnemyBasic) new EnemyBasic(xCord, -50 - (yCord));
                break;
            case 2:
                enemy = (EnemyJump) new EnemyJump(xCord, -50 - (yCord));
                break;
            default:
                enemy = null;
                break;
            }

            liveEnemies.add(enemy);
        }
    }

    /**
     * 
     * @return xCord for create Point
     */
    private int getXCordForCreate() {
        int min = 0;
        int max = Config.gameWidth - 50;
        
        int xCord = ThreadLocalRandom.current().nextInt(min, max + 1);
        
        return xCord;
    }

    /**
     * @return the liveEnemies
     */
    public ArrayList<AbstractEnemy> getLiveEnemies() {
        return liveEnemies;
    }

    /**
     * @param liveEnemies
     *            the liveEnemies to set
     */
    public void setLiveEnemies(ArrayList<AbstractEnemy> liveEnemies) {
        this.liveEnemies = liveEnemies;
    }
}

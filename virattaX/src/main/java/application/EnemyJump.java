package application;

import java.util.Random;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class EnemyJump extends AbstractEnemy {

    int numberWayChange = 50;
    int counterForWayChange = 0;
    int way = 0;

    EnemyJump(int xCord, int yCord) {
        super(xCord, yCord, 50, 50, "EnemyJump.png", 10);
        // TODO Auto-generated constructor stub
    }

    @Override
    void move(double deltaTime) {
        this.counterForWayChange++;

        Random random = new Random();
        int maxX = 3;
        int minX = 0;
        int randomNum = random.nextInt(maxX) + minX;

        this.toggleWay();

        double newX = super.getxCord() + (((way % 2) == 0 ? 1 : -1) * randomNum);

        if (newX < 0) {
            newX = 0;
        }

        // TODO Auto-generated method stub
        super.setyCord(super.getyCord() + randomNum);
        super.setxCord(newX);
    }

    /**
     * change the way
     */
    void toggleWay() {
        if (this.counterForWayChange >= this.numberWayChange) {
            this.way++;
            this.counterForWayChange = 0;
        }
    }

}

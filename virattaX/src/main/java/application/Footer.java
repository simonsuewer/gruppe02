package application;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class Footer {
    public Pane mainContent;
    public Pane footerPane;
    public Sound gameSound;
    public HighScoreController highScoreController;
    public int soundClicked;

    /**
     * 
     * @param mainContent
     * @param gameSound
     * @param highScoreController
     */
    Footer(Pane mainContent, Sound gameSound, HighScoreController highScoreController) {
        this.mainContent = mainContent;
        this.gameSound = gameSound;
        this.highScoreController = highScoreController;
        this.createFooterPane();
    }

    /**
     * create footer pane
     */
    private void createFooterPane() {
        Button btn_sound;
        VBox wrapperSound;

        btn_sound = new Button("\uf028");
        // btn_sound = new Button("Sound");
        btn_sound.getStyleClass().add("button--sound");

        btn_sound.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent e) {
                if (btn_sound.getStyleClass().remove("button--active")) {
                    gameSound.play();
                    btn_sound.setText("\uf028");
                } else {
                    btn_sound.getStyleClass().add("button--active");
                    gameSound.pause();
                    btn_sound.setText("\uf026");
                }

                if (soundClicked >= 5 && soundClicked % 2 == 0) {
                    String hex = getHexForEaserEgg();
                    mainContent.setStyle("-fx-background-color: #" + hex);
                    footerPane.setStyle("-fx-background-color: #" + hex);
                }
                soundClicked++;
            }
        });

        wrapperSound = new VBox();
        wrapperSound.getChildren().addAll(btn_sound);
        wrapperSound.getStyleClass().add("mainSoundBtnWrapper");

        this.footerPane = new Pane();
        this.footerPane.setMinWidth(Config.gameWidth);
        this.footerPane.setMaxWidth(Config.gameWidth);
        this.footerPane.setMinHeight(Config.footerHeight);
        this.footerPane.setMaxHeight(Config.footerHeight);
        this.footerPane.getStyleClass().add("footer");

        VBox footerWrapper = getFooterWrapper();

        footerWrapper.getChildren().addAll(wrapperSound);

        this.footerPane.getChildren().addAll(this.highScoreController.getGui().getScoreLabel(), footerWrapper);

    }

    /**
     * 
     * @return hex value for changing color
     */
    public String getHexForEaserEgg() {

        String hex = Integer.toHexString(soundClicked);
        if (hex.length() == 1) {
            hex = hex + "00";
        }
        if (hex.length() == 2) {
            hex = hex + "0";
        }

        return hex;
    }

    /**
     * 
     * @return
     */
    public Pane getFooterPane() {
        return this.footerPane;
    }

    /**
     * 
     * @return
     */
    private VBox getFooterWrapper() {
        VBox footerWrapper = new VBox();
        footerWrapper.getStyleClass().add("vbox");
        footerWrapper.getStyleClass().add("footerWrapper");

        return footerWrapper;
    }

}

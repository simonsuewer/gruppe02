package application;

import javafx.scene.layout.Pane;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class Game {

    private HighScoreController highScoreController;
    private int gameViewWidth;
    private int gameViewHeight;
    private GameController gameController;
    private int startWave = 1;
    private int endWave = 3;
    private int currentWave = this.getStartWave();
    private boolean gameActiveState = true;
    protected Pane gameView;

    /**
     * 
     * @param gameViewWidth
     * @param gameViewHeight
     * @param highScoreController
     */
    Game(int gameViewWidth, int gameViewHeight, HighScoreController highScoreController) {
        this.setHighScoreController(highScoreController);
        this.genereateGameView(gameViewWidth, gameViewHeight);
    }

    /**
     * create gameView with width and height
     * @param gameViewWidth
     * @param gameViewHeight
     */
    private void genereateGameView(int gameViewWidth, int gameViewHeight) {
        this.setGameView(new Pane());
        this.setGameViewWidth(gameViewWidth);
        this.setGameViewWidth(gameViewHeight);
        this.gameView.setMinWidth(this.getGameViewWidth());
        this.gameView.setMaxWidth(this.getGameViewWidth());
        this.gameView.setMinHeight(this.getGameViewHeight());
        this.gameView.setMaxHeight(this.getGameViewHeight());
    }

    /**
     * 
     * @return
     */
    public HighScoreController getHighScoreController() {
        return highScoreController;
    }

    /**
     * 
     * @param highScoreController
     */
    public void setHighScoreController(HighScoreController highScoreController) {
        this.highScoreController = highScoreController;
    }

    /**
     * 
     * @return
     */
    public int getGameViewWidth() {
        return gameViewWidth;
    }

    /**
     * 
     * @param gameViewWidth
     */
    public void setGameViewWidth(int gameViewWidth) {
        this.gameViewWidth = gameViewWidth;
    }

    /**
     * 
     * @return
     */
    public int getGameViewHeight() {
        return gameViewHeight;
    }

    /**
     * 
     * @param gameViewHeight
     */
    public void setGameViewHeight(int gameViewHeight) {
        this.gameViewHeight = gameViewHeight;
    }

    /**
     * 
     * @return
     */
    public GameController getGameController() {
        return gameController;
    }

    /**
     * 
     * @param gameController
     */
    public void setGameController(GameController gameController) {
        this.gameController = gameController;
    }

    /**
     * 
     * @return
     */
    public int getStartWave() {
        return startWave;
    }

    /**
     * 
     * @param startWave
     */
    public void setStartWave(int startWave) {
        this.startWave = startWave;
    }

    /**
     * 
     * @return
     */
    public int getEndWave() {
        return endWave;
    }

    /**
     * 
     * @param endWave
     */
    public void setEndWave(int endWave) {
        this.endWave = endWave;
    }

    /**
     * 
     * @return
     */
    public int getCurrentWave() {
        return currentWave;
    }

    /**
     * 
     * @param newWave
     */
    public void setCurrentWave(int newWave) {
        this.currentWave = newWave;
    }

    /**
     * 
     * @return
     */
    public boolean getGameActiveState() {
        return gameActiveState;
    }

    /**
     * 
     * @param gameActiveState
     */
    public void setGameActiveState(boolean gameActiveState) {
        this.gameActiveState = gameActiveState;
    }

    /**
     * 
     * @return
     */
    public Pane getGameView() {
        return gameView;
    }

    /**
     * 
     * @param gameView
     */
    public void setGameView(Pane gameView) {
        this.gameView = gameView;
    }

}

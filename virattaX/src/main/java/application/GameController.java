package application;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class GameController {

    private Game game;
    private GameLoop gameLoop;
    private EnemyController enemyController;
    private PlayControl playControl;

    /**
     * 
     * @param highScoreController
     */
    GameController(HighScoreController highScoreController) {
        this.setGame(new Game(Config.gameWidth, Config.gameHeight, highScoreController));
        this.setEnemyController(new EnemyController());
        this.setPlayControl(new PlayControl(this.game, this.enemyController));
        this.setGameLoop(new GameLoop(this.playControl));
    }

    /**
     * start game
     */
    public void start() {
        this.game.setGameActiveState(true);
        this.game.setCurrentWave(this.game.getStartWave());
        this.game.getHighScoreController().resetPoints();
        this.gameLoop.start();
    }

    /**
     * stop game
     */
    public void stop() {
        this.gameLoop.stop();
    }

    /**
     * 
     * @return
     */
    public Game getGame() {
        return this.game;
    }

    /**
     * 
     * @param game
     */
    private void setGame(Game game) {
        this.game = game;
    }

    /**
     * 
     * @param playControl
     */
    private void setPlayControl(PlayControl playControl) {
        this.playControl = playControl;
    }

    /**
     * 
     * @param enemyController
     */
    private void setEnemyController(EnemyController enemyController) {
        this.enemyController = enemyController;
    }

    /**
     * 
     * @param gameLoop
     */
    private void setGameLoop(GameLoop gameLoop) {
        this.gameLoop = gameLoop;
    }

}

package application;

import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Platform;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class GameLoop {
    Timer timer;
    PlayControl playControl;

    /**
     * 
     * @param playControl
     */
    GameLoop(PlayControl playControl) {
        this.timer = new Timer();
        this.playControl = playControl;
    }

    /**
     * stop gameloop
     */
    public void stop() {
        playControl.stopGame();
    }

    /**
     * start gameloop
     */
    public void start() {
        this.playControl.initGame();

        this.timer.schedule(new TimerTask() {
            double time = 1;
            double lastTime;
            // DEBUG STUFF
            //int fps;
            // DEBUG STUFF

            @Override
            public void run() {
                if (playControl.gameIfActiv()) {
                    double timestamp = System.nanoTime() / 1000000000.0;
                    double passedTime = timestamp - lastTime;
                    time = time - passedTime;
                    lastTime = timestamp;

                    // DEBUG STUFF
//                    fps = fps + 1;
//
//                    if (time <= 0) {
//                        System.out.println("Frames: " + fps);
//                        fps = 0;
//                        time = 1;
//                    }
                    // DEBUG STUFF

                    Platform.runLater(() -> playControl.clearView());

                    playControl.action(passedTime);

                } else {
                    // System.out.println("GameZuende");
                    this.cancel();
                    Platform.runLater(() -> playControl.showHighScorePage());
                }
            }
        }, 0, 16);
    }
}

package application;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class HighScore implements Comparable<Object> {
    private String name;
    private int round;
    private int points;

    HighScore() {

    }

    /**
     * 
     * @param name
     * @param round
     * @param points
     */
    HighScore(String name, int round , int points) {
        setName(name);
        setRound(round);
        setPoints(points);
    }

    /**
     * 
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     */
    public int getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * 
     * @return
     */
    public int getRound() {
        return round;
    }

    /**
     * 
     * @param round
     */
    public void setRound(int round) {
        this.round = round;
    }

    @Override
    public int compareTo(Object o) {
        int comparePoints = ((HighScore)o).getPoints();
        
        return this.getPoints()-comparePoints;
    }
}

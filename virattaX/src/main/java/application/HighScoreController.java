package application;

import javafx.scene.layout.Pane;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class HighScoreController {
    HighScore highScore;
    HighScoreGui highScoreGui;

    /**
     * 
     * @param mainContent
     */
    HighScoreController(Pane mainContent) {
        this.highScore = new HighScore();
        this.highScoreGui = new HighScoreGui(mainContent);
    }

    /**
     * 
     * @return
     */
    public HighScoreGui getGui() {
        return this.highScoreGui;
    }

    /**
     * 
     * @return
     */
    public HighScore getHighScore() {
        return this.highScore;
    }

    /**
     * add points
     * @param points
     */
    public void addPoints(int points) {
        this.getGui().addPoints(points);
    }

    /**
     * reset Points
     */
    public void resetPoints() {
        this.getGui().resetPoints();
    }
}

package application;

import java.util.ArrayList;
import java.util.Collections;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class HighScoreGui {

    private int points;
    private int round = 1;
    private Label scoreLabel;
    private MainMenu mainMenu;
    private Pane mainContent;
    private ArrayList<HighScore> highScoreList = new ArrayList<HighScore>();

    /**
     * 
     * @param mainContent
     */
    public HighScoreGui(Pane mainContent) {
        this.mainContent = mainContent;
        this.points = 0;
        createScoreLabel();
    }

    /**
     * 
     * @param mainMenu
     */
    public void setMainMenu(MainMenu mainMenu) {
        this.mainMenu = mainMenu;
    }

    /**
     * 
     * @param mainContent
     */
    public void setMainContent(Pane mainContent) {
        this.mainContent = mainContent;
    }

    /**
     * add new points to old points
     * 
     * @param points
     */
    public void addPoints(int points) {
        this.points = this.points + points;
        String value = new Integer(this.getPoints()).toString();
        this.scoreLabel.setText(value);
    }

    /**
     * reset points in gui
     */
    public void resetPoints() {
        this.points = 0;
        String value = new Integer(this.getPoints()).toString();
        this.scoreLabel.setText(value);
    }

    /**
     * 
     * @return
     */
    public int getPoints() {
        return this.points;
    }

    /**
     * 
     * @return
     */
    public Label getScoreLabel() {
        return this.scoreLabel;
    }

    /**
     * 
     * @return
     */
    public Pane getMainContent() {
        return this.mainContent;
    }

    /**
     * create ScoreLabel
     */
    private void createScoreLabel() {
        this.scoreLabel = new Label();
        String value = new Integer(this.getPoints()).toString();
        this.scoreLabel.setText(value);
        this.scoreLabel.getStyleClass().add("scoreLabel");
        this.scoreLabel.setLayoutX(20);
        this.scoreLabel.setLayoutY(20);
    }

    /**
     * 
     * @param showPointsAndNameFieldInput
     * @return
     */
    public VBox getHighScorePage(boolean showPointsAndNameFieldInput) {
        VBox highScorePage = new VBox();
        VBox backBtnWrapper = this.getBackBtnWrapper();
        VBox highScorePageHeader = this.getHighScorePageHeader();

        if (showPointsAndNameFieldInput) {
            Text header = (Text) highScorePageHeader.getChildren().get(0);
            header.setText("Ende");

            Text highScorePageSubHeader = this.getHighScorePageSubHeader();
            highScorePageHeader.getChildren().add(highScorePageSubHeader);

            Label labelForName = new Label("Du heißt wie?");
            labelForName.getStyleClass().add("inputLabel");
            TextField textField = new TextField();
            textField.setMaxWidth(300);
            textField.getStyleClass().add("textField");

            VBox labelInputWrapper = new VBox();
            labelInputWrapper.getStyleClass().add("topCenter");
            labelInputWrapper.getChildren().addAll(labelForName, textField);

            Button buttonForInsertHighScore = new Button("Namen eintragen und erneut spielen");
            buttonForInsertHighScore.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent e) {
                    // mainContent = mainMenu.getMainContent(mainMenu.getMainMenu());
                    // fileHandler.writeFile();
                    setHighScore(textField.getText(), points);

                    resetPoints();

                    if (!highScoreList.isEmpty()) {
                        mainMenu.showHighScorePage();
                    } else {
                        toMainMenu();

                    }
                }
            });

            VBox inputWrapper = new VBox();
            inputWrapper.getStyleClass().add("topCenter");
            inputWrapper.getStyleClass().add("inputWrapper");
            inputWrapper.setSpacing(20);

            inputWrapper.getChildren().addAll(labelInputWrapper, buttonForInsertHighScore);

            highScorePageHeader.getChildren().add(inputWrapper);

            highScorePage.getChildren().addAll(backBtnWrapper, highScorePageHeader);
        } else {
            VBox highScoreList = this.getHighScoreList();

            highScorePage.getStyleClass().add("vbox");
            highScorePage.getStyleClass().add("highScorePage");
            highScorePage.setSpacing(50);

            highScorePage.getChildren().addAll(backBtnWrapper, highScorePageHeader, highScoreList);
        }

        return highScorePage;
    }

    /**
     * 
     * @return
     */
    private VBox getHighScorePageHeader() {
        VBox highScorePageHeader = new VBox();

        Text highScoreHeader = new Text("HighScore");
        highScoreHeader.getStyleClass().add("highscoreHeader");

        highScorePageHeader.getStyleClass().add("topCenter");
        highScorePageHeader.getChildren().add(highScoreHeader);

        return highScorePageHeader;
    }

    /**
     * 
     * @return
     */
    private Text getHighScorePageSubHeader() {
        Text highScorePageSubHeader = new Text("Dein Punktestand betägt: " + this.getPoints() + " Punkte");
        highScorePageSubHeader.getStyleClass().add("highScorePageSubHeader");

        return highScorePageSubHeader;
    }

    /**
     * 
     * @return
     */
    private VBox getHighScoreList() {
        VBox highScoreListWrapper = new VBox();

        if (!this.highScoreList.isEmpty()) {
            for (HighScore highScore : this.highScoreList) {
                Text highScorePerson = new Text(
                        highScore.getName() + " // " + "Round " + Integer.toString(highScore.getRound()) + " // "
                                + Integer.toString(highScore.getPoints()) + " Punkte");

                highScoreListWrapper.getChildren().add(highScorePerson);
            }
        } else {
            highScoreListWrapper.getChildren().add(new Text("Noch keinen Eintrag"));
        }

        highScoreListWrapper.getStyleClass().add("topCenter");
        highScoreListWrapper.getStyleClass().add("highScoreList");

        return highScoreListWrapper;

    }

    /**
     * 
     * @param name
     * @param round
     * @param points
     */
    private void addPersonToFile(String name, int round, int points) {
        highScoreList.add(new HighScore(name, round, points));
    }

    /**
     * 
     * @param highScoreList
     */
    private void sortAndCheckPerson(ArrayList<HighScore> highScoreList) {
        int max = 10;

        Collections.sort(highScoreList, Collections.reverseOrder());

        if (highScoreList.size() > max) {
            highScoreList.remove(max);
        }
    }

    /**
     * 
     * @param name
     * @param Points
     */
    public void setHighScore(String name, int Points) {
        if (name != null && !name.equals("")) {
            addPersonToFile(name, round, points);
            sortAndCheckPerson(highScoreList);
        }

        this.round++;
    }

    /**
     * go to main Menu
     */
    private void toMainMenu() {
        this.mainContent.getChildren().remove(0);
        this.mainContent.getChildren().add(this.mainMenu.getMainMenu());
    }

    /**
     * 
     * @return
     */
    private VBox getBackBtnWrapper() {
        Button btn_back = new Button("\uf0e2");
        btn_back.getStyleClass().add("button--back");
        btn_back.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                resetPoints();
                toMainMenu();
            }
        });

        VBox backBtnWrapper = new VBox();
        backBtnWrapper.getStyleClass().add("backBtnWrapper");
        backBtnWrapper.getStyleClass().add("vbox");
        backBtnWrapper.getChildren().addAll(btn_back);

        return backBtnWrapper;
    }

}

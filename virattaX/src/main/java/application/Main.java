package application;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class Main extends Application {

    public static Main instance = new Main();
    Stage ps;

    @Override
    public void start(Stage primaryStage) {
        ps = primaryStage;

        try {

            MainController mainController = new MainController();
            Scene scene = mainController.getScene();

            primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent t) {
                    Platform.exit();
                    System.exit(1);
                }
            });

            primaryStage.setTitle("PraxisProjekt 3. Semester");
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @return
     */
    public JFXPanel jFX() {
        JFXPanel panel = new JFXPanel();
        MainController mainController = new MainController();
        Scene scene = mainController.getScene();
        panel.setScene(scene);
        return panel;
    }

    /**
     * 
     * @param args
     */
    public static void main(String[] args) {
        launch(args);
    }
}

package application;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class MainController {

    public Scene scene;
    public Sound sound;
    public Pane mainContent;
    public Footer footer;
    public VBox root;
    public HighScoreController highScoreController;
    public GameController gameController;
    public MainMenu mainMenu;

    /**
     * init main controller
     */
    MainController() {

        root = new VBox();

        this.mainContent = this.createMainContent();

        this.sound = new Sound();
        this.highScoreController = new HighScoreController(this.mainContent);

        this.gameController = new GameController(this.highScoreController);

        this.mainMenu = new MainMenu(this.mainContent, this.gameController, this.highScoreController);

        this.highScoreController.getGui().setMainMenu(this.mainMenu);

        this.mainContent.getChildren().add(this.mainMenu.getMainMenu());

        this.footer = new Footer(this.mainContent, this.sound, this.highScoreController);

        root.getChildren().addAll(mainContent, footer.getFooterPane());

        this.scene = new Scene(root, Config.windowWidth, Config.windowHeight);
    }

    /**
     * 
     * @return
     */
    private Pane createMainContent() {

        Pane mainContentPane = new Pane();
        mainContentPane.setMinWidth(Config.gameWidth);
        mainContentPane.setMaxWidth(Config.gameWidth);
        mainContentPane.setMinHeight(Config.gameHeight);
        mainContentPane.setMaxHeight(Config.gameHeight);
        mainContentPane.getStyleClass().add("mainContent");

        return mainContentPane;

    }

    /**
     * set fonts and style sheets and return main scene
     * @return
     */
    public Scene getScene() {
        // String path = System.getProperty("user.dir");
        String path = getClass().getClassLoader().getResource("fonts/").toExternalForm().toString();
        String latoRegularURI = path + "Lato-Regular.ttf";
        String latoBlackURI = path + "Lato-Black.ttf";
        String glaserStedURI = path + "GlaserSted.ttf";
        String erasdemiURI = path + "ERASDEMI.ttf";
        String faRegular400URI = path + "fa-regular-400.woff";

        path = getClass().getClassLoader().getResource("css/").toExternalForm().toString();
        String cssURI = path + "application.css";

        this.scene.getStylesheets().clear();

        this.scene.getStylesheets().add(latoRegularURI);
        this.scene.getStylesheets().add(latoBlackURI);
        this.scene.getStylesheets().add(glaserStedURI);
        this.scene.getStylesheets().add(erasdemiURI);
        this.scene.getStylesheets().add(faRegular400URI);
        this.scene.getStylesheets().add(cssURI);

        return scene;
    }

}

package application;

import de.hsh.projekt.common.GameEventInitiater;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class MainMenu {

    public Pane mainContent;
    GameController gameController;
    HighScoreController highScoreController;

    /**
     * 
     * @param mainContent
     * @param gameController
     * @param highScoreController
     */
    MainMenu(Pane mainContent, GameController gameController, HighScoreController highScoreController) {
        this.mainContent = mainContent;
        this.gameController = gameController;
        this.highScoreController = highScoreController;
    }

    /**
     * start game
     */
    private void gameStart() {

        this.mainContent.getChildren().remove(0);
        this.mainContent.getChildren().add(this.gameController.getGame().getGameView());

        this.gameController.start();
    }

    /**
     * go to highscore page
     */
    public void showHighScorePage() {
        this.mainContent.getChildren().remove(0);
        this.mainContent.getChildren().add(this.highScoreController.getGui().getHighScorePage(false));
    }

    /**
     * create main menu
     * @return
     */
    public VBox getMainMenu() {
        Button btn_close;
        Button btn_highscore;
        Text logoName;
        Text logoSlogan;
        HBox logoSloganWrapper;
        HBox logoWrapper;
        VBox mainMenu;
        VBox mainMenuVbox;
        VBox mainCloseBtnWrapper;

        btn_close = new Button("X");
        btn_close.getStyleClass().add("button--close");
        btn_close.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                gameController.stop();
                GameEventInitiater.exitGame();
            }
        });

        mainCloseBtnWrapper = new VBox();
        mainCloseBtnWrapper.getStyleClass().add("mainCloseBtnWrapper");
        mainCloseBtnWrapper.getChildren().addAll(btn_close);

        logoName = new Text("VirAttaX");
        logoName.getStyleClass().add("logo");

        logoSlogan = new Text("Die Virenjäger");
        logoSlogan.getStyleClass().add("logoSlogan");

        logoSloganWrapper = new HBox();
        logoSloganWrapper.getStyleClass().add("logoSloganWrapper");
        logoSloganWrapper.getChildren().addAll(logoSlogan);

        logoWrapper = new HBox();
        logoWrapper.getStyleClass().add("hbox");
        logoWrapper.getStyleClass().add("logoWrapper");
        logoWrapper.getChildren().addAll(logoName, logoSloganWrapper);

        mainMenu = new VBox();
        mainMenu.getStyleClass().add("vbox");
        mainMenu.getStyleClass().add("mainWrapper");
        mainMenu.setSpacing(50);

        mainMenuVbox = new VBox();
        mainMenuVbox.getStyleClass().add("vbox");
        mainMenuVbox.getStyleClass().add("mainMenuVbox");
        mainMenuVbox.setSpacing(50);

        Button btn_gameStart = new Button("Spiel Starten");
        btn_gameStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                gameStart();
            }
        });

        btn_highscore = new Button("Highscore");
        btn_highscore.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                // Highscore
                showHighScorePage();
            }
        });

        mainMenuVbox.getChildren().addAll(btn_gameStart, btn_highscore);

        mainMenu.getChildren().addAll(mainCloseBtnWrapper, logoWrapper, mainMenuVbox);

        return mainMenu;
    }
}

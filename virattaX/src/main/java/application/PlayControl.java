package application;

import javafx.application.Platform;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class PlayControl {

    private Game game;
    private EnemyController enemyController;
    private WaveControl waveControl;

    /**
     * 
     * @param game
     * @param enemyController
     */
    PlayControl(Game game, EnemyController enemyController) {
        this.setGame(game);
        this.setEnemyController(enemyController);
        this.setWaveControl(new WaveControl(this.game, this.enemyController.getEnemyFactory()));
    }

    /**
     * enemies action
     * @param passedTime
     */
    public void action(double passedTime) {
        this.actionForLiveEnemies(passedTime);
        this.actionForDeadEnemies();
        if (this.enemyController.getLiveEnemies().isEmpty()) {
            this.getWaveControl().checkAndCreateWave(this.game.getCurrentWave(), this.game.getEndWave());
        }
    }

    /**
     * go to highscore page
     */
    public void showHighScorePage() {

        this.game.getHighScoreController().getGui().getMainContent().getChildren().remove(0);
        this.game.getHighScoreController().getGui().getMainContent().getChildren()
                .add(this.game.getHighScoreController().getGui().getHighScorePage(true));
    }

    /**
     * init game
     */
    public void initGame() {
        this.game.setGameActiveState(true);
    }

    /**
     * stop game
     */
    public void stopGame() {
        this.game.setGameActiveState(false);
    }

    /**
     * clear the view
     */
    public void clearView() {
        this.game.getGameView().getChildren().clear();
    }

    /**
     * 
     * @param passedTime
     */
    private void actionForLiveEnemies(double passedTime) {
        for (AbstractEnemy enemy : this.enemyController.getLiveEnemies()) {
            enemy.move(passedTime);
            if (this.enemyCanDraw(enemy)) {
                this.draw(enemy);
            } else {
                this.enemyController.getToDeleteEnemies().add(enemy);
            }
        }
    }

    /**
     * clear dead enemies
     */
    private void actionForDeadEnemies() {
        for (AbstractEnemy enemy : this.enemyController.getToDeleteEnemies()) {
            if (enemy.getClicked() == true) {
                Platform.runLater(() -> this.game.getHighScoreController().addPoints(enemy.getPoints()));
            }
            this.enemyController.getLiveEnemies().remove(enemy);
        }
        this.enemyController.getToDeleteEnemies().clear();
    }

    /**
     * 
     * @param enemy
     */
    private void draw(AbstractEnemy enemy) {
        Platform.runLater(() -> {
            this.game.getGameView().getChildren().add(enemy.getRectangle());
        });
    }

    /**
     * 
     * @return
     */
    public boolean gameIfActiv() {
        if (this.game.getGameActiveState()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param enemy
     * @return
     */
    private boolean enemyCanDraw(AbstractEnemy enemy) {
        // Wenn Enemy aus dem Gamebereich ist ODER Enemy hat kein leben mehr
        // Dann
        // Kann es nicht gezeichnet werden
        if (ifEnemyOutOfBottom(enemy) || !enemy.getLife()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 
     * @param enemy
     * @return
     */
    private boolean ifEnemyOutOfBottom(AbstractEnemy enemy) {
        if (enemy.getyCord() >= (Config.gameHeight - enemy.getHeight())) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @param game
     */
    private void setGame(Game game) {
        this.game = game;
    }

    /**
     * 
     * @param enemyController
     */
    private void setEnemyController(EnemyController enemyController) {
        this.enemyController = enemyController;
    }

    /**
     * 
     * @param waveControl
     */
    private void setWaveControl(WaveControl waveControl) {
        this.waveControl = waveControl;
    }

    /**
     * 
     * @return
     */
    private WaveControl getWaveControl() {
        return this.waveControl;
    }

}

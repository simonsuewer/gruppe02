package application;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class Sound {

    private MediaPlayer mediaplayer;
    private Media musicFile;

    /**
     * init Sound
     */
    Sound() {
        this.musicFile = new Media(
                getClass().getClassLoader().getResource("sound/Good-Morning-Doctor-Weird.mp3").toExternalForm());
        this.mediaplayer = new MediaPlayer(musicFile);
        this.mediaplayer.setAutoPlay(true);
        this.mediaplayer.setVolume(1);
        this.mediaplayer.setCycleCount(MediaPlayer.INDEFINITE);
    }

    /**
     * play sound
     */
    public void play() {
        mediaplayer.play();
    }

    /**
     * pause sound
     */
    public void pause() {
        mediaplayer.pause();
    }

    /**
     * stop sound
     */
    public void stop() {
        mediaplayer.stop();
    }
}

package application;

/**
 * 
 * @author Patrick Schaper
 *
 */
public class WaveControl {

    private Game game;
    private EnemyFactory enemyFactory;

    /**
     * 
     * @param game
     * @param enemyFactory
     */
    WaveControl(Game game, EnemyFactory enemyFactory) {
        this.setGame(game);
        this.setEnemyFactory(enemyFactory);
    }

    /**
     * 
     * @param currentWave
     * @param maxWave
     * @return
     */
    public boolean checkAndCreateWave(int currentWave, int maxWave) {

        if (currentWave <= maxWave) {
            switch (currentWave) {
            case 1:
                this.enemyFactory.createEnemies(1, 30);
                break;
            case 2:
                this.enemyFactory.createEnemies(2, 60);
                break;
            case 3:
                this.enemyFactory.createEnemies(1, 30);
                this.enemyFactory.createEnemies(2, 40);
                break;
            }

            this.game.setCurrentWave(this.game.getCurrentWave() + 1);

            return true;
        } else {
            this.game.setGameActiveState(false);
            return false;
        }
    }

    /**
     * 
     * @param game
     */
    private void setGame(Game game) {
        this.game = game;
    }

    /**
     * 
     * @param enemyFactory
     */
    private void setEnemyFactory(EnemyFactory enemyFactory) {
        this.enemyFactory = enemyFactory;
    }

}

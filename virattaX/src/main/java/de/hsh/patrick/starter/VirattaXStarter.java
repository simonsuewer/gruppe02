package de.hsh.patrick.starter;

import javax.swing.JMenuBar;
import javax.swing.JPanel;

import application.Main;
import de.hsh.projekt.common.GameEventInitiater;
import de.hsh.projekt.common.KeyInput;
import de.hsh.projekt.common.MouseInput;
import javafx.embed.swing.JFXPanel;

public class VirattaXStarter extends de.hsh.projekt.common.Game {

    @Override
    public int start() {

        return 0;
    }

    @Override
    public int backToMain() {
        // gameloop stoppen!!!
        GameEventInitiater.exitGame();
        return 0;
    }

    @Override
    public KeyInput getKeyInput() {
        return new KeyInput();
    }

    @Override
    public MouseInput getMouseInput() {
        return new MouseInput();
    }

    @Override
    public String getName() {
        return "VirAttaX - Die Virenjäger";
    }

    @Override
    public String getDescription() {
        return "<html>Die Welt steht vor einer großen Bedrohung.<br />Gefährliche Viren kommen und wollen das ganze "
                + "Internet wie wir es kennen, angreifen und zerstören. Wir von VirAttaX brauchen Dich!<br />"
                + "<br />" + "Du bist unsere letzte Hoffnung und musst sie aufhalten.<br />"
                + "Beseitige soviele Viren wie du nur kannst "
                + "und sammle Punkte, um dich mit anderen Virenjägern zu messen. ;-) </html>";
    }

    @Override
    public JPanel getPanel() {
        return null;
    }

    @Override
    public JFXPanel getJFXPanel() {
        Main window = Main.instance;

        return window.jFX();

    }

    @Override
    public JMenuBar getJMenuBar() {
        return null;
    }
}

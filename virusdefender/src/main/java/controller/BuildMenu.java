package controller;

import data.Building;
import data.Data;
import data.ImageLibary;
import data.Update;
import data.UpdateButton;
import helper.Config;
import helper.StaticStrings;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BuildMenu {

    private ArrayList<UpdateButton>  updateButtons   = new ArrayList<>();
    private ArrayList<Update>        possibleUpdates = new ArrayList<>();
    private HashMap<Integer, Update> updates         = new HashMap<>();
    private double                   y               = 100;
    private double                   w               = 400;
    private double                   x               = (Config.getCANVAS_WIDTH() / 2.0) - (w / 2);
    private double                   h               = 550;
    private Building                 building;
    private ImageLibary              imageLibary;
    private Data                     data;

    BuildMenu(ImageLibary imageLibary, Data data) {
        this.imageLibary = imageLibary;
        Color color = new Color(0.333, 0.333, 0.333, 1);
        this.data = data;
    }

    /**
     * Renders the Building Menu with all Possible Updates for the Clicked Building
     *
     * @param gc
     */
    void render(GraphicsContext gc) {
        gc.save();
        gc.setFill(Color.DARKRED);
        gc.fillRect(x, y, w, h);
        gc.drawImage(building.getCannon().getImage(), x + 30, y + 30);
        gc.setFill(Color.WHITE);
        gc.fillText(building.getCannon().getName() + StaticStrings.BUILD_HEADER + building.getCannon().getLevel(), x + 100, y + 47);
        gc.fillText(StaticStrings.DMG + building.getCannon().getMinDmg() + StaticStrings.DMG_BETWEEN + building.getCannon().getMaxDmg(),
                    x + 100,
                    y + 72);

        gc.setFill(Color.LIGHTGRAY);
        // Trennlinie
        gc.fillRect(x + 10, y + 110, w - 20, 1);
        int yOffset = 120;
        for (Update u : possibleUpdates) {
            gc.save();

            gc.fillRect(x + 10, y + yOffset - 10, w - 20, 1);
            double offset = 25 - imageLibary.getImage(u.getImageName()).getWidth() / 2;
            gc.drawImage(imageLibary.getImage(u.getImageName()), x + 10 + offset, y + yOffset + offset);
            gc.fillText(u.getName(), x + 100, y + yOffset + 15);
            gc.fillText(StaticStrings.PRICE + u.getCost(), x + 100, y + yOffset + 41);
            gc.fillText(StaticStrings.DMG + u.getMinDamage() + StaticStrings.DMG_BETWEEN + u.getMaxDamage(),
                        x + 100,
                        y + yOffset + 61);
            if (u.getCost() <= data.getMoney()) {
                gc.setFill(Color.GREEN);
            } else {
                gc.setFill(Color.GREY);
            }
            int          index  = possibleUpdates.indexOf(u);
            UpdateButton button = updateButtons.get(index);
            gc.fillRect(x + 300, y + yOffset, button.getW(), button.getH());
            updateButtons.get(index).setX(x + 300);
            updateButtons.get(index).setY(y + yOffset);
            gc.setFill(Color.WHITE);
            gc.fillText(StaticStrings.BUY, x + 322, y + yOffset + 20);

            yOffset += 90;
            gc.restore();
        }
        gc.restore();
    }

    /**
     * checks the UpdateList with current Cannon to find next possible updates
     */
    void checkUpdates() {
        possibleUpdates.clear();
        updateButtons.clear();
        for (Object o : updates.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            Update    up   = (Update) pair.getValue();
            if (up.getId() == building.getCannon().getUpgradeID()) {
                for (int id : up.getNextUpgrades()) {
                    Update update = updates.get(id);
                    possibleUpdates.add(update);
                    //TODO add Buttons to Updatebuttons Array
                    updateButtons.add(new UpdateButton(0, 0, 80, 30, update));
                }
            }
        }
    }

    /**
     * checks Clicks on the Buldings to open the UpdateWindow with the correct Building
     *
     * @param mouseClickedX
     * @param mouseClickedY
     * @param buildings
     */
    void checkButtonClick(int mouseClickedX, int mouseClickedY, ArrayList<Building> buildings) {
        for (UpdateButton b : updateButtons) {
            if (mouseClickedX > b.getX() && mouseClickedX < (b.getX() + b.getW())) {
                if (mouseClickedY > b.getY() && mouseClickedY < (b.getY() + b.getH())) {
                    if (b.getUpdate().getCost() <= data.getMoney()) {
                        doUpdate(b);
                        Config.startFirstWave = true;
                    }
                }
            }
        }
    }

    /**
     * Updates the Building data with the new Update
     *
     * @param b
     */
    private void doUpdate(UpdateButton b) {
        building.getCannon().setImage(imageLibary.getImage(b.getUpdate().getImageName()));
        building.getCannon().setAttackSpeed(b.getUpdate().getAttackSpeed());
        building.getCannon().setLevel(b.getUpdate().getLevel());
        building.getCannon().setMinDmg(b.getUpdate().getMinDamage());
        building.getCannon().setMaxDmg(b.getUpdate().getMaxDamage());
        building.getCannon().setPrice(b.getUpdate().getCost());
        building.getCannon().setName(b.getUpdate().getName());
        building.getCannon().setUpgradeID(b.getUpdate().getId());
        data.setMoney(data.getMoney() - b.getUpdate().getCost());
    }

    public double getY() {
        return y;
    }

    double getW() {
        return w;
    }

    public double getX() {
        return x;
    }

    double getH() {
        return h;
    }

    void setUpdates(HashMap<Integer, Update> updates) {
        this.updates = updates;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setW(double w) {
        this.w = w;
    }

    public void setX(double x) {
        this.x = x;
    }

    void setBuilding(Building building) {
        this.building = building;
    }
}

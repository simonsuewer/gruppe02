package controller;

import data.Building;
import data.Cannon;
import data.Enemy;
import data.ImageLibary;
import helper.Config;
import helper.Distance;
import helper.Rotation;

import java.util.ArrayList;

class BuildingController {

    private ArrayList<Building> buildings = new ArrayList<>();
    private ImageLibary         imageLibary;
    private Cannon              c;
    private ShootController     shootController;

    /**
     * Genearte Buildings and Cannons
     *
     * @param imageLibary Gets the ImageLibary to load the ImageFiles
     */
    BuildingController(ImageLibary imageLibary) {
        this.imageLibary = imageLibary;
        c = new Cannon(0,
                       0,
                       0,
                       100,
                       0,
                       0,
                       imageLibary.getImage("Base"),
                       null,
                       "Base");

        double space = Config.getCANVAS_HEIGHT() - 8 * 50;
        space = space / 8;
        buildings.add(new Building(new Cannon(c), 50, 1 * space + 0, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 2 * space + 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 3 * space + 2 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 4 * space + 3 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 5 * space + 4 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 6 * space + 5 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 7 * space + 6 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 8 * space + 7 * 50, 50, 50));
    }

    /**
     * Updates alls Buildings.
     * Finds Target
     * Rotates to Target
     * shoots
     *
     * @param dt     passed Time to get Timebast Animation
     * @param enemys ArrayList with enemys to get Enemies
     */
    void tick(double dt, ArrayList<Enemy> enemys) {

        for (Building b : buildings) {
            Enemy enemy = Distance.nearestEnemy(enemys, b.getX(), b.getY());
            if (enemy != null) {
                b.getCannon().setTragetX(enemy.getX());
                b.getCannon().setTragetY(enemy.getY());
                b.getCannon().setEnemy(enemy);
            }
            if (b.getCannon().getName().equals("Base")) {
                continue;
            }
            double x      = b.getX();
            double y      = b.getY();
            double rotate = Rotation.getRotation(b.getCannon().getTragetX() - x, b.getCannon().getTragetY() - y);
            b.getCannon().setRoation(Math.toDegrees(rotate));

            if (!enemys.isEmpty()) {
                double timeBetweenShooting = 1 / b.getCannon().getAttackSpeed();
                double newDeltaShoot       = b.getCannon().getDeltashoot() + dt;
                if (newDeltaShoot > timeBetweenShooting) {
                    shootController.newshoot(b.getX(), b.getY(), b.getCannon());
                    b.getCannon().setDeltashoot(0);
                } else {
                    b.getCannon().setDeltashoot(newDeltaShoot);
                }
            }
        }
    }

    /**
     * Resets Building to basic
     */
    public void reset() {
        buildings.clear();
        c = new Cannon(0,
                       0,
                       0,
                       100,
                       0,
                       0,
                       imageLibary.getImage("Base"),
                       null,
                       "Base");

        double space = Config.getCANVAS_HEIGHT() - 8 * 50;
        space = space / 8;
        buildings.add(new Building(new Cannon(c), 50, 1 * space + 0, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 2 * space + 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 3 * space + 2 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 4 * space + 3 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 5 * space + 4 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 6 * space + 5 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 7 * space + 6 * 50, 50, 50));
        buildings.add(new Building(new Cannon(c), 50, 8 * space + 7 * 50, 50, 50));
    }

    ArrayList<Building> getBuildings() {
        return buildings;
    }

    void setShootController(ShootController shootController) {
        this.shootController = shootController;
    }
}

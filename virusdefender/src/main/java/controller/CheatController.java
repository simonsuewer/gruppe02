package controller;

import data.Building;
import data.Data;
import data.ImageLibary;
import data.Update;
import data.Updates;

import java.util.ArrayList;

public class CheatController {

    private Data                data;
    private String              input = "";
    private ArrayList<Building> buildings;
    private Updates             updates;
    private ImageLibary         imageLibary;

    void listen(String s) {
        this.input = this.input + s;

        System.out.println(input);
        check();
    }

    private void check() {
        if ("MONEY".contains(input)) {
            if (input.equals("MONEY")) {
                data.setMoney(data.getMoney() + 100000);
                resetInput();
            }
        } else if ("LIFE".contains(input)) {
            if (input.equals("LIFE")) {
                data.setLife(data.getLife() + 10);
                resetInput();
            }
        } else if ("WAVEX".contains(input)) {
            if (input.equals("WAVEX")) {
                data.setWave(data.getWave() + 10);
                resetInput();
            }
        } else if ("MAXCANNONS".contains(input)) {
            if (input.equals("MAXCANNONS")) {
                for (Building b : buildings) {
                    int nextUpdate = b.getCannon().getUpgradeID();
                    if (nextUpdate != 0) {
                        nextUpdate = nextUpdate / 1000;
                        nextUpdate = (nextUpdate * 1000) + 35;
                        Update update = updates.getUpdates().get(nextUpdate);
                        setUpdate(b, update);
                    }
                }
                resetInput();
            }
        } else if ("LOSE".contains(input)) {
            if (input.equals("LOSE")) {
                data.setLife(0);
                resetInput();
            }
        } else {
            resetInput();
        }
    }

    private void setUpdate(Building b, Update update) {
        b.getCannon().setAttackSpeed(update.getAttackSpeed());
        b.getCannon().setLevel(update.getLevel());
        b.getCannon().setMaxDmg(update.getMaxDamage());
        b.getCannon().setMinDmg(update.getMinDamage());
        b.getCannon().setImage(imageLibary.getImage(update.getImageName()));
        b.getCannon().setUpgradeID(update.getId());
        b.getCannon().setName(update.getName());
    }

    private void resetInput() {
        input = "";
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    void setBuildings(ArrayList<Building> buildings) {
        this.buildings = buildings;
    }

    void setUpdates(Updates updates) {
        this.updates = updates;
    }

    void setImageLibary(ImageLibary imageLibary) {
        this.imageLibary = imageLibary;
    }
}

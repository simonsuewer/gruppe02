package controller;

import data.AudioLibary;
import data.Building;
import data.Data;
import data.Enemy;
import data.FileLoader;
import data.Highscore;
import data.ImageLibary;
import data.Score;
import data.Shoot;
import data.Updates;
import engine.Audio;
import engine.VirusDefender;
import helper.Clicked;
import helper.Colision;
import helper.Config;
import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class Controller {

    private EnemyController    ec;
    private BuildingController bc;
    private ShootController    sc;
    private WaveControl        wc;
    private Updates            updates;
    private BuildMenu          buildMenu;
    private AudioLibary        audioLibary;
    private ImageLibary        imageLibary;
    private CheatController    cheatController;
    private boolean            renderBuildMenu = false;
    private boolean            firststart      = true;
    private Data               data            = new Data(150, 0, 10);
    private Colision           colision        = Colision.colisionInstance;
    private Audio              audioplayer     = Audio.instance;
    private int                bgcounter       = 0;
    private int                mouseY          = 0;
    private int                mouseX          = 0;
    private Highscore          hs              = Highscore.instance;
    private boolean            isEnterd        = false;

    public Controller() {
        cheatController = new CheatController();
        imageLibary = new ImageLibary();
        audioLibary = new AudioLibary();
        updates = new Updates();
        buildMenu = new BuildMenu(imageLibary, data);
        buildMenu.setUpdates(updates.getUpdates());
        FileLoader fl = new FileLoader(imageLibary, audioLibary, updates);
        fl.loadImageAudio();
        fl.loadUpgrades();
        ec = new EnemyController(imageLibary);
        bc = new BuildingController(imageLibary);
        sc = new ShootController(imageLibary, audioLibary);
        wc = new WaveControl(imageLibary);
        wc.setData(data);
        bc.setShootController(sc);
        colision.setEnemysOrigin(ec.getEnemys());
        colision.setShootsOrigin(sc.getShoots());
        //TODO in Methoden auslagern
        cheatInjetc();
        audioplayer.playBGSound(audioLibary.getSound("cyberbg"));
    }

    /**
     * gives all necessary Data to CheatController
     */
    private void cheatInjetc() {
        cheatController.setData(data);
        cheatController.setBuildings(bc.getBuildings());
        cheatController.setUpdates(updates);
        cheatController.setImageLibary(imageLibary);

    }

    public void tick(double dt) {

        if (data.getLife() <= 0) {
            return;
        }
        if (Config.startFirstWave) {
            if (ec.getEnemys().size() == 0) {
                wc.tick(ec.getNextWaveEnemy());

            }
        }
        ec.tick(dt, data);
        bc.tick(dt, ec.getEnemys());
        sc.tick(dt);
        colision.checkHit(ec.getEnemys(), sc.getShoots(), data, Config.getCANVAS_WIDTH());

    }

    public void render(GraphicsContext gc) {

        Platform.runLater(() -> {

            if (data.getLife() <= 0 || firststart) {
                gc.save();
                gc.setFill(Color.rgb(0, 0, 0, 0.3));
                gc.fillRect((Config.getCANVAS_WIDTH() / 2.0) - 250,
                            (Config.getCANVAS_HEIGHT() / 2.0) - 60,
                            500,
                            120);
                gc.setFont(new Font("Arial", 40));
                gc.drawImage(imageLibary.getImage("loose"), 0, 0);
                gc.setFill(Color.LIGHTGRAY);
                gc.fillRect(299, 719, 345, 42);
                gc.fillRect(679, 719, 345, 42);
                gc.setFill(Color.BLACK);
                if (!firststart) {
                    gc.fillText("Highscore\n" + data.getPoints(), 680, 117);
                }
                printRaning(gc);
                gc.setFont(new Font("Arial", 18));
                gc.fillText("Check Menü", 410, 747);

                if (firststart) {
                    gc.fillText("Spiel starten", 780, 747);
                } else {
                    gc.fillText("Noch eine Runde", 780, 747);
                }
                if (Clicked.clicked(679, 679 + 345, 719, 761, mouseX, mouseY)) {
                    gc.setStroke(Color.BLUE);
                    gc.strokeRect(679, 719, 345, 42);
                } else if (Clicked.clicked(299, 299 + 345, 719, 761, mouseX, mouseY)) {
                    gc.setStroke(Color.BLUE);
                    gc.strokeRect(299, 719, 345, 42);
                }

                gc.restore();
            } else {

                if (bgcounter == 116) {
                    bgcounter = 0;
                }
                gc.drawImage(imageLibary.getImage("bg" + bgcounter), 0, 0, Config.getCANVAS_WIDTH(), Config.getCANVAS_HEIGHT());
                bgcounter++;
                //gc.drawImage(imageLibary.getImage("wall"), 125, 0);
                for (Building b : bc.getBuildings()) {
                    double rotation = b.getCannon().getRoation();
                    double x        = b.getX();
                    double y        = b.getY();
                    Image  i        = b.getCannon().getImage();
                    renderMethode(gc, rotation, x, y, i);
                }
                for (Enemy enemy : ec.getEnemys()) {
                    double rotation = enemy.getRotation();
                    double x        = enemy.getX();
                    double y        = enemy.getY();
                    Image  i        = enemy.getImage();
                    renderMethode(gc, rotation, x, y, i);
                    renderLifeBar(gc, x, y, enemy.getLife(), enemy.getMaxLife(), enemy.getImage().getWidth());
                }
                // Render Shoots
                for (Shoot shoot : sc.getShoots()) {
                    double rotation = shoot.getRotation();
                    double x        = shoot.getX();
                    double y        = shoot.getY();
                    Image  i        = shoot.getImage();
                    renderMethode(gc, rotation, x, y, i);
                }
                if (renderBuildMenu) {
                    buildMenu.render(gc);
                }
                gc.setFill(Color.rgb(0, 0, 0, 0.3));
                gc.fillRect(Config.getCANVAS_WIDTH() - 500, 0, Config.getCANVAS_WIDTH(), 45);
                gc.setFill(Color.WHITE);

                gc.fillText(data.getPointString(), Config.getCANVAS_WIDTH() - 480, 28);
                gc.fillText(data.getWaveString(), Config.getCANVAS_WIDTH() - 320, 28);
                gc.fillText(data.getMoneyString(), Config.getCANVAS_WIDTH() - 200, 28);
                gc.fillText(data.getLifeString(), Config.getCANVAS_WIDTH() - 80, 28);

            }
        });
    }

    private void printRaning(GraphicsContext gc) {
        gc.setFont(new Font("Arial", 18));
        int counter = 0;

        gc.fillText("Rang" + ".\tName", 340, 100);
        gc.fillText("Punkte", 500, 100);

        gc.setFill(Color.GREY);
        gc.strokeRect(335, 110, 230, 1);
        gc.setFill(Color.BLACK);
        for (Score sc : hs.getRanking()) {
            if (counter >= 10) {
                continue;
            }
            int ofset = counter * 22;
            gc.fillText((counter + 1) + ".\t" + sc.getName(), 340, 130 + ofset);
            gc.fillText("" + sc.getScore(), 500, 130 + ofset);
            counter++;
        }
        if (data.getLife() <= 0 && !isEnterd)
            gc.fillText("Trage deinen Namen ein:\t" + data.getHighScoreName(), 340, 550);

    }

    private void renderLifeBar(GraphicsContext gc, double x, double y, int life, int maxLife, double w) {
        gc.save();
        double width = w / maxLife * life;
        if (width > 10) {
            gc.setFill(Color.GREEN);
        } else {
            gc.setFill(Color.RED);
        }
        gc.fillRect(x - (w / 2), y - (w / 2), width, 4);
        gc.restore();
    }

    private void renderMethode(GraphicsContext gc, double rotation, double x, double y, Image i) {
        gc.save();
        gc.translate(x, y);
        gc.rotate(rotation);
        gc.drawImage(i, -i.getWidth() / 2, -i.getHeight() / 2);
        gc.restore();
    }

    public void click(int mouseClickedX, int mouseClickedY) {
        if (data.getLife() <= 0 || firststart) {

            if (Clicked.clicked(679, 679 + 345, 719, 761, mouseClickedX, mouseClickedY)) {
                System.out.println("clicked restart");
                resetData();
            }
            if (Clicked.clicked(299, 299 + 345, 719, 761, mouseClickedX, mouseClickedY)) {
                System.out.println("back to Menu");
                audioplayer.stopMusic();
                VirusDefender virusDefender = new VirusDefender();
                virusDefender.backToMain();

            }

        }
        if (renderBuildMenu) {
            if (!(mouseClickedX > buildMenu.getX() && mouseClickedX < buildMenu.getX() + buildMenu.getW() && mouseClickedY > buildMenu.getY() && mouseClickedY < buildMenu
                .getY() + buildMenu.getH())) {
                renderBuildMenu = false;
            } else {
                buildMenu.checkButtonClick(mouseClickedX, mouseClickedY, bc.getBuildings());
                buildMenu.checkUpdates();
            }
        }

        for (Building b : bc.getBuildings()) {
            double bx  = b.getX() - (b.getCannon().getImage().getWidth() / 2);
            double bxm = b.getX() + (b.getCannon().getImage().getWidth() / 2);
            double by  = b.getY() - (b.getCannon().getImage().getHeight() / 2);
            double bym = b.getY() + (b.getCannon().getImage().getHeight() / 2);
            if (mouseClickedX > bx && mouseClickedX < bxm) {
                if (mouseClickedY > by && mouseClickedY < bym) {
                    buildMenu.setBuilding(b);
                    buildMenu.checkUpdates();
                    renderBuildMenu = true;
                    break;
                }
            }
        }

    }

    /**
     * Resets the game to play a new Round
     */
    private void resetData() {
        Config.startFirstWave = false;
        bc.reset();
        ec.reset();
        sc.reset();
        data.reset();
        firststart = false;
        isEnterd = false;
    }

    /**
     * Get input String from Keylistener and sends to cheatController
     *
     * @param s inputString from Keylistener
     */
    public void textInput(String s) {
        if (data.getLife() <= 0) {
            enterName(s);
        }
        cheatController.listen(s);
    }

    private void enterName(String s) {
        String name = data.getHighScoreName();
        if (s.equals("BACK_SPACE") && name.length() != 0 && !isEnterd) {
            data.setHighScoreName(name.substring(0, name.length() - 1));
        } else if (s.equals("ENTER")) {
            Score score = new Score(name, data.getPoints());
            hs.getRanking().add(score);
            data.setHighScoreName("");
            isEnterd = true;
            hs.sort();
            hs.safeScore(score);
        } else if (s.length() == 1) {
            data.setHighScoreName(name + s);
        }

    }

    /**
     * if You press "ESC", the Buildmenue hides
     */
    public void buildEsc() {
        System.out.println("ESC");
        renderBuildMenu = false;
    }

    /**
     * Sets the Mouseposition in the varibales mouseX and mouseY
     *
     * @param mouseX int x mouseposition
     * @param mouseY int y mouseposition
     */
    public void setMousemovement(int mouseX, int mouseY) {
        this.mouseX = mouseX;
        this.mouseY = mouseY;
    }
}

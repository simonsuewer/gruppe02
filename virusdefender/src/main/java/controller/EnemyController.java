package controller;

import data.Data;
import data.Enemy;
import data.ImageLibary;

import java.util.ArrayList;

class EnemyController {

    ImageLibary imageLibary;
    private ArrayList<Enemy> enemys              = new ArrayList<>();
    private ArrayList<Enemy> nextWaveEnemy       = new ArrayList<>();
    private ArrayList<Enemy> delteFromNextWave   = new ArrayList<>();
    private ArrayList<Enemy> delteFromNextEnemys = new ArrayList<>();

    EnemyController(ImageLibary imageLibary) {
        this.imageLibary = imageLibary;
    }

    void tick(double dt, Data data) {
        for (Enemy e : nextWaveEnemy) {
            if (e.getDeley() - dt < 0) {
                enemys.add(e);
                delteFromNextWave.add(e);
            } else {
                e.setDeley(e.getDeley() - dt);
            }

        }
        for (Enemy e : delteFromNextWave) {
            nextWaveEnemy.remove(e);
        }
        delteFromNextWave.clear();

        for (Enemy e : enemys) {
            e.updatePoisenTimer(dt);
            e.updateSlowTimer(dt);
            double speed = e.getSpeedPerSecond();
            double x     = e.getX();
            e.setX(x - (dt * speed));
            if (e.getX() < 150) {
                e.setAlive(false);
                delteFromNextEnemys.add(e);
                data.setLife(data.getLife() - 1);
            }
            if (e.getLife() <= 0) {
                e.setAlive(false);
                delteFromNextEnemys.add(e);
            }
        }
        for (Enemy e : delteFromNextEnemys) {
            enemys.remove(e);
        }
        delteFromNextEnemys.clear();
    }

    public void reset() {
        enemys.clear();
        nextWaveEnemy.clear();
        delteFromNextEnemys.clear();
        delteFromNextWave.clear();
    }

    ArrayList<Enemy> getEnemys() {
        return enemys;
    }

    public ArrayList<Enemy> getNextWaveEnemy() {
        return nextWaveEnemy;
    }
}

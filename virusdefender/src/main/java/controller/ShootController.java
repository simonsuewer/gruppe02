package controller;

import data.AudioLibary;
import data.Cannon;
import data.ImageLibary;
import data.Shoot;
import engine.Audio;
import helper.Config;
import helper.Rotation;

import java.util.ArrayList;
import java.util.Random;

class ShootController {

    private Random           random         = new Random();
    private ArrayList<Shoot> shoots         = new ArrayList<>();
    private ArrayList<Shoot> toDeleteShoots = new ArrayList<>();
    private String           imagename      = "shoot";
    private ImageLibary      imageLibary;
    private AudioLibary      audioLibary;
    private Audio            audioplayer    = Audio.instance;

    ShootController(ImageLibary imageLibary, AudioLibary audioLibary) {
        this.imageLibary = imageLibary;
        this.audioLibary = audioLibary;
    }

    void newshoot(double x, double y, Cannon cannon) {
        Shoot shoot = new Shoot();
        shoot.setRotation(cannon.getRoation());
        int shootDMG = random.nextInt(cannon.getMaxDmg() - cannon.getMinDmg()) + cannon.getMinDmg();
        shoot.setDmg(shootDMG);
        shoot.setSpeed(600);

        x = x + Rotation.getShootX(cannon.getImage().getWidth() / 2, cannon.getRoation());
        y = y + Rotation.getShootY(cannon.getImage().getWidth() / 2, cannon.getRoation());
        shoot.setX(x);
        shoot.setY(y);
        shoot.setEnemy(cannon.getEnemy());
        int type = cannon.getUpgradeID() / 1000;

        // 1 Cannon
        // 2 Slow
        // 3 Poisen
        // 4 Laser
        if (type == 1) {
            shoot.setImage(imageLibary.getImage("shoot2"));
        } else if (type == 2) {
            shoot.setHasEffect(true);
            if (cannon.getLevel() < 10) {
                shoot.setPoisen(10);
            } else {
                shoot.setPoisen(cannon.getLevel());
            }
            shoot.setPoisenEffectTime(1 + (cannon.getLevel() * 0.1));
            shoot.setImage(imageLibary.getImage("shoot3"));
        } else if (type == 3) {
            shoot.setImage(imageLibary.getImage("shoot1"));
        } else if (type == 4) {
            shoot.setHasEffect(true);
            if (cannon.getLevel() < 10) {
                shoot.setSlow(10);
            } else {
                shoot.setSlow(cannon.getLevel());
            }
            shoot.setSlowEffectTime(1 + (cannon.getLevel() * 0.1));
            shoot.setImage(imageLibary.getImage("shoot4"));
        } else {
            shoot.setImage(imageLibary.getImage("shoot"));
        }
        shoots.add(shoot);

    }

    /**
     * @param dt
     */
    void tick(double dt) {

        for (Shoot shoot : shoots) {
            if (shoot.getEnemy().isAlive()) {
                double rotate = Rotation.getRotation(shoot.getEnemy().getX() - shoot.getX(), shoot.getEnemy().getY() - shoot.getY());
                shoot.setRotation(Math.toDegrees(rotate));
            }

            double speedFormel = (shoot.getSpeed() * dt);
            double yOffset     = Rotation.getMoveRotateY(shoot.getRotation(), speedFormel);
            double xOffset     = Rotation.getMoveRotationX(speedFormel, yOffset);
            shoot.setxOffset(xOffset);
            shoot.setyOffset(yOffset);
            shoot.setY(shoot.getY() + shoot.getyOffset());
            shoot.setX(shoot.getX() + shoot.getxOffset());
            if ((shoot.getX() < 0) || (shoot.getX() > Config.getCANVAS_WIDTH()) || (shoot.getY() < 0) || (shoot.getY() > Config.getCANVAS_HEIGHT())) {
                toDeleteShoots.add(shoot);
            }
        }
        for (Shoot shoot : toDeleteShoots) {
            shoots.remove(shoot);
        }
        toDeleteShoots.clear();
    }

    void reset() {
        toDeleteShoots.clear();
        shoots.clear();
    }

    ArrayList<Shoot> getShoots() {
        return shoots;
    }
}

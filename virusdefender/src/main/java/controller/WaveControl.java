package controller;

import data.Data;
import data.Enemy;
import data.ImageLibary;
import helper.Config;

import java.util.ArrayList;
import java.util.Random;

public class WaveControl {

    private Random      r         = new Random();
    private ImageLibary imageLibary;
    private Data        data;
    private String      enemyName = "Enemy";

    WaveControl(ImageLibary imageLibary) {
        this.imageLibary = imageLibary;
    }

    void tick(ArrayList<Enemy> enemys) {
        if (enemys.size() > 0) {
            return;
        }
        int wave = data.getWave();
        System.out.println("EnemyLife:\t" + data.getLastlife());
        for (int i = 0; i < wave; i++) {
            Enemy enemy = new Enemy();
            enemy.setLife(data.getLastlife());
            int nameNumber = (wave % 11) + 1;
            enemy.setImage(imageLibary.getImage(enemyName + nameNumber));
            enemy.setX(Config.getCANVAS_WIDTH() - 100);
            int yStart = (int) (r.nextInt(
                (int) ((Config.getCANVAS_HEIGHT() - enemy.getImage().getHeight()) - enemy.getImage().getHeight())) + enemy.getImage()
                                                                                                                          .getHeight());
            enemy.setGoldValue((10 + (wave * 4)));
            enemy.setY(yStart);
            enemy.setRotation(0);
            enemy.setPoints(10);

            enemy.setSpeedPerSecond(r.nextInt(110 - 90) + 90);
            enemy.setDeley((int) (2 + i * 0.3));
            if (wave % 10 == 0 && i == (wave - 1)) {
                enemy.setLife(data.getLastlife() * 5);
                enemy.setDeley((int) (4 + i * 0.3));
                enemy.setImage(imageLibary.getImage("boss"));
                enemy.setPoints(wave * 10);
                enemy.setGoldValue((10 + (wave * 10)));
            }
            enemy.setMaxLife(enemy.getLife());
            enemys.add(enemy);
        }
        data.setWaveString(wave);
        int newlife = (int) (data.getLastlife() * 1.2);
        data.setLastlife(newlife);
        data.setWave(data.getWave() + 1);

    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}

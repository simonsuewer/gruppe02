package data;

import javafx.scene.media.Media;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Objects;

public class AudioLibary {

    private HashMap<String, Media> soundlist = new HashMap<>();

    /**
     * Fügt der in die Soundliste ein neuen Sound hinzu
     *
     * @param name Soundnname
     * @param path Path der Sounddatei
     */
    void addSound(String name, String path) {
        URI uri;
        path = path.replaceAll(" ", "%20");

        String installationPath = Objects.requireNonNull(getClass().getClassLoader().getResource(path)).toExternalForm();
        System.out.println(installationPath);
        try {
            uri = new URI(installationPath);
            System.out.println(path);
            System.out.println(installationPath);
            Media media = new Media(uri.toString());
            soundlist.put(name, media);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    /**
     * Gibt die Benötigte Sounddatei zurück
     *
     * @param name Soundname
     * @return Media Object
     */
    public Media getSound(String name) {
        return soundlist.get(name);
    }

}

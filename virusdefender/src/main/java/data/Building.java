package data;

public class Building {

    private Cannon cannon;
    private double x;
    private double y;
    private double w;
    private double h;

    public Building(Cannon cannon, double x, double y, double w, double h) {
        this.cannon = cannon;
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    public Cannon getCannon() {
        return cannon;
    }

    public double getY() {
        return y;
    }

    public double getX() {
        return x;
    }

    public void setCannon(Cannon cannon) {
        this.cannon = cannon;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setX(double x) {
        this.x = x;
    }
    //TODO infos aus Cannon weitergeben;
}

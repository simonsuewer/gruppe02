package data;

import helper.Config;
import javafx.scene.image.Image;
import javafx.scene.media.Media;

public class Cannon {

    private double roation;
    private double attackSpeed;
    private int    level;
    private int    price;
    private int    minDmg;
    private int    maxDmg;
    private Image  image;
    private Media  audio;
    private String name;

    private double deltashoot = 0;
    private double tragetX    = Config.getCANVAS_WIDTH();
    private double tragetY    = Config.getCANVAS_HEIGHT() / 2;
    private Enemy  enemy;
    private int    upgradeID;

    public Cannon(double roation,
                  double attackSpeed,
                  int level,
                  int price,
                  int minDmg,
                  int maxDmg,
                  Image image, Media audio, String name) {
        this.roation = roation;
        this.attackSpeed = attackSpeed;
        this.level = level;
        this.price = price;
        this.minDmg = minDmg;
        this.maxDmg = maxDmg;
        this.image = image;
        this.audio = audio;
        this.name = name;
    }

    public Cannon(Cannon c) {
        this.roation = c.getRoation();
        this.attackSpeed = c.getAttackSpeed();
        this.level = c.getLevel();
        this.price = c.getLevel();
        this.minDmg = c.getMinDmg();
        this.maxDmg = c.getMaxDmg();
        this.image = c.getImage();
        this.audio = c.getAudio();
        this.name = c.getName();
    }

    public String getName() {
        return name;
    }

    public double getRoation() {
        return roation;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public int getLevel() {
        return level;
    }

    public int getPrice() {
        return price;
    }

    public int getMinDmg() {
        return minDmg;
    }

    public int getMaxDmg() {
        return maxDmg;
    }

    public Image getImage() {
        return image;
    }

    public Media getAudio() {
        return audio;
    }

    public double getDeltashoot() {
        return deltashoot;
    }

    public double getTragetX() {
        return tragetX;
    }

    public double getTragetY() {
        return tragetY;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public int getUpgradeID() {
        return upgradeID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRoation(double roation) {
        this.roation = roation;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setMinDmg(int minDmg) {
        this.minDmg = minDmg;
    }

    public void setMaxDmg(int maxDmg) {
        this.maxDmg = maxDmg;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setDeltashoot(double deltashoot) {
        this.deltashoot = deltashoot;
    }

    public void setTragetX(double tragetx) {
        this.tragetX = tragetx;
    }

    public void setTragetY(double tragetY) {
        this.tragetY = tragetY;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public void setUpgradeID(int upgradeID) {
        this.upgradeID = upgradeID;
    }
}

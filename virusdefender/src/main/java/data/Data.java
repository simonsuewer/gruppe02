package data;

import helper.Config;

public class Data {

    private int    money;
    private int    points;
    private int    life;
    private int    wave          = 1;
    private int    lastlife      = 30;
    private String moneyString;
    private String pointString;
    private String lifeString;
    private String waveString;
    private String HighScoreName = "";

    public Data(int money, int points, int life) {
        this.money = money;
        this.points = points;
        this.life = life;
        moneyString = "\uD83D\uDCB0 " + this.money;
        lifeString = "\uD83D\uDC99 " + this.life;
        waveString = "Wave 1";
        pointString = "Highscore 0";
    }

    public void reset() {
        setLife(Config.START_LIFE);
        setMoney(Config.START_MONEY);
        setPoints(0);
        setWave(1);
        setWaveString(1);
        setLastlife(30);
    }

    public int getMoney() {
        return money;
    }

    public int getPoints() {
        return points;
    }

    public int getLife() {
        return life;
    }

    public String getMoneyString() {
        return moneyString;
    }

    public String getLifeString() {
        return lifeString;
    }

    public String getWaveString() {
        return waveString;
    }

    public int getWave() {
        return wave;
    }

    public String getPointString() {
        return pointString;
    }

    public int getLastlife() {
        return lastlife;
    }

    public String getHighScoreName() {
        return HighScoreName;
    }

    public void setMoney(int money) {
        this.money = money;
        moneyString = "\uD83D\uDCB0 " + this.money;
    }

    public void setPoints(int points) {
        this.points = points;
        this.pointString = "Highscore " + this.points;
    }

    public void setLife(int life) {
        this.life = life;
        lifeString = "\uD83D\uDC99 " + this.life;
    }

    public void setWaveString(int waveString) {
        this.waveString = "Wave " + waveString;
    }

    public void setWave(int wave) {
        this.wave = wave;
    }

    public void setPointString(String pointString) {
        this.pointString = pointString;
    }

    public void setLastlife(int lastlife) {
        this.lastlife = lastlife;
    }

    public void setHighScoreName(String highScoreName) {
        HighScoreName = highScoreName;
    }
}

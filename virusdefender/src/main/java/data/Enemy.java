package data;

import helper.Logger;
import javafx.scene.image.Image;

public class Enemy {

    private double  x;
    private double  y;
    private double  rotation;
    private int     speedPerSecond;
    private int     goldValue;
    private int     life;
    private Image   image;
    private double  deley;
    private boolean isAlive        = true;
    private double  slowEffect;
    private double  slowTimer;
    private double  poisenEffect;
    private double  poisenTimer;
    private double  lastPoisenTick = 0;
    private int     maxLife;
    private int     points         = 0;

    public Enemy() {

    }

    public void updateSlowTimer(double dt) {
        if (slowTimer >= 0) {
            this.slowTimer = this.slowTimer - dt;
        } else {
            setSlow(0);
        }
    }

    public void updatePoisenTimer(double dt) {
        if (poisenTimer >= 0) {
            this.poisenTimer = this.poisenTimer - dt;
            if (lastPoisenTick < 0) {
                life = (int) (life - poisenEffect);
                lastPoisenTick = 1 / 10;
                Logger.log("Life: " + life);
            } else {
                lastPoisenTick = lastPoisenTick - dt;
            }

        } else {
            lastPoisenTick = 0;
            setPoisenEffect(0);
        }
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getRotation() {
        return rotation;
    }

    public int getSpeedPerSecond() {
        int speed = (int) (speedPerSecond * (1 - (slowEffect / 100)));
        return speed;
    }

    public int getGoldValue() {
        return goldValue;
    }

    public int getLife() {
        return life;
    }

    public Image getImage() {
        return image;
    }

    public double getDeley() {
        return deley;
    }

    public boolean isAlive() {
        return isAlive;
    }

    public int getMaxLife() {
        return maxLife;
    }

    public int getPoints() {
        return points;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public void setSpeedPerSecond(int speedPerSecond) {
        this.speedPerSecond = speedPerSecond;
    }

    public void setGoldValue(int goldValue) {
        this.goldValue = goldValue;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setDeley(double deley) {
        this.deley = deley;
    }

    public void setAlive(boolean alive) {
        isAlive = alive;
    }

    public void setSlow(double slow) {
        this.slowEffect = slow;
    }

    public void setSlowTimer(double time) {
        this.slowTimer = time;
    }

    public void setPoisenEffect(double poisenEffect) {
        this.poisenEffect = poisenEffect;
    }

    public void setPoisenTimer(double poisenTimer) {
        this.poisenTimer = poisenTimer;
    }

    public void setMaxLife(int maxLife) {
        this.maxLife = maxLife;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}

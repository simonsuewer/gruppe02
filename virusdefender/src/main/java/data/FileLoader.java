package data;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Objects;

public class FileLoader {

    private ImageLibary imageLibary;
    private AudioLibary audioLibary;
    private Updates     updates;

    public FileLoader(ImageLibary imageLibary, AudioLibary audioLibary, Updates updates) {
        this.imageLibary = imageLibary;
        this.audioLibary = audioLibary;
        this.updates = updates;
    }

    /**
     * Load all Audio and Images path and names. Gives them to the audioLibary and ImageLibary
     */
    public void loadImageAudio() {
        JSONParser parser = new JSONParser();
        try {
            Object
                obj =
                parser.parse(new InputStreamReader(Objects.requireNonNull(getClass().getClassLoader()
                                                                                    .getResourceAsStream("files/initLoading.json"))
                ));
            JSONObject jsonObject = (JSONObject) obj;

            // ------ Load Images ------

            JSONArray imageArray = (JSONArray) jsonObject.get("images");
            for (Object anImageArray : imageArray) {
                JSONObject imagsO = (JSONObject) anImageArray;
                imageLibary.addImage(imagsO.get("name").toString(), imagsO.get("resource").toString());

            }

            // ------ Load Audio ------

            JSONArray audioArray = (JSONArray) jsonObject.get("audio");
            for (Object anAudioArray : audioArray) {
                JSONObject audioO = (JSONObject) anAudioArray;
                audioLibary.addSound(audioO.get("name").toString(), audioO.get("resource").toString());

            }
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load all Cannon Updates.
     * Creates for each Update a Update Class
     */
    public void loadUpgrades() {
        JSONParser parser = new JSONParser();
        try {
            Object
                obj =
                parser.parse(new InputStreamReader(Objects.requireNonNull(getClass().getClassLoader()
                                                                                    .getResourceAsStream("files/updates.json"))));
            JSONObject jsonObject = (JSONObject) obj;

            // ------ Load Upgrades ------res/files/updates.json

            JSONArray upgaredsArray = (JSONArray) jsonObject.get("upgrades");
            for (Object anUpdateArray : upgaredsArray) {
                JSONObject         updateO     = (JSONObject) anUpdateArray;
                String             name        = updateO.get("name").toString();
                String             imageName   = updateO.get("image").toString();
                int                level       = ((Long) updateO.get("level")).intValue();
                int                id          = ((Long) updateO.get("id")).intValue();
                int                cost        = ((Long) updateO.get("cost")).intValue();
                int                minDamage   = ((Long) updateO.get("minDamage")).intValue();
                int                maxDamage   = ((Long) updateO.get("maxDamage")).intValue();
                double             attackSpeed = ((Number) updateO.get("attackSpeed")).doubleValue();
                ArrayList<Integer> nextUp      = new ArrayList<>();
                JSONArray          nextUpgrade = (JSONArray) updateO.get("nextUpgrade");
                for (Object aNextUpgrade : nextUpgrade) {
                    nextUp.add(((Long) aNextUpgrade).intValue());
                }

                Update u = new Update(id, name, imageName, level, cost, minDamage, maxDamage, attackSpeed, nextUp);
                updates.addUpdates(u);
            }

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

    }
}



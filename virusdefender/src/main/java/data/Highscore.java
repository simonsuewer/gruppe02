package data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class Highscore {

    public static Highscore        instance = new Highscore();
    private       ArrayList<Score> ranking  = new ArrayList<Score>();

    private Highscore() {
        loadScore();
    }

    /**
     * Sends HTTP Rquest and gets Higscore list
     */
    private void loadScore() {
        BufferedReader br = null;
        try {

            URL url = new URL("http://virusdefender.classic-test.de/getscore.php");
            br = new BufferedReader(new InputStreamReader(url.openStream()));

            String line;

            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {

                sb.append(line);
                sb.append(System.lineSeparator());
            }
            parseString(sb.toString());
            System.out.println(sb);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Pars the incomming HTML in Score Objects and adds them to the Score Array
     *
     * @param rawInput
     */
    private void parseString(String rawInput) {
        String[] rows = rawInput.split("\n");

        for (String row : rows) {
            if (row.length() != 0) {
                String[] data     = row.split(":");
                String   name     = data[0];
                String   point    = data[1];
                int      pointNum = Integer.valueOf(point);
                ranking.add(new Score(name, pointNum));

            }
        }
        sort();
    }

    /**
     * Sorts the Score Array desc the Points
     */
    public void sort() {
        ranking.sort((o1, o2) -> (o2.getScore() - o1.getScore()));
    }

    /**
     * Sends the Score to an API
     *
     * @param score
     */
    public void safeScore(Score score) {
        String baseURL = "http://virusdefender.classic-test.de/enterScore.php";

        BufferedReader br = null;
        try {

            URL url = new URL(baseURL +
                                  "?name=" + score.getName() +
                                  "&punkte=" + score.getScore());
            br = new BufferedReader(new InputStreamReader(url.openStream()));

            String line;

            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {

                sb.append(line);
                sb.append(System.lineSeparator());
            }
            parseString(sb.toString());
            System.out.println(sb);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public ArrayList<Score> getRanking() {
        return ranking;
    }
}

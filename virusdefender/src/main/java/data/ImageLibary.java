package data;

import javafx.scene.image.Image;

import java.util.HashMap;

public class ImageLibary {

    private HashMap<String, Image> imageList = new HashMap<>();

    public Image getImage(String name) {
        return imageList.get(name);
    }

    void addImage(String name, String imageName) {
        Image image = new Image(getClass().getClassLoader().getResource("img/" + imageName + ".png").toExternalForm());
        imageList.put(name, image);
    }
}

package data;

import javafx.scene.image.Image;

public class Shoot {

    private boolean hasEffect        = false;
    private double  slow             = 0;
    private double  poisen           = 0;
    private double  slowEffectTime   = 0;
    private double  poisenEffectTime = 0;
    private double  x;
    private double  y;
    private double  speed;
    private double  rotation;
    private int     dmg;
    private double  yOffset;
    private double  xOffset;
    private Enemy   enemy;
    private Image   image;

    public Shoot() {

    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getSpeed() {
        return speed;
    }

    public double getRotation() {
        return rotation;
    }

    public int getDmg() {
        return dmg;
    }

    public double getyOffset() {
        return yOffset;
    }

    public double getxOffset() {
        return xOffset;
    }

    public Enemy getEnemy() {
        return enemy;
    }

    public boolean isHasEffect() {
        return hasEffect;
    }

    public double getSlow() {
        return slow;
    }

    public Image getImage() {
        return image;
    }

    public double getPoisen() {
        return poisen;
    }

    public double getSlowEffectTime() {
        return slowEffectTime;
    }

    public double getPoisenEffectTime() {
        return poisenEffectTime;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setRotation(double rotation) {
        this.rotation = rotation;
    }

    public void setDmg(int dmg) {
        this.dmg = dmg;
    }

    public void setyOffset(double yOffset) {
        this.yOffset = yOffset;
    }

    public void setxOffset(double xOffset) {
        this.xOffset = xOffset;
    }

    public void setEnemy(Enemy enemy) {
        this.enemy = enemy;
    }

    public void setHasEffect(boolean hasEffect) {
        this.hasEffect = hasEffect;
    }

    public void setSlow(double slow) {
        this.slow = slow;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public void setPoisen(double poisen) {
        this.poisen = poisen;
    }

    public void setSlowEffectTime(double slowEffectTime) {
        this.slowEffectTime = slowEffectTime;
    }

    public void setPoisenEffectTime(double poisenEffectTime) {
        this.poisenEffectTime = poisenEffectTime;
    }
}

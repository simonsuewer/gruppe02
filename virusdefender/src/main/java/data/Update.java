package data;

import java.util.ArrayList;

public class Update {

    private ArrayList<Integer> nextUpgrades;
    private String             name;
    private String             imageName;
    private int                level;
    private int                cost;
    private int                minDamage;
    private int                maxDamage;
    private double             attackSpeed;
    private int                id;

    Update(int id,
           String name,
           String imageName,
           int level,
           int cost,
           int minDamage,
           int maxDamage,
           double attackSpeed,
           ArrayList<Integer> nextUpgrades) {
        this.id = id;
        this.name = name;
        this.imageName = imageName;
        this.level = level;
        this.cost = cost;
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        this.attackSpeed = attackSpeed;
        this.nextUpgrades = nextUpgrades;
    }

    public String getName() {
        return name;
    }

    public String getImageName() {
        return imageName;
    }

    public int getLevel() {
        return level;
    }

    public int getCost() {
        return cost;
    }

    public int getMinDamage() {
        return minDamage;
    }

    public int getMaxDamage() {
        return maxDamage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public ArrayList<Integer> getNextUpgrades() {
        return nextUpgrades;
    }

    public int getId() {
        return id;
    }

}

package data;

public class UpdateButton {

    private double x, y, w, h;
    private String name;
    private Update update;

    public UpdateButton(double x, double y, double w, double h, Update update) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
        this.update = update;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getW() {
        return w;
    }

    public double getH() {
        return h;
    }

    public String getName() {
        return name;
    }

    public Update getUpdate() {
        return update;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setName(String name) {
        this.name = name;
    }

}

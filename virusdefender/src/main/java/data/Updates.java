package data;

import java.util.HashMap;

public class Updates {

    private HashMap<Integer, Update> updates = new HashMap<>();

    void addUpdates(Update u) {
        updates.put(u.getId(), u);
    }

    public HashMap<Integer, Update> getUpdates() {
        return updates;
    }
}

package engine;

import helper.Config;
import javafx.application.Platform;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class Audio {

    public static Audio       instance = new Audio();
    private       MediaPlayer mediaPlayer;

    /**
     * Spielt die Sounddatein ab
     *
     * @param media Media Object aus der AudioLibary
     */
    public void playBGSound(Media media) {
        //Platform.runLater(() -> {
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer.setVolume(Config.getSoundVolume());
            mediaPlayer.play();
            //mediaPlayer.setAutoPlay(true);
        //});
    }

    public void stopMusic() {
        //Platform.runLater(() -> {
        mediaPlayer.setVolume(0);
            mediaPlayer.setMute(true);
            //mediaPlayer.stop();
            mediaPlayer.dispose();
            System.out.println("test");
        //});
    }
}

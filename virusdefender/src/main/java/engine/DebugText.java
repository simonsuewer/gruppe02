package engine;

import helper.Config;
import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

class DebugText {

    private GraphicsContext gc;
    private Input           input;

    DebugText(GraphicsContext gc, Input input) {
        this.gc = gc;
        this.input = input;
    }

    void printDebug(int fps) {
        Platform.runLater(() -> {
            gc.save();
            gc.translate(0, 0);

            gc.setFill(Color.rgb(0, 0, 0, 0.3));
            gc.fillRect(0, 0, Config.getCANVAS_WIDTH(), 70);
            gc.setFill(Color.GRAY);

            gc.setFont(new Font("Arial", 18));
            gc.fillText("FPS: " + fps, 10, 28);

            gc.fillText("Moved X: " + input.mouseX, 100, 28);
            gc.fillText("Moved Y: " + input.mouseY, 100, 50);

            gc.fillText("Clicked X: " + input.mouseClickedX, 250, 28);
            gc.fillText("Clicked Y: " + input.mouseClickedY, 250, 50);

            gc.fillText("KeyPressed: " + input.keyTyped, 400, 28);
            gc.restore();
        });
    }
}

package engine;

import controller.Controller;
import helper.Config;
import javafx.application.Platform;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;

public class GameContainer implements Runnable {

    private final double          UPADTE_CAP = 1.0 / 60.0;
    private       boolean         running    = false;
    private       Thread          gameLoopThread;
    private       String          title      = "Virus Defender 0.9.0_ALPHA";
    private       GraphicsContext gc;
    private       DebugText       debugText;
    private       Controller      controller;

    GameContainer(GraphicsContext gc, Canvas canvas) {
        controller = new Controller();
        this.gc = gc;
        Input input = new Input(canvas, controller);
        debugText = new DebugText(gc, input);
    }

    void init() {
        gameLoopThread = new Thread(this, "Game Loop Thread");
    }

    void start() {
        if (!this.running) {
            this.running = true;
            if (!this.gameLoopThread.isInterrupted()) {
                gameLoopThread.start();
            }
        }
    }

    public void stop() {
        dispose();
    }

    public void run() {
        gameLoop();
        dispose();
    }

    private void gameLoop() {
        gc.setFont(new Font("Arial", 18));
        this.setRunning(true);
        boolean render;
        double  actualTime;
        double  deltaTick;
        double  passedTime;
        double  lastTime       = System.nanoTime() / 1000000000.0;
        double  unpocessedTime = 0;
        double  frameTime      = 0;
        double  lastTick       = 0;
        int     frames         = 0;
        int     fps            = 0;

        while (this.running) {
            render = false;
            actualTime = System.nanoTime() / 1000000000.0;
            passedTime = actualTime - lastTime;
            lastTime = actualTime;

            unpocessedTime += passedTime;
            frameTime += passedTime;

            while (unpocessedTime >= UPADTE_CAP) {
                unpocessedTime -= UPADTE_CAP;
                if (frameTime >= 1.0) {
                    frameTime = 0;
                    fps = frames;
                    frames = 0;
                }

                deltaTick = (System.nanoTime() / 1000000000.0) - lastTick;
                render = true;
                controller.tick(deltaTick);
                lastTick = System.nanoTime() / 1000000000.0;
            }
            if (render) {
                controller.render(gc);
                if (Config.isDebug()) {
                    debugText.printDebug(fps);
                }

                frames++;
            } /*else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }*/
        }

    }

    private void clear() {
        Platform.runLater(() -> gc.clearRect(0, 0, Config.getCANVAS_WIDTH(), Config.getCANVAS_HEIGHT()));
    }

    private void dispose() {
        if (!this.gameLoopThread.isInterrupted()) {
            try {
                this.running = false;
                this.gameLoopThread.interrupt();
                this.gameLoopThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // ------------------------------- GETTER & SETTER -------------------------------

    String getTitle() {
        return title;
    }

    synchronized void setRunning(boolean value) {
        this.running = value;
    }

}

package engine;

import controller.Controller;
import javafx.scene.canvas.Canvas;

public class Input {

    int    mouseX        = 0;
    int    mouseY        = 0;
    int    mouseClickedX = 0;
    int    mouseClickedY = 0;
    String keyTyped      = "";

    Input(Canvas canvas, Controller controller) {
        canvas.setOnKeyPressed(event -> {
            keyTyped = event.getCode().toString();
            controller.textInput(event.getCode().toString());
            if (event.getCode().toString().equals("ESCAPE")) {
                controller.buildEsc();
            }
        });

        canvas.setOnMouseClicked(event -> {
            mouseClickedX = (int) event.getSceneX();
            mouseClickedY = (int) event.getSceneY();
            controller.click(mouseClickedX, mouseClickedY);
        });
        canvas.setOnMouseMoved(event -> {
            mouseX = (int) event.getX();
            mouseY = (int) event.getY();
            controller.setMousemovement(mouseX, mouseY);
        });
    }
}

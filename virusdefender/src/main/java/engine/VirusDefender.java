package engine;

import de.hsh.projekt.common.GameEventInitiater;
import de.hsh.projekt.common.KeyInput;
import de.hsh.projekt.common.MouseInput;
import javafx.embed.swing.JFXPanel;

import javax.swing.JMenuBar;
import javax.swing.JPanel;

public class VirusDefender extends de.hsh.projekt.common.Game {

    @Override
    public int start() {

        return 0;
    }

    @Override
    public int backToMain() {
        Window window = Window.instance;

        window.getGc().setRunning(false);
        window.getGc().stop();
        GameEventInitiater.exitGame();
        return 0;
    }

    @Override
    public KeyInput getKeyInput() {
        return new KeyInput();
    }

    @Override
    public MouseInput getMouseInput() {
        return new MouseInput();
    }

    @Override
    public String getName() {
        return "Virusdefender";
    }

    @Override
    public String getDescription() {
        return "<html>Das TowerDefenseGame wie Plants vs Zombies Ähnlich wie bei Plants vs Zombies wird hier auf der linken Seite die Verteidigung nach und nach aufgebaut, während von rechts immer stärkerwerdende Viren, Würmer und Trojaner die Firewall bedrohen.<br><br>Haben zuviele Viren und Würmer deiner Firewall zugesetzt, schafft diese es nicht mehr deine Daten zu schützen und dein PC wird gesperrt.</html>";
    }

    @Override
    public JPanel getPanel() {
        return null;
    }

    @Override
    public JFXPanel getJFXPanel() {
        Window window = Window.instance;

        return window.jFX();

    }

    @Override
    public JMenuBar getJMenuBar() {
        return null;
    }
}

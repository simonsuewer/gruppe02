package engine;

import data.Highscore;
import helper.Config;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class Window extends Application {

    public static Window instance = new Window();
    Stage ps;
    private GameContainer gc;

    public Window() {
    }

    public void startWindow(String[] arguments) {
        launch(arguments);
    }

    @Override
    public void start(Stage primaryStage) {

        ps = primaryStage;
        Highscore hs = Highscore.instance;

        primaryStage.setMaxWidth(Config.getCANVAS_WIDTH());
        primaryStage.setMaxHeight(Config.getCANVAS_HEIGHT() + 29);
        Canvas canvas;
        canvas = createCanvas();

        primaryStage.setTitle(gc.getTitle());
        primaryStage.setResizable(false);

        Group VDroot = new Group();
        VDroot.getChildren().add(canvas);
        primaryStage.setScene(new Scene(VDroot));
        primaryStage.setOnCloseRequest(close -> {
            gc.setRunning(false);
            //this.gc.setRunning(false);
            Platform.exit();
            // When in doubt : System.exit(0); // Use at your own risk. Do not store near children.
        });
        primaryStage.show();
        //gc.init();
        //gc.start();
    }

    JFXPanel jFX() {
        JFXPanel panel = new JFXPanel();
        Scene    scene = createScene(panel);
        panel.setScene(scene);
        return panel;
    }

    private Scene createScene(JFXPanel panel) {
        Group     virsudefenderrootgroup = new Group();
        Highscore hs                     = Highscore.instance;

        Canvas canvas;
        canvas = createCanvas();

        virsudefenderrootgroup.getChildren().add(canvas);
        gc.init();
        gc.start();
        return new Scene(virsudefenderrootgroup);
    }

    private Canvas createCanvas() {
        Canvas canvas = new Canvas(Config.getCANVAS_WIDTH(), Config.getCANVAS_HEIGHT());
        canvas.setFocusTraversable(true);
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

        gc = new GameContainer(graphicsContext, canvas);
        return canvas;
    }

    public GameContainer getGc() {
        return gc;
    }
}

package helper;

public class Clicked {

    public static boolean clicked(double x1, double x2, double y1, double y2, int mouseClickedX, int mouseClickedY) {

        return (mouseClickedX > x1 && mouseClickedX < x2 && mouseClickedY < y2 && mouseClickedY > y1);

    }
}

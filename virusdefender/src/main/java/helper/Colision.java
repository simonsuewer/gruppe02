package helper;

import data.Data;
import data.Enemy;
import data.Shoot;

import java.util.ArrayList;

public class Colision {

    private final static int              MIN_GRID_SIZE    = 100;
    public static        Colision         colisionInstance = new Colision();
    private              ArrayList<Enemy> enemysOrigin;
    private              ArrayList<Shoot> shootsOrigin;
    private              ArrayList<Shoot> delteShoots      = new ArrayList<>();
    private              ArrayList<Enemy> delteEnemys      = new ArrayList<>();

    private Colision() {}

    public void checkHit(ArrayList<Enemy> enemys, ArrayList<Shoot> shoots, Data data, double width) {
        width = width / 2;
        ArrayList<Enemy> enemiesLeft  = new ArrayList<>();
        ArrayList<Enemy> enemiesRight = new ArrayList<>();
        ArrayList<Shoot> shootsLeft   = new ArrayList<>();
        ArrayList<Shoot> shootsRight  = new ArrayList<>();
        for (Enemy e : enemys) {
            if (e.getX() <= width) {
                enemiesLeft.add(e);
            } else {
                enemiesRight.add(e);
            }
        }
        for (Shoot s : shoots) {
            if (s.getX() <= width) {
                shootsLeft.add(s);
            } else {
                shootsRight.add(s);
            }
        }
        if (MIN_GRID_SIZE < width) {
            if (enemiesLeft.size() != 0 && shootsLeft.size() != 0) {
                checkHit(enemiesLeft, shootsLeft, data, width);
            }
            if (enemiesRight.size() != 0 && shootsRight.size() != 0) {
                checkHit(enemiesRight, shootsRight, data, width);
            }

        } else {
            splitHight(enemiesLeft, shootsLeft, data, Config.getCANVAS_HEIGHT());
            splitHight(enemiesRight, shootsRight, data, Config.getCANVAS_HEIGHT());
        }

    }

    private void splitHight(ArrayList<Enemy> enemys,
                            ArrayList<Shoot> shoots,
                            Data data,
                            double height) {

        height = height / 2;
        ArrayList<Enemy> enemiesTop    = new ArrayList<>();
        ArrayList<Enemy> enemiesBottom = new ArrayList<>();
        ArrayList<Shoot> shootsTop     = new ArrayList<>();
        ArrayList<Shoot> shootsBottom  = new ArrayList<>();
        for (Enemy e : enemys) {
            if (e.getX() <= height) {
                enemiesTop.add(e);
            } else {
                enemiesBottom.add(e);
            }
        }
        for (Shoot s : shoots) {
            if (s.getX() <= height) {
                shootsTop.add(s);
            } else {
                shootsBottom.add(s);
            }
        }
        if (MIN_GRID_SIZE < height) {
            if (enemiesTop.size() != 0 && shootsTop.size() != 0) {
                splitHight(enemiesTop, shootsTop, data, height);
            }
            if (enemiesBottom.size() != 0 && shootsBottom.size() != 0) {
                splitHight(enemiesBottom, shootsBottom, data, height);
            }
        } else {
            Logger.log("split Height ELSE");
            checkHintInField(enemiesTop, shootsTop, data);
            checkHintInField(enemiesBottom, shootsBottom, data);
        }
    }

    private void checkHintInField(ArrayList<Enemy> enemys,
                                  ArrayList<Shoot> shoots,
                                  Data data) {
        Logger.log("check COLOSION");
        for (Enemy e : enemys) {
            for (Shoot s : shoots) {
                if (e.isAlive()) {
                    double imgWidthD2  = e.getImage().getWidth() / 2;
                    double imgHeightD2 = e.getImage().getHeight() / 2;
                    if (e.getX() - imgWidthD2 < s.getX() && (e.getX() + imgWidthD2 > s.getX())) {
                        if (e.getY() - imgHeightD2 < s.getY() && (e.getY() + imgHeightD2 > s.getY())) {
                            e.setLife(e.getLife() - s.getDmg());
                            delteShoots.add(s);
                            checkEffects(e, s);
                            if (e.getLife() <= 0) {
                                e.setAlive(false);
                                data.setMoney(data.getMoney() + e.getGoldValue());
                                data.setPoints(data.getPoints() + e.getPoints());
                                delteEnemys.add(e);
                            }
                        }
                    }
                }
            }
        }
        for (Shoot s : delteShoots) {
            shootsOrigin.remove(s);
        }
        for (Enemy e : delteEnemys) {
            enemysOrigin.remove(e);
        }
        delteEnemys.clear();
        delteShoots.clear();
    }

    private void checkEffects(Enemy e, Shoot s) {
        if (s.isHasEffect()) {
            e.setSlow(s.getSlow());
            e.setSlowTimer(s.getSlowEffectTime());
            e.setPoisenEffect(s.getPoisen());
            e.setPoisenTimer(s.getPoisenEffectTime());
            Logger.log("ADD EFFECT");
        }
    }

    public void setEnemysOrigin(ArrayList<Enemy> enemysOrigin) {
        this.enemysOrigin = enemysOrigin;
    }

    public void setShootsOrigin(ArrayList<Shoot> shootsOrigin) {
        this.shootsOrigin = shootsOrigin;
    }
}

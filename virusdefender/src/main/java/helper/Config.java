package helper;

public class Config {

    public static final int START_MONEY = 150;
    public static final int START_LIFE  = 10;

    private static final int CANVAS_WIDTH  = 1024;
    private static final int CANVAS_HEIGHT = 768;

    public static boolean startFirstWave = false;

    private static boolean debug       = false;
    private static double  soundVolume = 0.5;

    public static double getSoundVolume() {
        return soundVolume;
    }

    public static void setSoundVolume(double soundVolume) {
        if (soundVolume >= 0 && soundVolume <= 1) {
            Config.soundVolume = soundVolume;
        }
    }

    public static int getCANVAS_WIDTH() {
        return CANVAS_WIDTH;
    }

    public static int getCANVAS_HEIGHT() {
        return CANVAS_HEIGHT;
    }

    public static boolean isDebug() {
        return debug;
    }
}

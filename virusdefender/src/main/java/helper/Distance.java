package helper;

import data.Enemy;

import java.util.ArrayList;

public class Distance {

    public static Enemy nearestEnemy(ArrayList<Enemy> enemys, double x, double y) {
        double  dist         = 1000000;
        Enemy   closestEnemy = new Enemy();
        boolean found        = false;
        for (Enemy e : enemys) {
            double dtx   = x - e.getX();
            double dty   = y - e.getY();
            double ndist = Math.sqrt(dtx * dtx + dty * dty);

            if (ndist < dist) {
                dist = ndist;
                closestEnemy = e;
                found = true;

            }
        }
        if (found) {
            return closestEnemy;
        }
        return null;
    }
}

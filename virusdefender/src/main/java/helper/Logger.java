package helper;

import javafx.scene.Node;
import javafx.scene.Parent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

/**
 * Logs information on the console.
 *
 * @author Alexander Komischke
 */
public class Logger {

    //public static Logger instance        = new Logger();
    //Logger logger = Logger.instance;
    //private Logger() {}
    private static final String SEPERATOR       = "\t : \t";
    private static final String TOPIC_SEPERATOR = " ------------------------- ";

    public static String log(Object o) {
        return log(o.toString());
    }

    public static String log(String message) {

        return log(message, SEPERATOR);
    }

    public static String log(String message, String separator) {
        String new_message = "";
        if (Config.isDebug()) {
            Calendar         calendar         = new GregorianCalendar();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyy.mm.dd HH:mm:ss");
            new_message = simpleDateFormat.format(calendar.getTime()) + separator + message;
            System.out.println(new_message);
        }
        return new_message;
    }

    public static String log(String prefix, String message, String suffix) {
        return log(prefix + message + suffix, SEPERATOR);
    }

    public static String log(String[] messages) {
        return log("", messages, "");
    }

    public static String log(String prefix, String[] messages, String suffix) {
        if (messages == null) {
            return "";
        }
        StringBuilder s = new StringBuilder();
        for (String message : messages) {
            s.append(log(prefix + message + suffix));
        }
        return s.toString();
    }

    public static String log(String topic, String[] messages) {
        return log(topic, "", messages, "");
    }

    public static String log(String topic, String prefix, String[] messages, String suffix) {
        String s = TOPIC_SEPERATOR + topic + TOPIC_SEPERATOR;
        return log("Start : " + s) + "\n" +
            log(prefix, messages, suffix) + "\n" +
            log("End : " + s);
    }

    public static String log(String topic, String prefix, String[] messages) {
        return log(topic, prefix, messages, "");
    }

    public static String logChildren(Parent parent) {
        String parentName = parent.toString();
        Optional<String> children = parent.getChildrenUnmodifiable().stream()
                                          .map(Node::toString)
                                          .findAny();
        return log(parentName, children);
    }

    public static String log(Object o, Object argument) {
        return log(o.toString() + SEPERATOR + argument.toString());
    }

}
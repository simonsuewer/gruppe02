package helper;

public class Rotation {

    public static double getRotation(double distx, double disty) {
        return Math.atan2(disty, distx);
    }

    public static double getMoveRotateY(double rotation, double speed) {
        double beta = 90 - rotation;
        return speed * Math.cos(Math.toRadians(beta));
    }

    public static double getMoveRotationX(double speed, double y) {
        return Math.sqrt(speed * speed - y * y);
    }

    public static double getShootX(double radius, double rotation) {
        return radius * Math.cos(Math.toRadians(rotation));
    }

    public static double getShootY(double radius, double rotation) {
        return radius * Math.sin(Math.toRadians(rotation));
    }
}

package helper;

public class StaticStrings {

    public final static String PRICE        = "Price:\t";
    public final static String WAVE         = "Wave ";
    public final static String BUILD_HEADER = "\tLevel: ";
    public final static String BUY          = "BUY";
    public final static String DMG          = "Schaden: ";
    public final static String DMG_BETWEEN  = " - ";
}
